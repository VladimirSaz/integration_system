def name_of_test(name):
    def wrapper(f):
        f.__name__ = name
        def test(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except BaseException as e:
                print("\nFAILED:"+name)
                raise(e)
        return test
    return wrapper
