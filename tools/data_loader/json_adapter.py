import json


class JSONAdapter():
    def load(self, params):
        path_to_data = '../../data/'
        extension = '.json'
        data_source_name = params['name']
        filepath = path_to_data+data_source_name+extension
        return json.load(open(filepath))
