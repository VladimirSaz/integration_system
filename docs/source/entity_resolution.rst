.. _entity_resolution:

==================
entity_resolution
==================

* entity_resolution - consists of three steps: blocking, pairwise matching, clustering

   * :doc:`entity_resolution/blocking`  - package with classes that implements blocking of entries

   * :doc:`entity_resolution/pairwise_matching`  - package with classes that performs pairwise matching

   * :doc:`entity_resolution/clustering`  - package with classes that clusters entries, to identify entity
