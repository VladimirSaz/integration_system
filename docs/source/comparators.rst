.. _comparators:

==================
comparators
==================

.. autoclass:: comparators.similarity.Similarity
.. automethod:: comparators.similarity.Similarity.are_similar
.. automethod:: comparators.similarity.Similarity.get_similarities
.. automethod:: comparators.similarity.Similarity.are_similar_based_on_scores

.. autoclass:: comparators.factory.ComparatorFactory
.. automethod:: comparators.factory.ComparatorFactory.get

.. autoclass:: comparators.levenshtein_similarity.LevenshteinSimilarity
.. automethod:: comparators.levenshtein_similarity.LevenshteinSimilarity.are_similar
.. automethod:: comparators.levenshtein_similarity.LevenshteinSimilarity.get_similarities
.. automethod:: comparators.levenshtein_similarity.LevenshteinSimilarity.are_similar_based_on_scores

.. autoclass:: comparators.agreement_disagreement.AgreementDisagreement
.. automethod:: comparators.agreement_disagreement.AgreementDisagreement.are_similar
.. automethod:: comparators.agreement_disagreement.AgreementDisagreement.get_similarities
.. automethod:: comparators.agreement_disagreement.AgreementDisagreement.are_similar_based_on_scores

