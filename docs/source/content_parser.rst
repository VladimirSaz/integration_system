.. _content_parser:

==================
content_parser
==================

.. autoclass:: content_parser.rule.Rule
.. automethod:: content_parser.rule.Rule.get
.. automethod:: content_parser.rule.Rule.parse

.. autoclass:: content_parser.spiders.common_scraper.CommonScrapper
