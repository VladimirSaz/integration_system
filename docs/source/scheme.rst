.. _scheme:

==================
scheme
==================

.. autoclass:: scheme.json_validator.Validator
.. automethod:: scheme.json_validator.Validator.is_match
