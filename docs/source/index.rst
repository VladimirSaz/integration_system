.. _topics_index:

Integration System documentation!
==============================================
.. toctree::
   :hidden:
   :maxdepth: 2

   scheme
   metrics
   comparators
   content_parser
   entity_resolution
   entity_resolution/blocking
   entity_resolution/pairwise_matching
   entity_resolution/clustering

Packages
===========
* :doc:`scheme`  - package with classes to validate data scheme

* :doc:`metrics`  - package with classes that implements various distance metrics

* :doc:`comparators`  - package with classes that applies metrics to compare entries

* :doc:`content_parser`  - package with classes to extract data from heterogeneous data sources

* :doc:`entity_resolution`  - package to resolve entities after extraction

   * :doc:`entity_resolution/blocking`  - package with classes that implements blocking of entries

   * :doc:`entity_resolution/pairwise_matching`  - package with classes that performs pairwise matching

   * :doc:`entity_resolution/clustering`  - package with classes that clusters entries, to identify entity

Supplementary Directories:

+-----------------------+-----------------------------------------------------------------+
|Directory              |Description                                                      |
+=======================+=================================================================+
|test                   |collection of tests, that follows the structure of the project   |
+-----------------------+-----------------------------------------------------------------+
|bin                    |collection of starter scripts, to run stages of integration      |
+-----------------------+-----------------------------------------------------------------+
|config                 | configuration of integration system                             |
+-----------------------+-----------------------------------------------------------------+
|config/extraction_rule |collection of rules used to extract data from sources            |
+-----------------------+-----------------------------------------------------------------+
|config/schemas         |collection of data schemas that describe                         |
|                       |                                                                 |
|                       |domains in which integration system operates                     |
+-----------------------+-----------------------------------------------------------------+
|data                   |directory that contains temporary supplemental data              |
+-----------------------+-----------------------------------------------------------------+


Architecture:

.. figure:: _static/dataflow.png
