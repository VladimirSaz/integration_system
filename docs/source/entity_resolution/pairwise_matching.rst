.. _pairwise_matching:

===================================
entity_resolution.pairwise_matching
===================================

.. autoclass:: entity_resolution.pairwise_matching.pair_range_pairwise_matcher.PairwiseMatcher
.. automethod:: entity_resolution.pairwise_matching.pair_range_pairwise_matcher.PairwiseMatcher.match

.. autoclass:: entity_resolution.pairwise_matching.partitioning_by_pairs.PartitioningByPairs
.. automethod:: entity_resolution.pairwise_matching.partitioning_by_pairs.PartitioningByPairs.get

.. autoclass:: entity_resolution.pairwise_matching.pair_comparator.PairComparator
.. automethod:: entity_resolution.pairwise_matching.pair_comparator.PairComparator.get_compared_pairs

.. autoclass:: entity_resolution.pairwise_matching.pair_range_pairwise_matcher_with_average_similarity.PairwiseMatcher
.. automethod:: entity_resolution.pairwise_matching.pair_range_pairwise_matcher_with_average_similarity.PairwiseMatcher.match

.. autoclass:: entity_resolution.pairwise_matching.pair_comparator_with_average_similarity.PairComparator
.. automethod:: entity_resolution.pairwise_matching.pair_comparator_with_average_similarity.PairComparator.get_compared_pairs
