.. _blocking:

==========================
entity_resolution.blocking
==========================

.. autoclass:: entity_resolution.blocking.basic_blocker.Blocker
.. automethod:: entity_resolution.blocking.basic_blocker.Blocker.get_blocks

.. autoclass:: entity_resolution.blocking.pair_range.PairRange
.. automethod:: entity_resolution.blocking.pair_range.PairRange.get
