.. _clustering:

============================
entity_resolution.clustering
============================

.. autoclass:: entity_resolution.clustering.connected_component.Clustering
.. automethod:: entity_resolution.clustering.connected_component.Clustering.cluster

.. autoclass:: entity_resolution.clustering.incremental_record_linkage.Clustering
.. automethod:: entity_resolution.clustering.incremental_record_linkage.Clustering.cluster

