from pyspark.sql import HiveContext
from pyspark.sql import SparkSession
from pyspark.sql.utils import AnalysisException

databases = [
    'extraction', 'for_blocking', 'for_pairwise_matching',
    'pairwise_matching_graph', 'for_resolved_entities', 'final'
]


def main():
    sc = SparkSession.builder.appName('db_creator')
    sc = sc.enableHiveSupport().getOrCreate()
    sc.sparkContext.setLogLevel("OFF")
    #hive_context = HiveContext(sc)
    print(sc.sql("show databases").collect())
    for db_name in databases:
        print(db_name)
        print(sc.sql(f"show tables in {db_name}").collect())
    #print(sc.sql("select * from for_blocking.tableopticbox_1 LIMIT 1").collect())
    #print(sc.sql("select * from final.tableopticbox_1 LIMIT 1").collect())
    sc.stop()

def add_extra_data_to_optic_box_11():
    pass

    # print("last_row!!!")
    # sql_text = f"""
    # SELECT * FROM {db_name_from}.{table_name}
    # """
    # df = hive_context.sql(sql_text).collect()
    # df_list = [row.asDict() for row in df]
    # last_row = df[-1]
    # initial_price = 14
    # for i in range(5):
    #     row = last_row.asDict()
    #     row['price']=f'{initial_price+1} 990 \u20cf'
    #     df_list.append(row)
    #
    # df = hive_context.createDataFrame(df_list)
    # hive_context.sql('USE ' + db_name_from)
    # df.write.mode('overwrite').format('hive').saveAsTable('table' + job_id+'1')
    # sc.stop()

main()
