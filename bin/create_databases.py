from pyspark.sql import HiveContext
from pyspark.sql import SparkSession
from pyspark.sql.utils import AnalysisException


def main():
    sc = SparkSession.builder.appName('db_creator')
    sc = sc.enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)
    try_create_db(hive_context, 'extraction')
    try_create_db(hive_context, 'for_blocking')
    try_create_db(hive_context, 'for_pairwise_matching')
    try_create_db(hive_context, 'pairwise_matching_graph')
    try_create_db(hive_context, 'for_resolved_entities')
    try_create_db(hive_context, 'for_data_fusion_step')
    try_create_db(hive_context, 'final')
    print(hive_context.sql("show databases").collect())
    sc.stop()


def try_create_db(hive_context, db_name):
    try:
        hive_context.sql("create database " + db_name)
    except AnalysisException:
        pass


main()
