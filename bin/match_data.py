import sys
sys.path.append('../')
from pyspark.sql import HiveContext, SparkSession, SQLContext
from entity_resolution.blocking.pair_range import PairRange
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher import PairwiseMatcher
from comparators.factory import ComparatorFactory
from pyspark.sql.types import StructType, StructField, IntegerType, StringType

job_id = sys.argv[1]
db_name_from = "for_pairwise_matching"
db_name_to = "pairwise_matching_graph"
table_name = 'table' + job_id


def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)
    sql_context = SQLContext(sc)

    df = hive_context.sql("SELECT * FROM " + db_name_from + "." + table_name)

    matcher = PairwiseMatcher(
        sqlContext=sql_context,
        partitions=3,
        comparators=_get_comparators()
    )
    pairs = PairRange().get(_get_df_as_blocks(df))
    pairwise_matching_graph = matcher.match([df], pairs)
    hive_context.sql('USE ' + db_name_to)
    pairwise_matching_graph.write.mode('append').format('hive').saveAsTable(table_name)
    sc.stop()


def _validate_input():
    if len(sys.argv) == 2: return
    print("invalid arguments. missing job id")
    sys.exit(1)


def _get_comparators():
    params = {
        'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
        'arguments': {
            'fields': [
                {
                    "name": "description",
                    "minimum_score": "0.50"
                },
                {
                    "name": "name",
                    "minimum_score": "0.85"
                },
                {
                    "name": "price",
                    "minimum_score": "0.85"
                }
            ]
        }
    }
    return [ComparatorFactory().get(params)]


def _get_df_as_blocks(df):
    data = df.orderBy('_block_id', '_id').collect()
    block_id = -1
    result = []
    block = []
    for row in data:
        if (block_id != row['_block_id']):
            block_id = row['_block_id']
            result.append(block)
            block = []
        block.append(row)
    return result


main()
