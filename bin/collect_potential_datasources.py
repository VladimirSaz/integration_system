import re
import sys
from random import randrange
import os

sys.path.append('../')
sys.path.append('.')
from scrapy.crawler import CrawlerProcess
from content_parser.spiders.common_scraper import CommonScrapper
import json
import itertools


def main():
    output_path = 'data/guidetoppharamcology_dataset_search.json_' + str(randrange(100000))
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
        'FEED_FORMAT': 'json',
        'FEED_URI': output_path,
        'FEED_EXPORT_ENCODING': 'utf-8'
    })
    process.crawl(
        CommonScrapper,
        domain='guidetopharmacology.org',
        rule_path=('config/'
                   '/extraction_rules/guidetoppharamcology_dataset_search.json'),
        schema_path=('config/'
                     '/schemas/dataset_sources.json'),
        extra_start_urls=_get_start_url_variants()

    )
    process.start()
    clear_internal_links(output_path)


def _get_start_url_variants():
    url = 'https://www.guidetopharmacology.org/GRAC/DatabaseSearchForward?searchString={}&searchCategories={}&species={}&type={}&order=rank&submit=Search+the+database'
    keywords = ['enzyme', 'protein', 'acid']
    search_categories = [
        "allRec", "receptorName", "overview", "intro", "receptorComment",
        "interactionComment", "immComment", "immProcess", "immProcessComment", "immGoTerm",
        "immGoID", "immCelltype", "immCelltypeComment", "immCoTerm", "immCoId",
        "immCoDef", "immDisease", "immDiseaseComment", "assocProt", "assocProtComment",
        "physFunDesc", "physFunTissue", "physFunComment", "distribTissue", "distribTech",
        "distribComment", "fAssayDesc", "fAssayTissue", "fAssayResponse", "fAssayComment",
        "altExpDesc", "altExpTissue", "altExpTech", "altExpComment", "varEffect",
        "varComment", "pathoDisease", "pathoRole", "pathoDrugs", "pathoSE",
        "pathoUse", "pathoComment", "expChange", "expPatho", "expTissue",
        "expTech", "expComment", "xenoExpChange", "xenoExpTissue", "xenoExpTech",
        "xenoExpComment", "pdbDesc", "selectivityComment", "voltageComment", "functionalChars",
        "transductionComment", "targetGeneName", "targetGeneComment", "dnaBindSeq", "dnaBindComment",
        "partnerName", "partnerEffect", "partnerComment", "coregName", "coregComment", "ec"
    ]
    species = ["Hs", "Mm", "Rn", "none"]
    types = ["GPCR", "IC", "NHR", "ENZ", "CATR", "TRANS", "OTHER", "IMMUNO"]

    urls = []
    for i in itertools.product(keywords, search_categories, species, types):
        urls.append(url.format(*i))
    return urls


def clear_internal_links(path):
    with open(path, 'r') as f:
        pages = json.load(f)
    external_data_sources = []
    for page in pages:
        filtered_links = []
        data_source = page.copy()

        for link in page.get('links', ):
            if re.match(r'https?:', link):
                filtered_links.append(link)
        data_source['links'] = list(set(filtered_links))
        external_data_sources.append(data_source)

    with open(path, 'w') as f:
        json.dump({'data_sources': external_data_sources}, f)


main()
