import sys

sys.path.append('../')
from pyspark.sql import HiveContext, SparkSession, SQLContext
from data_fusion.processor import Processor
#from data_fusion.clustering.connected_component import Clustering

job_id = sys.argv[1]
db_name_from = "for_resolved_entities"
db_name_to = "final"
table_name = 'table' + job_id


def main():
    _validate_input()
    is_spark = True
    hive_context = None
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    sc.sparkContext.setCheckpointDir('../data/checkpoint')
    hive_context = HiveContext(sc)
    sql_context = SQLContext(sc)

    processor = Processor(
        sql_context = hive_context, #заменить на настоящий
        config_path='../config/data_fusion/opticbox_ru.json',
        job_id=job_id
    )
    processor.process()
    sc.stop()


def _validate_input():
    if len(sys.argv) == 2: return
    print("invalid arguments. missing job id")
    sys.exit(1)


main()
