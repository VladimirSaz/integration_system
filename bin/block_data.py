import sys

sys.path.append('../')
sys.path.append('../tools/')
from pyspark.sql import HiveContext, SparkSession
from entity_resolution.blocking.basic_blocker import Blocker
from comparators.levenshtein_similarity import LevenshteinSimilarity

job_id = sys.argv[1]
db_name_from = "for_blocking"
db_name_to = "for_pairwise_matching"
table_name = 'table' + job_id


def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)

    df = hive_context.sql("SELECT * FROM " + db_name_from + "." + table_name)
    comparators = _get_comparator()
    blocker = Blocker(comparators=comparators, dataframes=[df])
    blocks = blocker.get_blocks()
    entries = _get_as_list_of_entries(blocks)
    blocks_df = hive_context.createDataFrame(entries)
    hive_context.sql('USE ' + db_name_to)
    blocks_df.write.mode('append').format('hive').saveAsTable(table_name)
    sc.stop()


def _validate_input():
    if len(sys.argv) == 2: return
    print("invalid arguments. missing job id")
    sys.exit(1)


def _get_comparator():
    fields = [
        {
            "name": "description",
            "minimum_score": "0.50"
        },
    ]
    return [LevenshteinSimilarity(fields=fields)]


def _get_as_list_of_entries(blocks):
    entries = []
    _id = 0
    _block_id = 0
    for block in blocks:
        for entry in block:
            entry = {**entry, '_id': _id, '_block_id': _block_id}
            _id += 1
            entries.append(entry)
        _block_id += 1
    return entries

main()
