import sys

sys.path.append('../')
from pyspark.sql import HiveContext, SparkSession, SQLContext
from entity_resolution.clustering.connected_component import Clustering

job_id = sys.argv[1]
db_name_from = "for_pairwise_matching"
db_name_pairwise_matching_graph = "pairwise_matching_graph"
db_name_to = "for_resolved_entities"
table_name = 'table' + job_id


def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    sc.sparkContext.setCheckpointDir('../data/checkpoint')
    hive_context = HiveContext(sc)
    sql_context = SQLContext(sc)

    df = hive_context.sql("SELECT * FROM " + db_name_from + "." + table_name)
    pairwise_matching_graph = hive_context.sql("SELECT * FROM " + db_name_pairwise_matching_graph + "." + table_name)

    clustering = Clustering(sql_context, checkpointInterval=0)
    for_resolved_entities = clustering.cluster([df], pairwise_matching_graph)

    hive_context.sql('USE ' + db_name_to)
    for_resolved_entities.write.mode('append').format('hive').saveAsTable(table_name)
    sc.stop()


def _validate_input():
    if len(sys.argv) == 2: return
    print("invalid arguments. missing job id")
    sys.exit(1)


main()
