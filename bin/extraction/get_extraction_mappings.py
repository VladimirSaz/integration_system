import sys

sys.path.append('../')
from pyspark.sql import HiveContext, SparkSession
from content_parser.entity_identification.html_entity_learner import HtmlEntityLearner
from metrics.levenshtein import Levenshtein
from pyspark.sql import *
import pickle
import os

job_id = sys.argv[1]
extracted_db_name = "extraction"
extracted_table_name = "raw_data_to_storage_mapping_" + job_id
kb_db_name = 'final'
kb_table_name = 'table' + job_id
# TODO: set names for out
db_name_to = ""
table_name_to = ""


def main():
    _validate_input()
    sc = SparkSession.builder.appName('html entity identifier')
    sc = sc.enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)

    df_mapping_extracted_to_storage = hive_context.sql(
        "SELECT * FROM " + extracted_db_name + "." + extracted_table_name
    )

    kb_df = hive_context.sql(
        "SELECT * FROM " + kb_db_name + "." + kb_table_name
    )
    # kb_stub = get_kb()

    entity_learner = HtmlEntityLearner(
        similarity=Levenshtein(),
        sqlContext=sc,
        kb=kb_df,
        threshold_weak_similarity=0.8,
        threshold_strong_similarity=0.8,
        page_support=5,
        # absolute_path=
        # '/Users/vladimir/Desktop/Workspace/Development/integration_system/'
    )
    # extracted_nodes = get_all_nodes_from_extracted(
    #     entity_learner, df_mapping_extracted_to_storage
    # )
    rules = entity_learner.compare(
        pages=df_mapping_extracted_to_storage.limit(150),
        rules_file="extraction_mappings_150.pickle"
    )
    strong_similarities = entity_learner.get_strong_similarities(rules)
    matchings = entity_learner.calculate_max_similarity_matching(
        strong_similarities, rules
    )
    print(matchings)

    #print(result)
    # hive_context.sql('USE ' + db_name_to)
    # blocks_df.write.mode('append').format('hive').saveAsTable(table_name_to)
    sc.stop()


def _validate_input():
    if len(sys.argv) == 2:
        return
    print("invalid arguments. missing job id")
    sys.exit(1)


# def get_kb():
#     # TODO remove stub
#     """
#
#     the values doesnt matter since we dont compare here. We use matchings and believe
#     their discretion
#
#     """
#     return [
#         Row(
#             _id=0,
#             attribute1='some other string not matching',
#             attribute2='info fields data',
#             attribute3='attribute3 data',
#         ),
#         Row(
#             _id=1,
#             attribute1='a1, abc1, ee1, rr1 abc1,',
#             attribute2='zxc, aaa, bbb,ccc',
#             attribute3='attribute3 different data',
#         ),
#         Row(
#             _id=3,
#             attribute1='a1, abc1, ee1, rr1 abc1,',
#             attribute2='zxc, aaa, bbb,ccc',
#             attribute3='attribute3 different data',
#         ),
#     ]
#

# def get_all_nodes_from_extracted(
#     entity_learner, df_mapping_extracted_to_storage
# ):
#     for mapping in df_mapping_extracted_to_storage.collect():
#         return [entity_learner._get_nodes(mapping)]
#         break
#     #TODO remove stub
#     return [
#         {
#             '_id': 0,
#             'path': 'html/body/ul/li[1]',
#             'value': 'a1, abc1, ee1, rr1 abc1,',
#             'url': 'actor_address_1/actor0.html',
#             'attribute': 'attribute1',
#         },
#         {
#             '_id': 1,
#             'path': 'html/body/ul/li[2]',
#             'value': 'some non similar string abcd1',
#             'url': 'actor_address_1/actor0.html',
#             'attribute': 'attribute1',
#         },
#         {
#             '_id': 0,
#             'path': 'html/body/ul/li[55]',
#             'value': 'info fields data',
#             'url': 'actor_address_1/actor0.html',
#             'attribute': 'attribute2',
#         },
#         {
#             '_id': 3,
#             'path': 'html/body/ul/li[1]',
#             'value': 'a1, abc1, ee1, rr1 abc1,',
#             'url': 'actor_address_1/actor1.html',
#             'attribute': 'attribute2',
#         },
#         {
#             '_id': 4,
#             'path': 'html/body/ul/li[1]',
#             'value': 'some non similar string abcd1',
#             'url': 'actor_address_1/actor2.html',
#             'attribute': 'attribute2',
#         },
#         {
#             '_id': 5,
#             'path': 'html/body/ul/li[55]',
#             'value': 'a1, abc1, ee1, rr1 abc1,',
#             'url': 'actor_address_1/actor1.html',
#             'attribute': 'attribute2',
#         },
#         {
#             '_id': 0,
#             'path': 'html/body/ul/li[99]',
#             'value': 'info fields data',
#             'url': 'actor_address_1/actor0.html',
#             'attribute': 'attribute3',
#         },
#         {
#             '_id': 3,
#             'path': 'html/body/ul/li[99]',
#             'value': 'info fields data',
#             'url': 'actor_address_1/actor4.html',
#             'attribute': 'attribute3',
#         },
#         {
#             '_id': 4,
#             'path': 'html/body/ul/li[99]',
#             'value': 'info fields data',
#             'url': 'actor_address_1/actor5.html',
#             'attribute': 'attribute3',
#         },
#     ]


def get_matchings():
    #Todo remove stub
    return {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2': [{
            'kb': 0,
            'extracted': 0,
        }],
    }


main()
