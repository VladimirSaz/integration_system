import collections

from pyspark.sql import Window, HiveContext, SQLContext, SparkSession, DataFrame
from pyspark.sql.functions import monotonically_increasing_id, row_number
from functools import reduce
import json
import sys

job_id = sys.argv[1]
data_pathes = sys.argv[2:]
db_name = 'extraction'
table_name = 'raw_data_to_storage_mapping_' + job_id
mapping_dir = '../data/mappings/'


def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader')
    sc = sc.enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)
    hive_context.sql('USE ' + db_name)
    df = _get_all_data_as_df(hive_context, data_pathes)
    df.write.mode('append').format('hive').saveAsTable(table_name)
    sc.stop()


def _validate_input():
    if len(sys.argv) > 2:
        return
    print("invalid arguments. script_name.py job_id file1 [file2 file3 ...]")
    sys.exit(1)


def _get_all_data_as_df(hive_context, data_pathes):
    dfs = []
    for path in data_pathes:
        with open(mapping_dir + path) as f:
            json_string = _get_json_from_jsonl_file(f)
            data = json.loads(json_string)
            df = hive_context.createDataFrame(data)
            dfs.append(df)
    all_dfs = _union_all(*dfs)

    return all_dfs.withColumn(
        "_id",
        row_number().over(Window.orderBy(monotonically_increasing_id())) - 1
    )


def _get_json_from_jsonl_file(file):
    content = file.read().replace('\n', '')
    return '[' + content + ']'


def _union_all(*dfs):
    dfs_to_union = list(filter(None, dfs))
    columns = dfs_to_union[0].schema.names
    dfs_to_union = [df.select(*columns) for df in dfs_to_union]
    return reduce(DataFrame.union, dfs_to_union)


main()
