import sys
import os

sys.path.append('../')
from scrapy.crawler import CrawlerProcess
from content_parser.spiders.raw_html_collector import RawHtmlCollector


def main():
    assert 0 == 1  #dont run otherwise you will erase all data
    output_path = '/Users/vladimir/Desktop/Workspace/Development/integration_system/data/mappings/rb-ochki/rb-ochki1.json'
    process = CrawlerProcess(
        {
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
            'FEED_FORMAT': 'json',
            'FEED_URI': output_path,
            'FEED_EXPORT_ENCODING': 'utf-8'
        }
    )
    process.crawl(
        RawHtmlCollector,
        domain='rb_ochki.ru',
        rule_path=(
            '/Users/vladimir/integration_system/config/'
            '/extraction_rules/rb_ochki_ru_for_raw_extraction.json'
        )
    )
    process.start()
    _convert_to_jsonl(output_path)


def _convert_to_jsonl(path):
    jsonl = os.popen("grep '{' " + path).read()

    with open(path, 'w') as file:
        file.write(jsonl)


main()
