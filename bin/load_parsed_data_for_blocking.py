from pyspark.sql import HiveContext, SQLContext, SparkSession, DataFrame
from functools import reduce
import json
import sys

job_id = sys.argv[1]
data_pathes = sys.argv[2:]
db_name = "for_blocking"
table_name = 'table' + job_id

def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    hive_context = HiveContext(sc)
    hive_context.sql('USE ' + db_name)
    df = _get_all_data_as_df(hive_context, data_pathes)
    df.write.mode('append').format('hive').saveAsTable(table_name)
    sc.stop()


def _validate_input():
    if len(sys.argv) > 2: return
    print("invalid arguments. script_name.py job_id file1 [file2 file3 ...]")
    sys.exit(1)


def _get_all_data_as_df(hive_context, data_pathes):
    dfs = []
    for path in data_pathes:
        with open('../data/' + path) as f:
            json_string = _remove_newlines_in_file(f)
            data = json.loads(json_string)
            df = hive_context.createDataFrame(data)
            dfs.append(df)
    return _union_all(*dfs)


def _remove_newlines_in_file(file):
    lines = []
    for line in file:
        lines.append(line.replace('\\n', ' ').replace('\\r', ''))
    return ''.join(lines)


def _union_all(*dfs):
    dfs_to_union = list(filter(None, dfs))
    columns = dfs_to_union[0].schema.names
    dfs_to_union = [df.select(*columns) for df in dfs_to_union]
    return reduce(DataFrame.union, dfs_to_union)


main()
