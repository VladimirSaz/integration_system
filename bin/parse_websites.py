import sys
import os

sys.path.append('../')
from scrapy.crawler import CrawlerProcess
from content_parser.spiders.common_scraper import CommonScrapper


def main():
    output_path = '../data/opticbox_ru.json_20230703'
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
        'FEED_FORMAT': 'json',
        'FEED_URI': output_path,
        'FEED_EXPORT_ENCODING': 'utf-8'
    })
    process.crawl(
        CommonScrapper,
        domain='opticbox.ru',
        rule_path=('../config/extraction_rules/opticbox_ru.json'),
        schema_path=('../config/schemas/eyewear_stores.json')
    )
    process.start()
    #_convert_to_jsonl(output_path)


def _convert_to_jsonl(path):
    os.system("grep '{' " + path + " > " + path)


main()
