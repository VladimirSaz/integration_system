import sys

sys.path.append('../')
from pyspark.sql import HiveContext, SparkSession, SQLContext

job_id = sys.argv[1]
db_name_from = "for_resolved_entities"
db_name_to = "final"
table_name = 'table' + job_id


def main():
    _validate_input()
    sc = SparkSession.builder.appName('loader').enableHiveSupport().getOrCreate()
    sc.sparkContext.setCheckpointDir('../data/checkpoint')
    hive_context = HiveContext(sc)
    sql_context = SQLContext(sc)
    sql_text = f"""
    SELECT * FROM {db_name_from}.{table_name} 
    """
    df = hive_context.sql(sql_text).collect()
    print(f"some entries in {db_name_from}")
    print(df[0:2])
    sql_text = f"""
    SELECT * FROM {db_name_to}.{table_name}
    """
    df = hive_context.sql(sql_text).collect()
    print(f"some entries in {db_name_to}")
    print(df[0:2])

    print("COUNT entries on steps")

    sql_text = f"""
    SELECT COUNT(DISTINCT component) FROM {db_name_from}.{table_name} 
    """
    df = hive_context.sql(sql_text).collect()
    print(f"unique entries in {db_name_from}")
    print(df)
    sql_text = f"""
    SELECT COUNT(DISTINCT component) FROM {db_name_to}.{table_name}
    """
    df = hive_context.sql(sql_text).collect()
    print(f"unique entries in {db_name_to}")
    print(df)

    print("COUNT  UNIQUE entries on steps")

    sql_text = f"""
    SELECT COUNT(DISTINCT component) FROM {db_name_from}.{table_name} 
    """
    df = hive_context.sql(sql_text).collect()
    print(f"number of unique entries in {db_name_from}")
    print(df)
    sql_text = f"""
    SELECT COUNT(DISTINCT component) FROM {db_name_to}.{table_name}
    """
    df = hive_context.sql(sql_text).collect()
    print(f"number of unique entries in {db_name_to}")
    print(df)

    print("COUNT entries on steps")

    sql_text = f"""
    SELECT COUNT(component) FROM {db_name_from}.{table_name} 
    """
    df = hive_context.sql(sql_text).collect()
    print(f"number of entries in {db_name_from}")
    print(df)
    sql_text = f"""
    SELECT COUNT(component) FROM {db_name_to}.{table_name}
    """
    df = hive_context.sql(sql_text).collect()
    print(f"number of entries in {db_name_to}")
    print(df)

    sql_text = f"""
    SELECT * FROM {db_name_to}.{table_name} WHERE COMPONENT = 887
    """
    df = hive_context.sql(sql_text).collect()
    print(f"entries with component 887 in {db_name_to}")
    print(df)

    sql_text = f"""
    SELECT * FROM {db_name_from}.{table_name} WHERE COMPONENT = 887
    """
    df = hive_context.sql(sql_text).collect()
    print(f"entries with component 887 in {db_name_from}")
    print(df)


def _validate_input():
    if len(sys.argv) == 2: return
    print("invalid arguments. missing job id")
    sys.exit(1)


main()
