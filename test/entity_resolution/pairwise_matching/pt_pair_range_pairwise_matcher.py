import warnings
import time
from pyspark.sql import *

from test_spark import TestSpark
from tools.test_tools import name_of_test
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher import PairwiseMatcher
from comparators.factory import ComparatorFactory


class TestPairRangePairwiseMatcher(TestSpark):

    def setUp(self):
        super().setUp()
        self.comparator_factory = ComparatorFactory()

    @name_of_test(
        "match__"
        "with_at_least_4_partitions_and_1_partition__"
        "execution_time_in_first_case_less_than_in_first"
    )
    def test_match__with_4_partitions_and_1_partition__execution_time_less_in_first(self):
        if self.sqlContext._jsc.sc().defaultParallelism() < 4:
            warnings.warn(
                "test_match__with_2_partitions_and_1_partition__"
                "execution_time_less_in_first skipped due to"
                " insufficient amount of executioners")
            return
        # Arrange
        executioners = self.sqlContext._jsc.sc().defaultParallelism()
        overall_blocks = 500
        rows = self.get_2_rows_per_block(overall_blocks)
        df = self.sqlContext.createDataFrame(rows)

        pairs = self.get_pairs_for_2_matching_row_per_block(overall_blocks)
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_distance.LevenshteinDistance",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.80"
                    },
                ]
            }
        })
        # Act

        matcher_partitions_1 = self.get_pairwise_matcher(
            partitions=1, comparators=[comparator]
        )
        runtime_first_case = self.get_average_run_time(
            function_name=matcher_partitions_1.match,
            args=[[df], pairs],
            kwargs={},
            number=3
        )
        matcher_partitions_2 = self.get_pairwise_matcher(
            partitions=executioners, comparators=[comparator]
        )
        runtime_second_case = self.get_average_run_time(
            function_name=matcher_partitions_2.match,
            args=[[df], pairs],
            kwargs={},
            number=3
        )
        result_ratio = runtime_first_case / runtime_second_case
        # Assert
        minimum_expected_ratio = 4
        print(runtime_first_case)
        print(runtime_second_case)
        print(result_ratio)
        self.assertTrue(result_ratio > minimum_expected_ratio)

    @staticmethod
    def get_2_rows_per_block(overall_blocks):
        row_id = 0
        rows = []
        for i in range(overall_blocks):
            rows.append(Row(_id=row_id, _block_id=i, fieldName="1234567890"))
            row_id += 1
            rows.append(Row(_id=row_id, _block_id=i, fieldName="1234567890"))
            row_id += 1
        return rows

    @staticmethod
    def get_pairs_for_2_matching_row_per_block(overall_blocks):
        row_id = 0
        pairs = []
        for i in range(overall_blocks):
            pairs.append([i, row_id, row_id + 1])
            row_id += 2
        return pairs

    def get_pairwise_matcher(self, **kwargs):
        if 'comparators' not in kwargs: kwargs['comparators'] = []
        return PairwiseMatcher(sqlContext=self.sqlContext, **kwargs)

    @staticmethod
    def get_average_run_time(function_name, args, kwargs, number=2):
        overall_time = 0
        for i in range(number):
            time_start = time.perf_counter()
            function_name(*args, **kwargs).collect()
            time_of_run = time.perf_counter() - time_start
            if i == 0: continue
            overall_time += time_of_run
        return overall_time / (number - 1)
