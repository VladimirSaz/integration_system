import unittest
import warnings
from pyspark.sql import *
from pyspark.sql.types import *
from tools.test_tools import name_of_test
from test_spark import TestSpark
from entity_resolution.pairwise_matching.partitioning_by_pairs import PartitioningByPairs


class TestPartitioningByPairs(TestSpark):

    def test_init__with_no_number_of_partitions__instance_hash_field_partitions_with_default_value_4(self):
        # Arrange
        # Act
        partitioning = self.get_partitioning_by_pairs()
        result = partitioning.partitions_count
        # Assert
        expected = 4
        self.assertEqual(expected, result)

    def get_partitioning_by_pairs(self, **kwargs):
        return PartitioningByPairs(
            sqlContext=self.sqlContext,
            **kwargs
        )

    def test_init__with_number_of_partitions_5__instance_has_field_partitions_with_value_5(self):
        # Arrange
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=5)
        result = partitioning.partitions_count
        # Assert
        expected = 5
        self.assertEqual(expected, result)

    @name_of_test("get__"
               "with_empty_pairs_and_1_partition__and_dataframe_with_5_elements__"
               "returns_empty_dataframe")
    def test_get__with_empty_pairs_and_1_partition__returns_empty_df(self):
        # Arrange
        pairs = []
        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=4, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        partitions = 1
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_df = partitioning.get(dataframes, pairs=pairs)
        import pprint
        pprint.pprint(result_df)
        result_df_length = len(result_df.collect())
        # Assert
        expected_length = 0
        self.assertEqual(expected_length, result_df_length)

    @name_of_test(
        "get__"
        "with_empty_pairs_and_2_partitions__and_dataframe_with_5_elements__"
        "return_empty_dataframe"
    )
    def test_get__with_empty_pairs_and_2_partitions__returns_empty_df(self):
        # Arrange
        pairs = []
        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=4, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        partitions = 2
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_df = partitioning.get(dataframes, pairs=pairs)
        result_df_length = len(result_df.collect())
        # Assert
        expected_length = 0
        self.assertEqual(expected_length, result_df_length)

    def test_get__with_empty_dataframes_list__raises_exception(self):
        # Arrange
        pairs = []
        dataframes = []
        partitions = 1
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        is_value_error = False
        try:
            partitioning.get(dataframes, pairs=pairs)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "get__"
        "with_empty_pairs_and_2_partitions__and_dataframe_with_0_elements__"
        "return_empty_dataframe"
    )
    def test_get__with_empty_pairs_empty_dataframe__returns_empty_df(self):
        # Arrange
        pairs = []
        dataframes = [self.get_empty_dataframe_with_schema()]
        partitions = 2
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_df = partitioning.get(dataframes, pairs=pairs)
        result_df_length = len(result_df.collect())
        # Assert
        expected_length = 0
        self.assertEqual(expected_length, result_df_length)

    @name_of_test(
        "get__"
        "with_2_pairs_and_2_partitions__and_dataframe_with_0_elements__"
        "returns_empty_dataframe_with_2_partitions"
    )
    def test_get__with_2_pairs_2_partitions_empty_dataframe__returns_empty_dataframe_with_2_partitions(self):
        # Arrange
        pairs = []
        partitions = 2
        dataframes = [self.get_empty_dataframe_with_schema()]
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        df = partitioning.get(dataframes, pairs=pairs)
        result_number_of_partitions = df.rdd.getNumPartitions()
        result_items_count = len(df.collect())
        # Assert
        expected_number_of_partitions = 2
        expected_items_count = 0
        self.assertEqual(expected_number_of_partitions, result_number_of_partitions)
        self.assertEqual(expected_items_count, result_items_count)

    def get_empty_dataframe_with_schema(self):
        schema = StructType([
            StructField("_id", LongType()),
            StructField("fieldName", StringType()),
            StructField("fieldName2", StringType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)

    @name_of_test(
        "get__"
        "with_4_pairs_and_2_partitions__and_dataframe_with_4_elements__"
        "returns_dataframe_with_2_partitions_both_have_items_for_2_pairs_respectively"
    )
    def test_get__4_pairs_2_partitions_4_items_returns_2partition_with_items(self):
        # Arrange
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
            [0, 2, 3],
        ]
        partitions = 2

        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_partitions = partitioning.get(dataframes, pairs=pairs).rdd.glom().collect()
        # Assert
        expected = [
            [
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ],
            [
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ]
        ]
        self.assertCountEqual(result_partitions, expected)

    @name_of_test(
        "get__"
        "with_3_pairs_and_2_partitions_and_dataframe_with_4_elements__"
        "returns_dataframe_with_2_partitions_where_"
        "first_have_items_for_2_pairs_and_second_for_one_pair"
    )
    def test_get__3_pairs_2_partitions_4_items_returns_2partition(self):
        # Arrange
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 2, 3],
        ]
        partitions = 2

        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_partitions = partitioning.get(dataframes, pairs=pairs).rdd.glom().collect()
        # Assert
        expected = [
            [
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ],
            [
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ]
        ]
        self.assertCountEqual(result_partitions, expected)

    @name_of_test(
        "get__"
        "with_1_pair_and_2_partitions_and_dataframe_with_4_elements__"
        "returns_dataframe_with_2_partitions_where_first_have_items_"
        "for_1_pairs_and_second_have_no_pairs"
    )
    def test_get__1_pair_2_partitions_4_items_returns_2partition(self):
        # Arrange
        pairs = [
            [0, 0, 1],
        ]
        partitions = 2

        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_partitions = partitioning.get(dataframes, pairs=pairs).rdd.glom().collect()
        # Assert
        expected = [
            [
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
            ],
            [],
        ]
        self.assertCountEqual(result_partitions, expected)

    @name_of_test(
        "get__"
        "with_6_pairs_and_3_partitions_and_dataframe_with_4_elements__"
        "returns_dataframe_with_3_partitions_all_three_have_items_"
        "for_2_pairs_respectively`"
    )
    def test_get__6_pairs_3_partitions_4_items_returns_2partition(self):
        # Arrange
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
        ]
        partitions = 3

        dataframes = [
            self.sqlContext.createDataFrame([
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ]),
            self.sqlContext.createDataFrame([
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ])
        ]
        # Act
        partitioning = self.get_partitioning_by_pairs(partitions=partitions)
        result_partitions = partitioning.get(dataframes, pairs=pairs).rdd.glom().collect()
        # Assert
        expected = [
            [
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
            ],
            [
                Row(_id=0, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ],
            [
                Row(_id=1, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=2, fieldName="1234567890", fieldName2="3333333333"),
                Row(_id=3, fieldName="1234567890", fieldName2="3333333333"),
            ]
        ]
        self.assertCountEqual(result_partitions, expected)


if __name__ == '__main__':
    unittest.main()
