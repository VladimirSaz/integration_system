import unittest
from pyspark.sql import *
from pyspark.sql.types import *

from test_spark import TestSpark
from tools.test_tools import name_of_test
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher_with_average_similarity import PairwiseMatcher
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher import PairwiseMatcher as BasePairwiseMatcher
from comparators.factory import ComparatorFactory
from datetime import datetime


class TestPairRangePairwiseMatcherWithAverageSimilarity(TestSpark):

    def setUp(self):
        super().setUp()
        self.comparator_factory = ComparatorFactory()

    def test_init__with_instance_of_class__instance_inherited_from_base_pairwise(self):
        # Arrange
        # Act
        matcher = self.get_pairwise_matcher(partitions=2)
        # Assert
        self.assertIsInstance(matcher, BasePairwiseMatcher)

    def get_pairwise_matcher(self, **kwargs):
        if 'comparators' not in kwargs: kwargs['comparators'] = []
        return PairwiseMatcher(sqlContext=self.sqlContext, **kwargs)

    def test_match__with_dataframe_1_field__response_contains_average_similarity_field_and_expected_value(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="ab"),
            Row(_id=1, _block_id=0, fieldName="cb"),
            Row(_id=2, _block_id=0, fieldName="ad"),
        ])
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
        ]
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        matcher = self.get_pairwise_matcher(partitions=2, comparators=[comparator])
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1, average_similarity=0.5),
            Row(src=0, dst=2, average_similarity=0.5),
            Row(src=1, dst=2, average_similarity=0),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_dataframe_2_fields__"
        "response_contains_average_similarity_field_and_expected_value"
    )
    def test_match__with_dataframe_2_fields__response_contains_average_similarity(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName1="ab", fieldName2="abc"),
            Row(_id=1, _block_id=0, fieldName1="cb", fieldName2="abc"),
            Row(_id=2, _block_id=0, fieldName1="ad", fieldName2="abc"),
        ])
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
        ]
        comparator1 = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName1",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        comparator2 = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        comparators = [comparator1, comparator2]
        # Act
        matcher = self.get_pairwise_matcher(partitions=2, comparators=comparators)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1, average_similarity=0.75),
            Row(src=0, dst=2, average_similarity=0.75),
            Row(src=1, dst=2, average_similarity=0.5),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_dataframe_2_fields__"
        "response_contains_average_similarity_field_and_expected_value"
    )
    def test_match__with_3_fields_1_common_comparator__response_contains_average_similarity(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(
                _id=0,
                _block_id=0,
                fieldName1="ab",
                fieldName2="abc",
                name="Robert James",
                profession="Programmer",
                home_airport="Chicago",
                co_travellers="David, Black",
                date=datetime.strptime('2004 04 8', '%Y %m %d')
            ),
            Row(
                _id=1,
                _block_id=0,
                fieldName1="cb",
                fieldName2="abc",
                name="Robert James",
                profession="Manager",
                home_airport="Chicago",
                co_travellers="Larry,David",
                date=datetime.strptime('2004 08 12', '%Y %m %d')
            )
        ])
        pairs = [
            [0, 0, 1],
        ]
        comparator1 = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName1",
                        "minimum_score": "0.49"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        comparator2 = self.comparator_factory.get({
            'name': "comparators.agreement_disagreement.AgreementDisagreement",
            'arguments': {
                'minimum_score': "0.6",
                'fields': [
                    {
                        "name": "name",
                        "weight": 0.4,
                        "temporal_coefficient": {
                            'span': 4,
                            'span_type': 'month',
                            'type': 'agreement',
                            'value': 0.001
                        }
                    },
                    {
                        "name": "profession",
                        "weight": 0.2,
                        "temporal_coefficient": {
                            'span': 4,
                            'span_type': 'month',
                            'type': 'disagreement',
                            'value': 0.8
                        }
                    },
                    {
                        "name": "home_airport",
                        "weight": 0.2,
                        "temporal_coefficient": {
                            'span': 4,
                            'span_type': 'month',
                            'type': 'agreement',
                            'value': 0.01
                        }
                    },
                    {
                        "name": "co_travellers",
                        "weight": 0.2
                    }
                ],
            }
        })
        comparators = [comparator1, comparator2]
        # Act
        matcher = self.get_pairwise_matcher(partitions=2, comparators=comparators)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = 0.73782234957
        self.assertAlmostEqual(result[0]['average_similarity'], expected)


if __name__ == '__main__':
    unittest.main()
