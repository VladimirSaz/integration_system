import unittest
from pyspark.sql import *
from pyspark.sql.types import *

from test_spark import TestSpark
from tools.test_tools import name_of_test
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher import PairwiseMatcher
from comparators.factory import ComparatorFactory


class TestPairRangePairwiseMatcher(TestSpark):

    def setUp(self):
        super().setUp()
        self.comparator_factory = ComparatorFactory()

    @name_of_test(
        "match__"
        "with_empty_dataframe_of_blocks_and_empty_pairs_per_block__"
        "returns_empty_array"
    )
    def test_match__with_empty_dataframe_and_pairs__returns_empty_array(self):
        # Arrange
        df = self.get_empty_dataframe_with_schema()
        pairs = []
        # Act
        matcher = self.get_pairwise_matcher(partitions=2)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected_item_count = 0
        self.assertEqual(expected_item_count, len(result))

    def get_pairwise_matcher(self, **kwargs):
        if 'comparators' not in kwargs: kwargs['comparators'] = []
        return PairwiseMatcher(sqlContext=self.sqlContext, **kwargs)

    def get_empty_dataframe_with_schema(self):
        schema = StructType([
            StructField("_id", LongType()),
            StructField("_block_id", LongType()),
            StructField("fieldName", StringType()),
            StructField("fieldName2", StringType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)

    def test_match__with_empty_df_of_blocks_and_empty_pairs__returns_empty_array(self):
        # Arrange
        df = self.get_empty_dataframe_with_schema()
        pairs = []
        # Act
        matcher = self.get_pairwise_matcher(partitions=2)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected_item_count = 0
        self.assertEqual(expected_item_count, len(result))

    @name_of_test(
        "match__"
        "with_empty_dataframe_of_blocks_and_1_item_in_pairs_per_block__"
        "raises_value_exception"
    )
    def test_match__with_empty_dataframe_and_1_item_in_pairs__raises_value_exception(self):
        # Arrange
        df = self.get_empty_dataframe_with_schema()
        pairs = [
            [0, 0, 1],
        ]
        # Act
        is_value_error = False
        matcher = self.get_pairwise_matcher(partitions=2)
        try:
            matcher.match([df], pairs)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "match__"
        "with_1_item_in_1_block_and_1_item_in_pairs_per_block__"
        "raises_value_exception"
    )
    def test_match__with_1_item_in_block_and_1_item_in_pairs__raises_value_exception(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
        ]
        # Act
        is_value_error = False
        matcher = self.get_pairwise_matcher(partitions=2)
        try:
            matcher.match([df], pairs)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "match__"
        "with_1_item_in_1_block_and_0_items_in_pairs_per_block__"
        "returns_empty_array"
    )
    def test_match__with_1_item_in_1_block_and_0_items_in_pairs__returns_empty_array(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = []
        # Act
        matcher = self.get_pairwise_matcher(partitions=2)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected_item_count = 0
        self.assertEqual(expected_item_count, len(result))

    @name_of_test(
        "match__"
        "with_2_items_in_1_block_and_0_items_in_pairs_per_block__"
        "returns_empty_array"
    )
    def test_match__with_2_in_1_block_and_0_in_pairs__returns_empty_array(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = []
        # Act
        matcher = self.get_pairwise_matcher(partitions=2)
        result = matcher.match([df], pairs).collect()
        # Assert
        expected_item_count = 0
        self.assertEqual(expected_item_count, len(result))

    @name_of_test(
        "match__"
        "with_2_items_in_1_block_and_1_item_for_other_block_in_pairs_per_block__"
        "raises_value_exception"
    )
    def test_match__with_2_in_1_block_and_1_in_pairs__raises_value_exception(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = [
            [1, 2, 3],
        ]
        # Act
        is_value_error = False
        matcher = self.get_pairwise_matcher(partitions=2)
        try:
            matcher.match([df], pairs)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "match__"
        "not_valid_params_for_the_comparators__"
        "raises_value_expression"
    )
    def test_match__invalid_comparators__raises_value_exception(self):
        # Arrange
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "missing_minimum_score": "0.85"
                    },
                    {
                        "name": "otherFieldName",
                        "minimum_score": "0.80"
                    }
                ]
            }
        }

        # Act
        is_value_error = False
        try:
            self.get_pairwise_matcher(
                partitions=2,
                comparators=[self.comparator_factory.get(params)]
            )
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "match__"
        "with_2_items_and_valid_comparators_but_not_matching_scheme_and_1_"
        "item_for_this_block__"
        "raises_value_expression"
    )
    def test_match__with_comparators_for_invalid_scheme__raises_value_exception(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = [
            [1, 2, 3],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "otherFieldName",
                        "minimum_score": "0.80"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        is_value_error = False
        try:
            matcher.match([df], pairs)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "match__"
        "with_2_matching_items_in_1_block_and_1_item_"
        "for_this_block_in_pairs_per_block_with_valid_comparators__"
        "returns_1_pair_in_array"
    )
    def test_match__with_2_matching_items__returns_1_pair_in_array(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_2_not_matching_items_in_1_block_and_"
        "1_item_for_this_block_in_pairs_per_block_with_valid_comparators__"
        "returns_1_pair_in_array"
    )
    def test_match__with_2_not_matching_items__returns_1_pair_in_array(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="abcdefghjk", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected_item_count = 0
        self.assertEqual(expected_item_count, len(result))

    @name_of_test(
        "match__"
        "with_4_items_where_3_match_in_1_block_and_1_item_for_matching_items_"
        "for_this_block_in_pairs_per_block__"
        "returns_1_pair_in_array"
    )
    def test_match__with_4_items_and_3_matching_1_rule__returns_1_pair_in_array(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=2, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=3, _block_id=0, fieldName="abcdefghjk", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_4_items_where_3_match_in_1_block_and_there_are_items_"
        "to_match_each_one_with_each_one_in_pairs_per_block__"
        "returns_all_pairs"
    )
    def test_match__with_4_items_and_3_matching_all_rules__returns_all_pairs(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567891", fieldName2="3333333334"),
            Row(_id=2, _block_id=0, fieldName="1234567892", fieldName2="3333333335"),
            Row(_id=3, _block_id=0, fieldName="abcdefghjk", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1),
            Row(src=0, dst=2),
            Row(src=1, dst=2),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_4_items_where_3_match_in_2_blocks_and_there_are_"
        "items_to_match_each_one_with_each_one_in_pairs_per_block__"
        "returns_all_pairs"
    )
    def test_match__with_4_items_3_matching_in_2_blocks_all_rules__returns_all_pairs(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=2, _block_id=1, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=3, _block_id=1, fieldName="abcdefghjk", fieldName2="3333333333"),
        ])
        pairs = [
            [0, 0, 1],
            [1, 2, 3],
        ]
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        }

        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[self.comparator_factory.get(params)]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_2_different_comparators_for_2_different_only_"
        "on_second_field_and_two_same_items__"
        "returns_2_items"
    )
    def test_match__with_2_different_comparators_for_4_items__returns_2_items(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="1234567890", fieldName2="3333333333"),
            Row(_id=1, _block_id=0, fieldName="1234567890", fieldName2="4444444444"),
            Row(_id=2, _block_id=1, fieldName="abcdefghjk", fieldName2="5555555555"),
            Row(_id=3, _block_id=1, fieldName="abcdefghjk", fieldName2="5555555555"),
        ])
        pairs = [
            [0, 0, 1],
            [1, 2, 3],
        ]
        comparator1 = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                ]
            }
        })
        comparator2 = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName2",
                        "minimum_score": "0.85"
                    }
                ]
            }
        })
        # Act
        matcher = self.get_pairwise_matcher(
            partitions=2,
            comparators=[comparator1, comparator2]
        )
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=2, dst=3),
        ]
        self.assertEqualDataframes(result, expected)

    @name_of_test(
        "match__"
        "with_3_items_in_block_where_one_pair_is_not_matching_and_"
        "all_else_matching_with_merge_flag_true_and_transitivity_false__"
        "returns_all_three_items_as_is"
    )
    def test_match__with_3_items_where_1_pair_not_match_merge_true_transitivity_false__returns_3_items_as_is(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="ab"),
            Row(_id=1, _block_id=0, fieldName="cb"),
            Row(_id=2, _block_id=0, fieldName="ad"),
        ])
        pairs = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
        ]
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        matcher = self.get_pairwise_matcher(partitions=2, comparators=[comparator])
        result = matcher.match([df], pairs).collect()
        # Assert
        expected = [
            Row(src=0, dst=1),
            Row(src=0, dst=2),
        ]
        self.assertEqualDataframes(result, expected)


if __name__ == '__main__':
    unittest.main()
