import unittest
from request_processor import RequestProcessor
from tools.test_tools import name_of_test
from pprint import pprint #noqa


class TestUM(unittest.TestCase):
    def setUp(self):
        self.request_processor = RequestProcessor()

    @name_of_test("test_select_by_With_given_list_of_functions_to_apply_"
               "and_resolved_entities__Returned_resolved_data")
    def test_select_by(self):
        # Arrange
        resolved_data = {
            's1': [{
                'name': 'abc cde qqqq',
                'price': '5230.50',
                'description': 'item0 description',
                '__id': 0
            }],
            's2': [{
                'name': 'abc cdf',
                'price': '4 230.50 \u20cf',
                'description': 'better item0 description',
                '__id': 0}],
            's3': [{
                'name': 'xyz 333',
                'price': '111.70',
                'description': 'good item1 description',
                '__id': 1}],
            's4': [{
                'name': 'xyx 333 asdf',
                'price': '51.70',
                'description': 'item1',
                '__id': 1}]
        }
        request = {
            'attributes': [
                {
                    'name': "name",
                    'function_to_apply': {
                        'name': "Longest",
                        'params': ""
                    }
                },
                {
                    'name': "price",
                    'function_to_apply': {
                        'name': "MinimumValue",
                        'params': ""
                    }
                },
                {
                    'name': "description",
                    'function_to_apply': {
                        'name': "Longest",
                        'params': ""
                    }
                }
            ],
            'sources': ["opticbox_test", "rb-ochki_test"],
            'fuse_by': {
                'name': "name",
                'function_to_apply': {
                        'name': "LevenshteinDistance",
                        'params': "0.85"
                }
            }
        }

        # Act
        result_data = self.request_processor.select_by(resolved_data, request)
        expected_data = [
            {
                'name': 'abc cde qqqq',
                'price': 4230.50,
                'description': 'better item0 description',
            },
            {
                'name': 'xyx 333 asdf',
                'price': 51.70,
                'description': 'good item1 description',
            },
        ]
        # Assert
        self.assertEqual(expected_data, result_data)


if __name__ == '__main__':
    unittest.main()
