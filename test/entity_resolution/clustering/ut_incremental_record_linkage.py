import unittest
from pyspark.sql import *
from pyspark.sql.types import *

from test_spark import TestSpark
from tools.test_tools import name_of_test
from entity_resolution.pairwise_matching.pair_range_pairwise_matcher_with_average_similarity import PairwiseMatcher
from entity_resolution.clustering.incremental_record_linkage import Clustering
from entity_resolution.blocking.pair_range import PairRange
from comparators.factory import ComparatorFactory


class TestIncrementalRecordLinkageClustering(TestSpark):

    def setUp(self):
        super().setUp()
        self.comparator_factory = ComparatorFactory()

    @unittest.skip
    @name_of_test(
        "cluster__"
        "with_no_previous_data_and_no_merge_needed__"
        "returns_all_rows_with_different_ids"
    )
    def test_cluster__with_3_new_items_not_matching__returns_3_different_items(self):
        # Arrange
        original_cluster = None
        delta_data = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="1234"),
            Row(_id=2, _block_id=0, fieldName="4567"),
        ])
        original_matching_graph = None
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        clustering = self.get_clustering([comparator])
        result, pairwise_graph = clustering.cluster(
            original_cluster,
            delta_data,
            original_matching_graph
        )
        result = result.collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="abcd", component=0),
            Row(_id=1, _block_id=0, fieldName="1234", component=1),
            Row(_id=2, _block_id=0, fieldName="4567", component=2),
        ]
        self.assertEqualDataframes(expected, result)

    def get_clustering(self, comparators):
        return Clustering(
            self.sqlContext,
            pairwise_matcher=PairwiseMatcher(
                sqlContext=self.sqlContext,
                partitions=2,
                comparators=comparators
            ),
            pair_composer=PairRange()

        )

    @unittest.skip
    @name_of_test(
        "cluster__"
        "with_no_previous_data_and_2_similar_and_1_different__"
        "returns_2_rows_with_same_id_and_1_different"
    )
    def test_cluster__with_2_similar_1_different__returns_data_in_2_components(self):
        # Arrange
        original_cluster = None
        delta_data = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="John"),
            Row(_id=1, _block_id=0, fieldName="John"),
            Row(_id=2, _block_id=0, fieldName="Alexander"),
        ])
        original_matching_graph = None
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        clustering = self.get_clustering([comparator])
        result, pairwise_graph = clustering.cluster(
            original_cluster,
            delta_data,
            original_matching_graph
        )
        result = result.collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="John", component=1),
            Row(_id=1, _block_id=0, fieldName="John", component=1),
            Row(_id=2, _block_id=0, fieldName="Alexander", component=0),
        ]
        print(result)
        self.assertEqualDataframes(expected, result)

    @unittest.skip
    @name_of_test(
        "cluster__"
        "starting_with_three_in_one_cluster_adds_new_three__"
        "two_components"
    )
    def test_cluster__with_3_similar_3_different__returns_data_in_2_components(self):
        # Arrange
        original_cluster = None
        delta_data = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="John"),
            Row(_id=1, _block_id=0, fieldName="James"),
            Row(_id=2, _block_id=0, fieldName="James"),
            Row(_id=3, _block_id=0, fieldName="John"),
            Row(_id=4, _block_id=0, fieldName="John"),
            Row(_id=5, _block_id=0, fieldName="John"),
        ])
        original_matching_graph = None
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        clustering = self.get_clustering([comparator])
        result, pairwise_graph = clustering.cluster(
            original_cluster,
            delta_data,
            original_matching_graph
        )
        result = result.orderBy('_id').collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="John", component=1),
            Row(_id=1, _block_id=0, fieldName="James", component=0),
            Row(_id=2, _block_id=0, fieldName="James", component=0),
            Row(_id=3, _block_id=0, fieldName="John", component=1),
            Row(_id=4, _block_id=0, fieldName="John", component=1),
            Row(_id=5, _block_id=0, fieldName="John", component=1),
        ]
        print(result)
        self.assertEqualDataframes(expected, result)

    @unittest.skip
    @name_of_test(
        "cluster__"
        "with_previous_data_and_no_pairwise_match_data__"
        "returns_expected_clusters"
    )
    def test_cluster__no_pairwise_match_data__returns_expected_clusters(self):
        # Arrange
        original_cluster = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="John", component=1),
            Row(_id=1, _block_id=0, fieldName="James", component=0),
            Row(_id=2, _block_id=0, fieldName="James", component=0),
            Row(_id=3, _block_id=0, fieldName="John", component=1),
            Row(_id=4, _block_id=0, fieldName="John", component=1),
            Row(_id=5, _block_id=0, fieldName="John", component=1),
        ])
        delta_data = self.sqlContext.createDataFrame([
            Row(_id=6, _block_id=0, fieldName="John"),
        ])
        original_matching_graph = None
        comparator = self.comparator_factory.get({
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.49"
                    },
                ]
            }
        })
        # Act
        clustering = self.get_clustering([comparator])
        result, pairwise_graph = clustering.cluster(
            original_cluster,
            delta_data,
            original_matching_graph
        )
        result = result.orderBy('_id').collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="John", component=1),
            Row(_id=1, _block_id=0, fieldName="James", component=0),
            Row(_id=2, _block_id=0, fieldName="James", component=0),
            Row(_id=3, _block_id=0, fieldName="John", component=1),
            Row(_id=4, _block_id=0, fieldName="John", component=1),
            Row(_id=5, _block_id=0, fieldName="John", component=1),
            Row(_id=6, _block_id=0, fieldName="John", component=1),
        ]
        print(result)
        self.assertEqualDataframes(expected, result)

        if __name__ == '__main__':
            unittest.main()
