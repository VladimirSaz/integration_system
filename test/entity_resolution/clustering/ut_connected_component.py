import unittest
from pyspark.sql import *
from pyspark.sql.types import *

from test_spark import TestSpark
from tools.test_tools import name_of_test
from entity_resolution.clustering.connected_component import Clustering


class TestConnectedComponentClustering(TestSpark):

    def setUp(self):
        super().setUp()
        self.clustering = Clustering(self.sqlContext, checkpointInterval=0)

    def test_cluster__with_3_items_all_matching__returns_3_items_in_one_component(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="abcd"),
            Row(_id=2, _block_id=0, fieldName="abcd"),
        ])

        pairwise_matching_graph = self.sqlContext.createDataFrame([
            Row(src=0, dst=1),
            Row(src=2, dst=1),
            Row(src=2, dst=0),
        ])
        # Act
        result = self.clustering.cluster([df], pairwise_matching_graph).collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="abcd", component=0),
            Row(_id=1, _block_id=0, fieldName="abcd", component=0),
            Row(_id=2, _block_id=0, fieldName="abcd", component=0),
        ]
        self.assertEqualDataframes(expected, result)

    @name_of_test(
        "cluster__"
        "with_5_items_in_two_dataframes_all_matching__"
        "returns_5_items_in_one_component"
    )
    def test_cluster__with_5_items_in_2_df__returns_5_items(self):
        # Arrange
        df1 = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="abcd"),
            Row(_id=2, _block_id=0, fieldName="abcd"),
        ])
        df2 = self.sqlContext.createDataFrame([
            Row(_id=3, _block_id=0, fieldName="abcd"),
            Row(_id=4, _block_id=0, fieldName="abcd"),
        ])

        pairwise_matching_graph = self.sqlContext.createDataFrame([
            Row(src=0, dst=1),
            Row(src=0, dst=2),
            Row(src=0, dst=3),
            Row(src=0, dst=4),
            Row(src=1, dst=2),
            Row(src=1, dst=3),
            Row(src=1, dst=4),
            Row(src=2, dst=3),
            Row(src=2, dst=4),
            Row(src=3, dst=4),
        ])
        dataframes = [df1, df2]
        # Act
        result = self.clustering.cluster(dataframes, pairwise_matching_graph).collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="abcd", component=0),
            Row(_id=1, _block_id=0, fieldName="abcd", component=0),
            Row(_id=2, _block_id=0, fieldName="abcd", component=0),
            Row(_id=3, _block_id=0, fieldName="abcd", component=0),
            Row(_id=4, _block_id=0, fieldName="abcd", component=0),
        ]
        self.assertEqualDataframes(expected, result)

    @name_of_test(
        "cluster__"
        "with_6_items_and_half_matching__"
        "returns_3_items_in_one_component_and_3_in_another"
    )
    def test_cluster__with_6_items_half_matching__returns_3_and_3_items(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="abcd"),
            Row(_id=2, _block_id=0, fieldName="abcd"),
            Row(_id=3, _block_id=0, fieldName="1234"),
            Row(_id=4, _block_id=0, fieldName="1234"),
            Row(_id=5, _block_id=0, fieldName="1234"),
        ])

        pairwise_matching_graph = self.sqlContext.createDataFrame([
            Row(src=0, dst=1),
            Row(src=0, dst=2),
            Row(src=1, dst=2),
            Row(src=3, dst=4),
            Row(src=3, dst=5),
            Row(src=4, dst=5),
        ])
        # Act
        result = self.clustering.cluster([df], pairwise_matching_graph).collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="abcd", component=0),
            Row(_id=1, _block_id=0, fieldName="abcd", component=0),
            Row(_id=2, _block_id=0, fieldName="abcd", component=0),
            Row(_id=3, _block_id=0, fieldName="1234", component=3),
            Row(_id=4, _block_id=0, fieldName="1234", component=3),
            Row(_id=5, _block_id=0, fieldName="1234", component=3),
        ]
        self.assertEqualDataframes(expected, result)

    def test_cluster__with_0_items__returns_0_items(self):
        # Arrange
        df = self.get_empty_dataframe_with_schema()
        pairwise_matching_graph = self.get_empty_pairwise_matching_graph_dataframe()
        # Act
        result = self.clustering.cluster([df], pairwise_matching_graph).collect()
        # Assert
        expected = 0
        self.assertEqual(expected, len(result))

    def get_empty_dataframe_with_schema(self):
        schema = StructType([
            StructField("_id", LongType()),
            StructField("_block_id", LongType()),
            StructField("fieldName", StringType()),
            StructField("fieldName2", StringType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)

    def get_empty_pairwise_matching_graph_dataframe(self):
        schema = StructType([
            StructField("src", LongType()),
            StructField("dst", LongType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)

    def test_cluster__with_0_dataframes__raises_value_exception(self):
        # Arrange
        dataframes = []
        pairwise_matching_graph = self.get_empty_pairwise_matching_graph_dataframe()
        # Act
        is_value_error = False
        try:
            self.clustering.cluster(dataframes, pairwise_matching_graph).collect()
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "cluster__"
        "empty_pairwise_graph__"
        "returns_all_items_each_in_a_different_component"
    )
    def test_cluster__empty_pairwise__returns_all_items_in_different_component(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="abcd"),
            Row(_id=2, _block_id=0, fieldName="abcd"),
            Row(_id=3, _block_id=0, fieldName="1234"),
            Row(_id=4, _block_id=0, fieldName="1234"),
            Row(_id=5, _block_id=0, fieldName="1234"),
        ])
        pairwise_matching_graph = self.get_empty_pairwise_matching_graph_dataframe()
        # Act
        result = self.clustering.cluster([df], pairwise_matching_graph).collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="abcd", component=0),
            Row(_id=1, _block_id=0, fieldName="abcd", component=1),
            Row(_id=2, _block_id=0, fieldName="abcd", component=2),
            Row(_id=3, _block_id=0, fieldName="1234", component=3),
            Row(_id=4, _block_id=0, fieldName="1234", component=4),
            Row(_id=5, _block_id=0, fieldName="1234", component=5),
        ]
        self.assertEqualDataframes(expected, result)

    def test_cluster__None_as_pairwise_graph__raises_value_exception(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="abcd"),
            Row(_id=1, _block_id=0, fieldName="abcd"),
            Row(_id=2, _block_id=0, fieldName="abcd"),
            Row(_id=3, _block_id=0, fieldName="1234"),
            Row(_id=4, _block_id=0, fieldName="1234"),
            Row(_id=5, _block_id=0, fieldName="1234"),
        ])
        pairwise_matching_graph = None
        # Act
        is_value_error = False
        try:
            self.clustering.cluster([df], pairwise_matching_graph).collect()
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test(
        "cluster__"
        "with_3_items_and_pairwise_graph_have_2_pairs__"
        "returns_all_items_in_one_component"
    )
    def test_cluster__with_3_items_and_2_matching_returns_all_in_one(self):
        # Arrange
        df = self.sqlContext.createDataFrame([
            Row(_id=0, _block_id=0, fieldName="ab"),
            Row(_id=1, _block_id=0, fieldName="cb"),
            Row(_id=2, _block_id=0, fieldName="cd"),
        ])
        pairwise_matching_graph = self.sqlContext.createDataFrame([
            Row(src=0, dst=1),
            Row(src=1, dst=2),
        ])
        # Act
        result = self.clustering.cluster([df], pairwise_matching_graph).collect()
        # Assert
        expected = [
            Row(_id=0, _block_id=0, fieldName="ab", component=0),
            Row(_id=1, _block_id=0, fieldName="cb", component=0),
            Row(_id=2, _block_id=0, fieldName="cd", component=0),
        ]
        self.assertEqualDataframes(expected, result)


if __name__ == '__main__':
    unittest.main()
