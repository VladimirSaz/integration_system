import unittest
from entity_resolution.request_parser import RequestParser
from tools.test_tools import name_of_test
from pprint import pprint #noqa


class TestUM(unittest.TestCase):
    def setUp(self):
        self.request_parser = RequestParser()

    @name_of_test("test_parse__With_1_attribute_and_2_sources__"
               "Returns_attributes_sources_and_by_which_attribute_we_"
               "do_fuse")
    def test_parse_request_1(self):
        # Arrange
        request = '''
        SELECT
            RESOLVE(name, Longest)
        FUSE FROM
            opticbox_test,
            rb-ochki_test
        FUSE BY
            SIMILARITY(name, LevenshteinDistance(0.85))
        '''
        # Act
        result = self.request_parser.parse(request)
        expected = {
            'attributes': [
                {
                    'name': "name",
                    'function_to_apply': {
                        'name': "Longest",
                        'params': ""
                    }
                }
            ],
            'sources': ["opticbox_test", "rb-ochki_test"],
            'fuse_by': {
                'name': "name",
                'function_to_apply': {
                        'name': "LevenshteinDistance",
                        'params': "0.85"
                }
            }
        }
        # Assert
        self.assertEqual(expected, result)

    @name_of_test("test_parse__With_3_attributes_and_2_sources__"
               "Returns_attributes_sources_and_by_which_attribute_we_"
               "do_fuse")
    def test_parse_request_2(self):
        # Arrange
        request = '''
        SELECT
            RESOLVE(name, Longest)
            RESOLVE(price, MinimumValue)
            RESOLVE(description, Longest)
        FUSE FROM
            opticbox_test,
            rb-ochki_test
        FUSE BY
            SIMILARITY(name, LevenshteinDistance(0.85))
        '''
        # Act
        result = self.request_parser.parse(request)
        expected = {
            'attributes': [
                {
                    'name': "name",
                    'function_to_apply': {
                        'name': "Longest",
                        'params': ""
                    }
                },
                {
                    'name': "price",
                    'function_to_apply': {
                        'name': "MinimumValue",
                        'params': ""
                    }
                },
                {
                    'name': "description",
                    'function_to_apply': {
                        'name': "Longest",
                        'params': ""
                    }
                }
            ],
            'sources': ["opticbox_test", "rb-ochki_test"],
            'fuse_by': {
                'name': "name",
                'function_to_apply': {
                        'name': "LevenshteinDistance",
                        'params': "0.85"
                }
            }
        }
        # Assert
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
