import unittest
from tools.test_tools import name_of_test
from pprint import pprint #noqa
from entity_resolution.entity_resolver import EntityResolver


class TestUM(unittest.TestCase):
    def setUp(self):
        self.entity_resolver = EntityResolver('__id')

    @name_of_test("test_resolve_entities__With_two_of_two_semi_similar_"
               "entitites__Returns_data_with_id_0_and_1_for_similar_ones")
    def test_resolve_entities(self):
        # Arrange
        data = {'s1': [{'name': 'abc cde'}], 's2': [{'name': 'abc cdf'}],
                's3': [{'name': 'xyz 333'}], 's4': [{'name': 'xyx 333'}]}
        resolve_by = {
                'name': "name",
                'function_to_apply': {
                        'name': "LevenshteinDistance",
                        'params': "0.85"
                }
            }

        # Act
        result_data = self.entity_resolver.resolve(data, resolve_by)
        expected_data = {
            's1': [{'name': 'abc cde', '__id': 0}],
            's2': [{'name': 'abc cdf', '__id': 0}],
            's3': [{'name': 'xyz 333', '__id': 1}],
            's4': [{'name': 'xyx 333', '__id': 1}]
        }
        # Assert
        self.assertEqual(expected_data, result_data)


if __name__ == '__main__':
    unittest.main()
