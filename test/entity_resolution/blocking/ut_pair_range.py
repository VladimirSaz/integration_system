import unittest
from tools.test_tools import name_of_test
from entity_resolution.blocking.pair_range import PairRange


class TestingPairRange(unittest.TestCase):
    def setUp(self):
        self.pair_range = PairRange()

    def test_get__with_empty_list__returns_empty_list(self):
        # Arrange
        blocks = []
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = []
        self.assertCountEqual(result, expected)

    def test_get__with_1_block_and_1_item__returns_empty_list(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description of product1",
                    "price": "11.1"
                },
            ]
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = []
        self.assertCountEqual(result, expected)

    def test_get__with_2_blocks_and_1_item_in_each_of_them__returns_empty_list(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description of product1",
                    "price": "11.1"
                },
            ],
            [
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "some other description of product2",
                    "price": "22.2"
                },
            ]
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = []
        self.assertCountEqual(result, expected)

    def test_get__with_1_block_and_2_items__returns_list_with_1_item(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
            ]
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected_block = 0
        expected_item1 = 0
        expected_item2 = 1
        expected = [[expected_block, expected_item1, expected_item2]]
        self.assertCountEqual(expected, result)

    def test_get__with_2_blocks_and_2_items_in_each_of_them__return_list_with_2_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
            ],
            [
                {
                    "_id": 2,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 3,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [1, 2, 3]
        ]
        self.assertCountEqual(expected, result)

    def test_get__with_1_block_and_3_items__returns_list_with_3_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 2,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
        ]
        self.assertCountEqual(expected, result)

    @name_of_test("get__"
               "with_2_blocks_and_3_items_in_each_of_them__"
               "returns_list_with_6_items")
    def test_get__with_2_blocks_and_3_items_in_each__returns_6_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 2,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
            ],
            [
                {
                    "_id": 3,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 4,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 5,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 1, 2],
            [1, 3, 4],
            [1, 3, 5],
            [1, 4, 5],
        ]
        self.assertCountEqual(expected, result)

    def test_get__with_1_block_and_4_items__returns_list_with_6_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 2,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 3,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
        ]
        self.assertCountEqual(expected, result)

    def test_get__with_2_block_and_4_items__returns_list_with_12_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 2,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 3,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
            [
                {
                    "_id": 4,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 5,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 6,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 7,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
            [1, 4, 5],
            [1, 4, 6],
            [1, 4, 7],
            [1, 5, 6],
            [1, 5, 7],
            [1, 6, 7],
        ]
        self.assertCountEqual(expected, result)

    def test_get__with_3_block_and_4_items__returns_list_with_18_items(self):
        # Arrange
        blocks = [
            [
                {
                    "_id": 0,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 1,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 2,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 3,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
            [
                {
                    "_id": 4,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 5,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 6,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 7,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
            [
                {
                    "_id": 8,
                    "name": "product1",
                    "description": "description1",
                    "price": "11.1"
                },
                {
                    "_id": 9,
                    "name": "product2",
                    "description": "description2",
                    "price": "22.2"
                },
                {
                    "_id": 10,
                    "name": "product3",
                    "description": "description1",
                    "price": "33.3"
                },
                {
                    "_id": 11,
                    "name": "product4",
                    "description": "description1",
                    "price": "44.4"
                },
            ],
        ]
        # Act
        result = self.pair_range.get(blocks)
        # Assert
        expected = [
            [0, 0, 1],
            [0, 0, 2],
            [0, 0, 3],
            [0, 1, 2],
            [0, 1, 3],
            [0, 2, 3],
            [1, 4, 5],
            [1, 4, 6],
            [1, 4, 7],
            [1, 5, 6],
            [1, 5, 7],
            [1, 6, 7],
            [2, 8, 9],
            [2, 8, 10],
            [2, 8, 11],
            [2, 9, 10],
            [2, 9, 11],
            [2, 10, 11],
        ]
        self.assertCountEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
