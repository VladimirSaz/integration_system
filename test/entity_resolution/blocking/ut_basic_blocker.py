import unittest
from pyspark.sql import *
from tools.test_tools import name_of_test
from test_spark import TestSpark
from entity_resolution.blocking.basic_blocker import Blocker
from comparators.levenshtein_similarity import LevenshteinSimilarity


class TestingClass(TestSpark):

    def test_get_blocks__with_empty_comparators_and_empty_dataframes__returns_empty_list(self):
        # Arrange
        # Act
        blocker = Blocker(comparators=[], dataframes=[])
        result = blocker.get_blocks()
        # Assert
        expected = []
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_empty_comparators_and_2_dataframe_that_contains_3_item__"
               "returns_1_list_with_all_objects")
    def test_get_blocks__with_empty_comparators_and_2_dataframe__returns_1_list(self):
        # Arrange
        row1 = Row(name="product1", description="description of product1", price="11.1")
        row2 = Row(name="product2", description="description of product2", price="22.2")
        row3 = Row(name="product3", description="description of product3", price="33.3")
        df1 = self.sqlContext.createDataFrame([row1])
        df2 = self.sqlContext.createDataFrame([row2, row3])
        # Act
        blocker = Blocker(comparators=[], dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {
                    "name": "product1",
                    "description": "description of product1",
                    "price": "11.1"
                },
                {
                    "name": "product2",
                    "description": "description of product2",
                    "price": "22.2"
                },
                {
                    "name": "product3",
                    "description": "description of product3",
                    "price": "33.3"
                },
            ]
        ]
        self.assertCountEqual(result, expected)

    def test_get_blocks__with_1_comparator_and_empty_dataframes__returns_empty_list(self):
        # Arrange
        comparators = self._get_strong_comparator()
        # Act
        blocker = Blocker(comparators=comparators, dataframes=[])
        result = blocker.get_blocks()
        # Assert
        expected = []
        self.assertEqual(result, expected)

    def _get_strong_comparator(self):
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            }
        ]
        return [LevenshteinSimilarity(fields=fields)]

    def test_get_blocks__1_comparator_2_equal_objects__returns_1_list(self):
        # Arrange
        comparators = self._get_strong_comparator()
        row1 = Row(fieldName="abcdefghjijklmnop")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName="abcdefghjijklmnop")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName": "abcdefghjijklmnop"},
                {"fieldName": "abcdefghjijklmnop"}
            ]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_1_comparator_and_2_different_objects_in_dataframes__"
               "returns_2_lists")
    def test_get_blocks__with_1_comparator_and_2_different_objects_in_dataframes__returns_2_lists(self):
        # Arrange
        comparators = self._get_strong_comparator()
        row1 = Row(fieldName="abcdefghjijklmnop")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName="01234567890")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [{"fieldName": "abcdefghjijklmnop"}],
            [{"fieldName": "01234567890"}]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_1_comparator_and_2_nearly_equal_objects_in_dataframes__"
               "returns_1_list")
    def test_get_blocks__with_2_nearly_equal_objects__returns_1_list(self):
        # Arrange
        comparators = self._get_strong_comparator()
        row1 = Row(fieldName="abcefghjklmn0123456789")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName="abcefghjklmn0123456999")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName": "abcefghjklmn0123456789"},
                {"fieldName": "abcefghjklmn0123456999"}
            ],
        ]
        self.assertEqual(result, expected)

    def test_get_blocks__with_2_comparator_and_no_items__returns_1_list(self):
        # Arrange
        comparators = self._get_2_comparators_on_different_fields()
        # Act
        blocker = Blocker(comparators=comparators, dataframes=[])
        result = blocker.get_blocks()
        # Assert
        expected = []
        self.assertEqual(result, expected)

    def _get_2_comparators_on_different_fields(self):
        config_for_field1 = [
            {
                "name": "fieldName1",
                "minimum_score": "0.85"
            },
        ]
        config_for_field2 = [
            {
                "name": "fieldName2",
                "minimum_score": "0.85"
            },
        ]
        return [
            LevenshteinSimilarity(fields=config_for_field1),
            LevenshteinSimilarity(fields=config_for_field2)
        ]

    def test_get_blocks__2_comparator_2_equal_objects__returns_1_list(self):
        # Arrange
        comparators = self._get_2_comparators_on_different_fields()

        row1 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567890")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567890")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567890"},
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567890"}
            ]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__with_2_comparator_and_2_objects_that_equal_"
               "on_one_attribute_and_nearly_equal_on_other_attribute__"
               "returns_1_list")
    def test_get_blocks__2_comparator_2_equal_objects__returns_1_list_1(self):
        # Arrange
        comparators = self._get_2_comparators_on_different_fields()

        row1 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567890")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567891")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567890"},
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567891"}
            ]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_2_comparator_and_2_objects_that_differ_on_one_attribute__"
               "returns_2_lists")
    def test_get_blocks__2_comparator_2_different_objects__returns_2_lists_1(self):
        # Arrange
        comparators = self._get_2_comparators_on_different_fields()

        row1 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567890")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName1="abcdefghjijklmnop", fieldName2="0000000000")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567890"},
            ],
            [
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "0000000000"}
            ]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_2_comparator_and_2_objects_that_differ_on_two_attributes__"
               "returns_2_lists")
    def test_get_blocks__2_comparator_2_different_objects__returns_2_lists_2(self):
        # Arrange
        comparators = self._get_2_comparators_on_different_fields()

        row1 = Row(fieldName1="abcdefghjijklmnop", fieldName2="1234567890")
        df1 = self.sqlContext.createDataFrame([row1])
        row2 = Row(fieldName1="zxyqrstw", fieldName2="0000000000")
        df2 = self.sqlContext.createDataFrame([row2])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName1": "abcdefghjijklmnop", "fieldName2": "1234567890"},
            ],
            [
                {"fieldName1": "zxyqrstw", "fieldName2": "0000000000"}
            ]
        ]
        self.assertEqual(result, expected)

    @name_of_test("get_blocks__"
               "with_1_comparator_and_10_objects_2_of_them_are_equal_and_"
               "other_4_are_equal_and_other_4_equal_among_each_other__"
               "returns_3_lists_lengths_of_2_and_4_and_4_respectively")
    def test_get_blocks__2_comparator_10_different_objects__returns_3_lists(self):
        # Arrange
        comparators = self._get_strong_comparator()

        df1 = self.sqlContext.createDataFrame([
            Row(fieldName="abcdefghjijklmnop", fieldName2="1234567890"),
            Row(fieldName="abcdefghjijklmnop", fieldName2="1234567890"),

            Row(fieldName="abcde1234", fieldName2="2222222222"),
            Row(fieldName="abcde1234", fieldName2="2222222222"),
        ])
        df2 = self.sqlContext.createDataFrame([
            Row(fieldName="abcde1234", fieldName2="2222222222"),
            Row(fieldName="abcde1234", fieldName2="2222222222"),

            Row(fieldName="1234567890", fieldName2="3333333333"),
        ])
        df3 = self.sqlContext.createDataFrame([
            Row(fieldName="1234567890", fieldName2="3333333333"),
            Row(fieldName="1234567890", fieldName2="3333333333"),
            Row(fieldName="1234567890", fieldName2="3333333333"),
        ])

        # Act
        blocker = Blocker(comparators=comparators, dataframes=[df1, df2, df3])
        result = blocker.get_blocks()
        # Assert
        expected = [
            [
                {"fieldName": "abcdefghjijklmnop", "fieldName2": "1234567890"},
                {"fieldName": "abcdefghjijklmnop", "fieldName2": "1234567890"},
            ],
            [
                {"fieldName": "abcde1234", "fieldName2": "2222222222"},
                {"fieldName": "abcde1234", "fieldName2": "2222222222"},
                {"fieldName": "abcde1234", "fieldName2": "2222222222"},
                {"fieldName": "abcde1234", "fieldName2": "2222222222"},
            ],
            [
                {"fieldName": "1234567890", "fieldName2": "3333333333"},
                {"fieldName": "1234567890", "fieldName2": "3333333333"},
                {"fieldName": "1234567890", "fieldName2": "3333333333"},
                {"fieldName": "1234567890", "fieldName2": "3333333333"},

            ]
        ]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
