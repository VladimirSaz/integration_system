import unittest
import warnings

from pyspark import SQLContext
from pyspark.sql import SparkSession
import datetime

class TestSpark(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        warnings.simplefilter("ignore", ResourceWarning)
        cls.spark = SparkSession.builder.master("local[*]").appName("PySparkShell").getOrCreate()
        cls.spark.sparkContext.setCheckpointDir('/Users/vladimir/Desktop/Workspace/Development/integration_system/data/checkpoint')
        cls.sqlContext = SQLContext(cls.spark)

    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()

    def setUp(self):
        warnings.simplefilter("ignore", ResourceWarning)

    def assertEqualDataframes(self, expected, result):
        self.assertCountEqual(
            self.convert_to_list_of_dicts(expected),
            self.convert_to_list_of_dicts(result)
        )

    @staticmethod
    def convert_to_list_of_dicts(rows):
        result = []
        for row in rows:
            result.append(row.asDict())
        return result
