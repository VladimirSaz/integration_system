import logging

import pytest
import warnings

from pyspark import SparkConf
from pyspark.sql import SparkSession


def quiet_py4j():
    """Suppress spark logging for the test context."""
    logger = logging.getLogger('py4j')
    logger.setLevel(logging.WARN)


@pytest.fixture(scope="session")
def spark_session(request):
    """Fixture for creating a spark context."""

    warnings.simplefilter("ignore", ResourceWarning)
    conf = SparkConf()
    conf.set("spark.executor.memory", "1g")
    spark = SparkSession.builder.master("local[*]").config(conf=conf).appName("PySparkShell").getOrCreate()
    spark.sparkContext.setCheckpointDir(
        '/Users/vladimir/Desktop/Workspace/Development/integration_system/data/checkpoint'
    )
    request.addfinalizer(lambda: spark.stop())

    quiet_py4j()
    return spark

