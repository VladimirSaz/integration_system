import unittest
from comparators.factory import ComparatorFactory
from comparators.levenshtein_similarity import LevenshteinSimilarity
from tools.test_tools import name_of_test


class TestComparatorFactory(unittest.TestCase):

    def setUp(self):
        self.comparator_factory = ComparatorFactory()

    def test_get__with_empty_string__raises_value_exception(self):
        # Arrange
        params = ""
        # Act
        is_value_error = False
        try:
            self.comparator_factory.get(params)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_get__with_empty_dict__raises_value_exception(self):
        # Arrange
        params = {}
        # Act
        is_value_error = False
        try:
            self.comparator_factory.get(params)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_get__with_non_existing_metric_name_string__raises_value_exception(self):
        # Arrange
        params = {
            'class_name': "SomeNonExistingMetric",
            'arguments': []
        }
        # Act
        is_value_error = False
        try:
            self.comparator_factory.get(params)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("get__"
               "with_levenshtein_similarity_metric_without_argument_list__"
               "raises_value_exception")
    def test_get__with_incorrect_params__raises_value_exception(self):
        # Arrange
        params = {
            'name': "SomeNonExistingMetric",
        }
        # Act
        is_value_error = False
        try:
            self.comparator_factory.get(params)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    # WIP
    @name_of_test("test_get__"
               "with_levenshtein_similarity_metric_"
               "with_argument_minimum_score_and_fields__"
               "returns_LevenshteinSimilarity_instance_with_arguments"
                  )
    def test_get__with_LevenshteinSimilarity_and_arguments__returns_instance(self):
        # Arrange
        params = {
            'name': "comparators.levenshtein_similarity.LevenshteinSimilarity",
            'arguments': {
                'fields': [
                    {
                        "name": "fieldName",
                        "minimum_score": "0.85"
                    },
                    {
                        "name": "otherFieldName",
                        "minimum_score": "0.80"
                    }
                ]
            }
        }
        # Act
        result = self.comparator_factory.get(params)
        # Assert
        expected_fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            },
            {
                "name": "otherFieldName",
                "minimum_score": "0.80"
            }
        ]
        self.assertIsInstance(result, LevenshteinSimilarity)
        self.assertEqual(result.fields, expected_fields)

        if __name__ == '__main__':
            unittest.main()
