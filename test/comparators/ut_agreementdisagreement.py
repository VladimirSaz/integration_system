import unittest
from comparators.agreement_disagreement import AgreementDisagreement
from tools.test_tools import name_of_test
from datetime import datetime


class TestAgreementDisagreement(unittest.TestCase):

    def test_get_similarities__with_1_field_missing_in_1_item__raises_value_exception(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": "abcefghjklmn0123456789",
        }
        object2 = {
            "otherField": "11111111112",
        }
        # Act
        is_value_error = False
        agreement_disagreement = AgreementDisagreement(**parameters)
        try:
            agreement_disagreement.get_similarities(object1, object2)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_get_similarities__with_1_field_present_and_equal_in_both_items__returns_1(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": "abcefghjklmn0123456789",
        }
        object2 = {
            "fieldName": "abcefghjklmn0123456789",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 1
        self.assertTrue(len(result), 1)
        self.assertEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_1_field_and_1_item_have_none_as_value_of_"
        "field_and_some_string__"
        "returns_0"
    )
    def test_get_similarities__none_and_some_string_as_value_of_field__return_0(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": None,
        }
        object2 = {
            "fieldName": "abcefghjklmn0123456789",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0
        self.assertEqual(expected, result[0])

    def test_get_similarities__with_1_field_present_and_not_equal__returns_0(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": "abc",
        }
        object2 = {
            "fieldName": "cde",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0
        self.assertEqual(expected, result[0])

    def test_are_similar__similarity_lower_than_threshold__returns_false(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": "abc",
        }
        object2 = {
            "fieldName": "cde",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.are_similar(object1, object2)
        # Assert
        self.assertFalse(result)

    def test_are_similar__similarity_greater_or_equal_than_threshold__returns_true(self):
        # Arrange
        parameters = {
            'fields': [{"name": "fieldName"}],
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName": "abc",
        }
        object2 = {
            "fieldName": "abc",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.are_similar(object1, object2)
        # Assert
        self.assertTrue(result)

    @name_of_test(
        "get_similarities__"
        "with_2_fields_missing_in_1_item__"
        "raises_value_exception"
    )
    def test_get_similarities__with_2_fields_missing__raises_value_exception(self):
        # Arrange
        fields = [
            {"name": "fieldName1"},
            {"name": "fieldName2"},
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85"
        }
        object1 = {
            "otherField": "abc",
        }
        object2 = {
            "otherField": "abc",
        }
        # Act
        is_value_error = False
        agreement_disagreement = AgreementDisagreement(**parameters)
        try:
            agreement_disagreement.get_similarities(object1, object2)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_get_similarities__with_2_fields_matching__returns_1(self):
        # Arrange
        fields = [
            {"name": "fieldName1"},
            {"name": "fieldName2"},
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85"
        }
        object1 = {
            "fieldName1": "abc",
            "fieldName2": "123",
        }
        object2 = {
            "fieldName1": "abc",
            "fieldName2": "123",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 1
        self.assertAlmostEqual(expected, result[0])

    def test_get_similarities__with_1_of_2_fields_matching__returns_0_5(self):
        # Arrange
        fields = [
            {"name": "fieldName1"},
            {"name": "fieldName2"},
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85"
        }

        object1 = {
            "fieldName1": "abc",
            "fieldName2": "123",
        }
        object2 = {
            "fieldName1": "abc",
            "fieldName2": "346",
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.5
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_r282_matching_equal_weights__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_4_fields_equal_weights__returns_expected_similarity(self):
        # Arrange
        fields = [
            {"name": "name"},
            {"name": "profession"},
            {"name": "home_airport"},
            {"name": "co_travellers"}
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85"
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004', '%Y')

        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008', '%Y')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.5
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_r282_matching_"
        "with_custom_weights__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_4_fields_and_custom_weights__returns_expected_similarity(self):
        # Arrange
        fields = [
            {"name": "name", "weight": 0.4},
            {"name": "profession", "weight": 0.2},
            {"name": "home_airport", "weight": 0.2},
            {"name": "co_travellers", "weight": 0.2}
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004', '%Y')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008', '%Y')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.6
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_"
        "r282_matching_with_custom_weights_and_agreements_and_disagreement__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_4_fields_weights_agreements__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004', '%Y')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008', '%Y')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.71346704871
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_r282_matching_with_"
        "custom_weights_and_agreements_and_disagreement_for_more_3_periods__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_4_fields_4_periods__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 1,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 1,
                    'span_type': 'year',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 1,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004', '%Y')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008', '%Y')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.6535378173
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_r282_matching_with_"
        "equal_weights_and_agreements_and_disagreement__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_temporal_and_equal_weights__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004', '%Y')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008', '%Y')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.62370649106
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__"
        "with_4_fields_from_travel_example_r280_and_r282_matching_with_"
        "custom_weights_and_agreements_and_disagreement_for_years_"
        "but_fields_from_different_months_ie_less_than_1_period__"
        "returns_expected_similarity_as_without_temporal"
    )
    def test_get_similarities__with_records_of_less_than_1_period__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'year',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2,
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2008 9', '%Y %m')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2008 12', '%Y %m')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.6
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__with_4_fields_from_travel_example_r280_and_r282_"
        "matching_with_custom_weights_and_agreements_and_disagreement_"
        "for_days_and_fields_from_different_days__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_agreements_for_days__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'day',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'day',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'day',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004 10 8', '%Y %m %d')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2004 10 12', '%Y %m %d')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.71346704871
        self.assertAlmostEqual(expected, result[0])

    @name_of_test(
        "get_similarities__with_4_fields_from_travel_example_r280_and_r282_"
        "matching_with_custom_weights_and_agreements_and_disagreement_"
        "for_month_and_fields_from_different_months__"
        "returns_expected_similarity"
    )
    def test_get_similarities__with_agreements_for_months__returns_expected_similarity(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004 04 8', '%Y %m %d')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2004 08 12', '%Y %m %d')
        }
        # Act
        agreement_disagreement = AgreementDisagreement(**parameters)
        result = agreement_disagreement.get_similarities(object1, object2)
        # Assert
        expected = 0.71346704871
        self.assertAlmostEqual(expected, result[0])

    def test_get_similarities__with_temporal_for_unknown__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }

        object1 = {
            "name": "Robert James",
            "profession": "Programmer",
            "home_airport": "Chicago",
            "co_travellers": "David, Black",
            "date": datetime.strptime('2004 04 8', '%Y %m %d')
        }

        object2 = {
            "name": "Robert James",
            "profession": "Manager",
            "home_airport": "Chicago",
            "co_travellers": "Larry,David",
            "date": datetime.strptime('2004 08 12', '%Y %m %d')
        }
        # Act
        is_value_error = False
        agreement_disagreement = AgreementDisagreement(**parameters)
        try:
            agreement_disagreement.get_similarities(object1, object2)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_no_fields__raises_value_exception(self):
        # Arrange
        fields = []
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_custom_weight_less_or_equal_to_0__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": -0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_agreement_decay_lower_than_0__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': -0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_disagreement_decay_greater_than_1__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': 1.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_disagreement_decay_lower_than_0__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': -0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_agreement_decay_greater_than_1__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 1.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'some other unknown',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'unknown',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }
        # Act
        is_value_error = False
        try:
            AgreementDisagreement(**parameters)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)


if __name__ == '__main__':
    unittest.main()
