import unittest
from comparators.levenshtein_similarity import LevenshteinSimilarity
from tools.test_tools import name_of_test


class TestLevenshteinDistanceInit(unittest.TestCase):

    def test_init__with_empty_args__raises_value_exception(self):
        # Arrange
        # Act
        is_value_error = False
        try:
            LevenshteinSimilarity()
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_argument_fields__raises_value_exception(self):
        # Arrange
        fields = []
        # Act
        is_value_error = False
        try:
            LevenshteinSimilarity(fields=fields)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_init__with_argument_fields_with_empty_name__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": ""
            }
        ]
        # Act
        is_value_error = False
        try:
            LevenshteinSimilarity(fields=fields)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("test_init__with_argument_fields_with_name_"
               "and_no_minimum_score__raises_value_exception")
    def test_init__with_argument_fields_with_name_and_no_minimum_score__raises_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "fieldName"
            }
        ]
        # Act
        is_value_error = False
        try:
            LevenshteinSimilarity(fields=fields)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)


    @name_of_test("test_init__with_argument_fields_with_only_1_valid_field_"
               "and_1_invalid_field_with_no_field_minimum_score"
               "__raises_value_exception")
    def test_init_1(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.12"
            },
            {
                "name": "otherFieldName",
            }
        ]
        # Act
        is_value_error = False
        try:
            LevenshteinSimilarity(fields=fields)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("test_init__with_argument_valid_fields"
               "__returns_instance_contains_fields_field")
    def test_init_2(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.12"
            },
            {
                "name": "otherFieldName",
                "minimum_score": "0.34"
            }
        ]
        # Act
        result = LevenshteinSimilarity(fields=fields)
        # Assert
        expected = fields
        self.assertEqual(result.fields, expected)


class TestLevenshteinDistanceMethods(unittest.TestCase):
    def test_are_similar__two_empty_dicts__raise_value_exception(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {}
        object2 = {}
        # Act
        is_value_error = False
        try:
            self.comparator.are_similar(object1, object2)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("are_similar__two_dicts_without_field_name_with_comparison_"
               "over_field_name__raise_value_exception")
    def test_are_similar_2(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {"otherFieldName": "abc"}
        object2 = {"otherFieldName": "abc"}
        # Act
        is_value_error = False
        try:
            self.comparator.are_similar(object1, object2)
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("are_similar__two_dicts_with_field_name_that_is_nearly"
               "_equal_with_comparison_over_field_name__returns_true")
    def test_are_similar_3(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {"fieldName": "abcefghjklmn0123456789"}
        object2 = {"fieldName": "abcefghjklmn0123456999"}
        # Act
        result = self.comparator.are_similar(object1, object2)
        # Assert
        self.assertTrue(result)

    @name_of_test("are_similar__two_dicts_with_field_name_that_are_not_similar_"
               "with_comparison_over_field_name__returns_false")
    def test_are_similar_4(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {"fieldName": "abcefghjklmn"}
        object2 = {"fieldName": "0123456999"}
        # Act
        result = self.comparator.are_similar(object1, object2)
        # Assert
        self.assertFalse(result)

    @name_of_test("are_similar__two_dicts_with_field_name_that_are_similar_and_"
               "other_field_that_are_not_similar_with_comparison_"
               "over_field_name_and_description__returns_false")
    def test_are_similar_5(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            },
            {
                "name": "otherFieldName",
                "minimum_score": "1.00"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {
            "fieldName": "abcefghjklmn0123456789",
            "otherFieldName": "11111111111",
        }
        object2 = {
            "fieldName": "abcefghjklmn0123456999",
            "otherFieldName": "11111222222"
        }
        # Act
        result = self.comparator.are_similar(object1, object2)
        # Assert
        self.assertFalse(result)

    @name_of_test("are_similar__two_dicts_with_field_name_that_"
               "are_similar_and_other_field_that_are_"
               "nearly_equal_with_comparison_over_field_name_"
               "and_description__returns_true")
    def test_are_similar__two_dicts_with_field_name_that_are_similar_and_field_description_that_are_nearly_equal_with_comparison_over_field_name_and_description__returns_true(self):
        # Arrange
        fields = [
            {
                "name": "fieldName",
                "minimum_score": "0.85"
            },
            {
                "name": "otherFieldName",
                "minimum_score": "0.80"
            }
        ]
        self.comparator = LevenshteinSimilarity(fields=fields)
        object1 = {
            "fieldName": "abcefghjklmn0123456789",
            "otherFieldName": "11111111111",
        }
        object2 = {
            "fieldName": "abcefghjklmn0123456789",
            "otherFieldName": "11111111112"
        }
        # Act
        result = self.comparator.are_similar(object1, object2)
        # Assert
        self.assertTrue(result)

if __name__ == '__main__':
    unittest.main()
