import unittest
from scheme.json_validator import Validator


class TestUM(unittest.TestCase):

    def setUp(self):
        schema = {
            "type": "object",
            "properties": {
                "firstName": {
                    "type": "string"
                },
                "age": {
                    "type": "integer"
                }
            },
            "additionalProperties": False,
            "required": ["firstName", "age"]
        }
        self.validator = Validator(schema)

    def test_GetRule_WithNoConfigPathAndWithRuleName_ThrowsExceptionValueError(self):
        is_value_error = False
        try:
            self.validator.is_match("")
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    def test_is_match_WithValidSchemeAndEmptyObjectAsDict_ReturnsFalse(self):
        self.assertFalse(self.validator.is_match({}))

    def test_is_match_WithValidSchemeAndNotMatchingObject_ReturnsFalse(self):
        self.assertFalse(self.validator.is_match({
            "firstName": "abc"
        }))

    def test_is_match_WithValidSchemeAndMatchingObject_ReturnsTrue(self):
        self.assertTrue(self.validator.is_match({
            "firstName": "abc",
            "age": 21
        }))


if __name__ == '__main__':
    unittest.main()
