import unittest
from content_parser.rule import Rule
from tools.test_tools import name_of_test
from unittest.mock import MagicMock


class TestUM(unittest.TestCase):

    def setUp(self):
        path = ""
        self.rule = Rule(path)

    @name_of_test("test_GetRule_WithNoConfigPathAndWithRuleName_ThrowsExceptionValueError")
    def test_1(self):
        # Arrange
        self.rule.path = None
        # Act
        is_value_error = False
        try:
            self.rule.get()
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("test_GetRule_WithNoConfigPathAndNoRuleName_ThrowsExceptionValueError")
    def test_3(self):
        # Arrange
        self.rule.path = None
        # Act
        is_value_error = False
        try:
            self.rule.get()
        except ValueError:
            is_value_error = True
        # Assert
        self.assertTrue(is_value_error)

    @name_of_test("GetRule_WithConfigPathAndRuleName_ReturnsExpectedRule")
    def test_4(self):
        # Arrange
        self.rule.path = './data/'
        rule_content = {
            "someField1": "value1",
            "someField2": "value2"
        }
        self.rule._read_rule_from_json_file = MagicMock(return_value=rule_content)
        # Act
        result = self.rule.get()
        # Assert
        expected = rule_content
        self.assertDictEqual(result, expected)

    @name_of_test("GetRule_WithConfigPathAndDifferentRuleNameAndWhileAlreadyLoadedRule_ReturnsFirstLoadedRuleAndNotNewOne")
    def test_5(self):
        # Arrange
        self.rule.path = './data/'
        first_rule_content = {
            "someField1": "value1",
            "someField2": "value2"
        }
        second_rule_content = {}
        self.rule._read_rule_from_json_file = MagicMock(return_value=first_rule_content)
        # Act
        self.rule.get()
        self.rule.name = 'different_rule'
        self.rule._read_rule_from_json_file = MagicMock(return_value=second_rule_content)
        result = self.rule.get()
        # Assert
        expected = first_rule_content
        self.assertDictEqual(result, expected)

    @name_of_test("Parse_WithHtmlWith2ItemsAndParcelRuleNotForThisHtml_Extracted0Items")
    def test_6(self):
        # Arrange
        rule_content = {
            "extraction_rule": {
                "someField1": "value1",
                "someField2": "value2"
            }
        }
        self.rule.get = MagicMock(return_value=rule_content)
        html = self.get_sample_html()
        # Act
        result = self.rule.parse(html)
        # Assert
        expected = {}
        self.assertDictEqual(result, expected)

    def get_sample_html(self):
        return """
... <!DOCTYPE html>
... <html>
... <head>
...     <title>Sample document to test parslepy</title>
...     <meta http-equiv="content-type" content="text/html;charset=utf-8" />
... </head>
... <body>
... <h1 id="main">Heading1</h1>
... <ul>
...     <li class="newsitem"><a href="/article-001.html">element1</a></li>
...     <li class="newsitem"><a href="/article-002.html">element2</a></li>
...     <li class="newsitem"><a href="/article-003.html">element3</a> <span class="fresh">New!</span></li>
... </ul>
... </body>
... </html>
... """

    @name_of_test("Parse_WithHtmlWith1ItemsAndParcelRuleForThisHtml_Extracted1Items")
    def test_7(self):
        # Arrange
        rule_content = {
            "extraction_rule": {
                "someField1": "#main"
            }
        }
        self.rule.get = MagicMock(return_value=rule_content)
        html = self.get_sample_html()
        # Act
        result = self.rule.parse(html)
        # Assert
        expected = {'someField1': 'Heading1'}
        self.assertDictEqual(result, expected)

    @name_of_test("Parse_WithHtmlWith3ItemsAndParcelRuleForThisHtml_Extracted2Items")
    def test_8(self):
        # Arrange
        rule_content = {
            "extraction_rule": {
                "elements(.newsitem)": [{
                    'someField1': "a"
                }]
            }
        }
        self.rule.get = MagicMock(return_value=rule_content)
        html = self.get_sample_html()
        # Act
        result = self.rule.parse(html)
        # Assert
        expected = {
            'elements': [
                {'someField1': 'element1'},
                {'someField1': 'element2'},
                {'someField1': 'element3'}
            ]
        }
        self.assertDictEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
