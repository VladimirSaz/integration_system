'''
work in progress no idea what this tests for yet.
Delete if not needed TODO
'''
from test_spark import TestSpark
from content_parser.entity_identification.html_entity_learner import HtmlEntityLearner
from pprint import pprint
from pytest_cases import fixture_ref, fixture_plus, parametrize_plus
from pyspark.sql import *
from content_parser.entity_identification.similarity.strong_similarity import StrongSimilarity
from metrics.levenshtein import Levenshtein
from content_parser.entity_identification.text_processor import TextProcessor


@fixture_plus(scope='function')
def html_entity_learner(spark_session):
    return HtmlEntityLearner(
        similarity=Levenshtein(),
        sqlContext=SQLContext(spark_session),
        kb=None,
        threshold_weak_similarity=0.8,
        threshold_strong_similarity=0.8,
        page_support=5
    )


def test_get_weakly_similar_pairs(html_entity_learner):
    matching = {
        'attribute1':
            [
                {
                    'node': {
                        '_id': 0,
                        'value': None,
                        'path': None
                    },
                    'entry': {
                        '_id': 0
                    },
                    'similarity': 1.0
                },
            ],
        'attribute2': [{}],
    }
    print(html_entity_learner)
    result = html_entity_learner.get_weakly_similar_pairs(
        matching, attribute='attribute1'
    )
    expected = [[0, 0]]
    assert expected == result


# def test_get_weakly_similar_pairs_on_real_data():
# def test_get_weakly_similar_pairs_with_more_data():

# def test_get_similarity_for_one_attribute_and_one_position():
#     kb = [
#         Row(
#             _id=0,
#             actor_address='Hampington street of New York, building 1, flat 24'
#         ),
#     ]
#     extracted = [
#         {
#             '_id':
#                 0,
#             'path':
#                 'html/body/ul/li[1]',
#             'value':
#                 'Actor leaves at HampingtAn street buildEng 1, happily with wife in flat 24',
#             'url':
#                 'actor_address_1/actor0.html'
#         }
#     ]
#     weakly_similar = [[0, 0]]
#     attribute = 'actor_address'
#
#     strong_similarity = StrongSimilarity(
#         similarity=Levenshtein(), threshold_word_similarity=0.5
#     )
#     result = strong_similarity.calculate(
#         kb_entries=kb,
#         extracted_entries=extracted,
#         weakly_similar=weakly_similar,
#         attribute=attribute
#     )
#     matching_parts_kb = ['Hampington street', 'building 1,', 'flat 24']
#     matching_parts_extracted = ['HampingtAn street', 'buildEng 1,', 'flat 24']
#     tp = TextProcessor()
#     expected_similarity = Levenshtein().calculate(
#         tp.join(matching_parts_kb), tp.join(matching_parts_extracted)
#     )
#     assert result == (
#         {
#             'kb': 0,
#             'extracted': 0,
#             'similarity': expected_similarity
#         },
#     )
#     pass
#     # kb = [
#     #     Row(_id=0, actor_address='a1, abc1, ee1, rr1 abc1,'),
#     #     Row(_id=1, actor_address='a1, abc1, ee1, rr1 abc1,'),
#     # ]
#     # extracted = [
#     #     {
#     #         '_id': 0,
#     #         'path': 'html/body/ul/li[1]',
#     #         'value': 'abc1, qq12 ee1, abc1, rr1 rr1 gg',
#     #         'url': 'actor_address_1/actor0.html'
#     #     }, {
#     #         '_id': 1,
#     #         'path': 'html/body/ul/li[1]',
#     #         'value': 'abc1, qq12 ee1, abc1, rr1 rr1 gg',
#     #         'url': 'actor_address_1/actor0.html'
#     #     }
#     # ]
#     # weakly_similar = []
#     # attribute = 'actor_address'
#     #
#     # strong_similarity = StrongSimilarity(
#     #     similarity=Levenshtein(), threshold_word_similarity=0.5
#     # )
#     # result = strong_similarity.calculate(
#     #     kb_entries=kb,
#     #     extracted_entries=extracted,
#     #     weakly_similar=weakly_similar,
#     #     attribute=attribute
#     # )
#     # assert result == (
#     #     {
#     #         'kb': 0,
#     #         'extracted': 0,
#     #         'similarity': 0.4375
#     #     },
#     #     {
#     #         'kb': 0,
#     #         'extracted': 1,
#     #         'similarity': 0.4375
#     #     },
#     #     {
#     #         'kb': 1,
#     #         'extracted': 0,
#     #         'similarity': 0.4375
#     #     },
#     #     {
#     #         'kb': 1,
#     #         'extracted': 1,
#     #         'similarity': 0.4375
#     #     },
#     # )
