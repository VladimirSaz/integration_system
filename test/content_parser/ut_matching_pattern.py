import pytest
from pyspark.sql import *
from content_parser.entity_identification.similarity.matching_pattern import MatchingPattern


def test_get_sequences_of_matching_and_not_matching_words():
    # Arrange
    kb = Row(
        _id=0,
        actor_address='Hampington street of New York, building 1, flat 24'
    )
    extracted = {
        'path':
            '/html/body/ul/li[1]',
        'value':
            'Actor leaves at Hampington street building 1, happily with wife in flat 24',
        'url':
            None,
        '_id':
            0
    }
    attribute = 'actor_address'
    matching_words = {
        ('kb_1', 'extracted_4'),
        ('kb_2', 'extracted_5'),
        ('kb_6', 'extracted_6'),
        ('kb_7', 'extracted_7'),
        ('kb_8', 'extracted_12'),
        ('kb_9', 'extracted_13'),
    }
    # Act
    result = MatchingPattern().get(
        kb=kb,
        extracted=extracted,
        attribute=attribute,
        graph_edges=matching_words
    )
    # Assert
    expected = {
        'kb':
            {
                '_id': 0,
                'sequence':
                    [
                        'Hampington street', 'of New York,', 'building 1,',
                        'flat 24'
                    ],
                'pattern': [2, 0, 3, 5]
            },
        'extracted':
            {
                '_id': 0,
                'sequence':
                    [
                        'Actor leaves at', 'Hampington street', 'building 1,',
                        'happily with wife in', 'flat 24'
                    ],
                'pattern': [0, 1, 3, 0, 4]
            }
    }
    assert result == expected


def test_get_sequences_where_in_graph_more_words_than_in_matching_words_expected_to_be_less_in_matching_sequence(
):
    kb = Row(
        _id=0,
        actor_address='Hampington street of New York, building 1, flat 24'
    )
    extracted = {
        '_id':
            0,
        'path':
            '/html/body/ul/li[1]',
        'value':
            'Actor leaves at Hampington street building 1, happily with wife in flat 24',
        'url':
            None
    }
    attribute = 'actor_address'
    matching_words = {
        ('kb_2', 'extracted_5'),
        ('kb_7', 'extracted_7'),
        ('kb_8', 'extracted_12'),
        ('kb_9', 'extracted_13'),
    }
    # Act
    result = MatchingPattern().get(
        kb=kb,
        extracted=extracted,
        attribute=attribute,
        graph_edges=matching_words
    )
    expected = {
        'kb':
            {
                '_id': 0,
                'sequence':
                    [
                        'Hampington', 'street', 'of New York, building', '1,',
                        'flat 24'
                    ],
                'pattern': [0, 2, 0, 4, 6]
            },
        'extracted':
            {
                '_id': 0,
                'sequence':
                    [
                        'Actor leaves at Hampington', 'street', 'building',
                        '1,', 'happily with wife in', 'flat 24'
                    ],
                'pattern': [0, 2, 0, 4, 0, 5]
            }
    }
    assert result == expected


def test_get_pattern_with_first_example_from_article():
    kb = Row(_id=0, actor_address='120 Lexington Avenue New York, NY 10016')
    extracted = {
        'path':
            '/html/body/ul/li[1]',
        'value':
            '120 Lexington Ave (between 28th and 29th St) New York, NY 10016',
        'url':
            None,
        '_id':
            0
    }

    attribute = 'actor_address'
    matching_words = {
        ('kb_1', 'extracted_1'),
        ('kb_2', 'extracted_2'),
        ('kb_4', 'extracted_9'),
        ('kb_5', 'extracted_10'),
        ('kb_6', 'extracted_11'),
        ('kb_7', 'extracted_12'),
    }
    # Act
    result = MatchingPattern().get(
        kb=kb,
        extracted=extracted,
        attribute=attribute,
        graph_edges=matching_words
    )
    expected = {
        'kb':
            {
                '_id': 0,
                'sequence': ['120 Lexington', 'Avenue', 'New York, NY 10016'],
                'pattern': [1, 0, 3]
            },
        'extracted':
            {
                '_id': 0,
                'sequence':
                    [
                        '120 Lexington', 'Ave (between 28th and 29th St)',
                        'New York, NY 10016'
                    ],
                'pattern': [1, 0, 3]
            }
    }
    assert result == expected


def test_get_pattern_with_second_example_from_article():
    kb = Row(_id=1, actor_address='120 Lexington Avenue New York, NY 10016')
    extracted = {
        '_id': 1,
        'path': '/html/body/ul/li[1]',
        'value': 'Lexington Ave New York, NY',
        'url': None
    }
    attribute = 'actor_address'
    matching_words = {
        ('kb_2', 'extracted_1'),
        ('kb_4', 'extracted_3'),
        ('kb_5', 'extracted_4'),
        ('kb_6', 'extracted_5'),
    }
    # Act
    result = MatchingPattern().get(
        kb=kb,
        extracted=extracted,
        attribute=attribute,
        graph_edges=matching_words
    )
    expected = {
        'kb':
            {
                '_id': 1,
                'sequence':
                    ['120', 'Lexington', 'Avenue', 'New York, NY', '10016'],
                'pattern': [0, 1, 0, 3, 0]
            },
        'extracted':
            {
                '_id': 1,
                'sequence': ['Lexington', 'Ave', 'New York, NY'],
                'pattern': [2, 0, 4]
            }
    }
    assert result == expected
