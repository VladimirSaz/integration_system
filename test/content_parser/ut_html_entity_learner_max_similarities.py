from test_spark import TestSpark
from content_parser.entity_identification.html_entity_learner import HtmlEntityLearner
from pprint import pprint
from pytest_cases import fixture_ref, fixture_plus, parametrize_plus
from pyspark.sql import *
from content_parser.entity_identification.similarity.strong_similarity import StrongSimilarity
from metrics.levenshtein import Levenshtein
from content_parser.entity_identification.text_processor import TextProcessor


@fixture_plus(scope='function')
def html_entity_learner(spark_session):
    return HtmlEntityLearner(
        similarity=Levenshtein(),
        sqlContext=SQLContext(spark_session),
        kb=None,
        threshold_weak_similarity=0.8,
        threshold_strong_similarity=0.8,
        page_support=5
    )


@fixture_plus(scope='function')
def kb():
    """
    
    the values doesnt matter since we dont compare here. We use matchings and believe
    their discretion

    """
    return [
        Row(
            _id=0,
            attribute1='some other string not matching',
            attribute2='info fields data',
            attribute3='attribute3 data',
        ),
        Row(
            _id=1,
            attribute1='a1, abc1, ee1, rr1 abc1,',
            attribute2='zxc, aaa, bbb,ccc',
            attribute3='attribute3 different data',
        ),
        Row(
            _id=3,
            attribute1='a1, abc1, ee1, rr1 abc1,',
            attribute2='zxc, aaa, bbb,ccc',
            attribute3='attribute3 different data',
        ),
    ]


@fixture_plus(scope='function')
def extracted():
    return [
        {
            '_id': 0,
            'path': 'html/body/ul/li[1]',
            'value': 'a1, abc1, ee1, rr1 abc1,',
            'url': 'actor_address_1/actor0.html',
            'attribute': 'attribute1',
        },
        {
            '_id': 1,
            'path': 'html/body/ul/li[2]',
            'value': 'some non similar string abcd1',
            'url': 'actor_address_1/actor0.html',
            'attribute': 'attribute1',
        },
        {
            '_id': 0,
            'path': 'html/body/ul/li[55]',
            'value': 'info fields data',
            'url': 'actor_address_1/actor0.html',
            'attribute': 'attribute2',
        },
        {
            '_id': 3,
            'path': 'html/body/ul/li[1]',
            'value': 'a1, abc1, ee1, rr1 abc1,',
            'url': 'actor_address_1/actor1.html',
            'attribute': 'attribute2',
        },
        {
            '_id': 4,
            'path': 'html/body/ul/li[1]',
            'value': 'some non similar string abcd1',
            'url': 'actor_address_1/actor2.html',
            'attribute': 'attribute2',
        },
        {
            '_id': 5,
            'path': 'html/body/ul/li[55]',
            'value': 'a1, abc1, ee1, rr1 abc1,',
            'url': 'actor_address_1/actor1.html',
            'attribute': 'attribute2',
        },
        {
            '_id': 0,
            'path': 'html/body/ul/li[99]',
            'value': 'info fields data',
            'url': 'actor_address_1/actor0.html',
            'attribute': 'attribute3',
        },
        {
            '_id': 3,
            'path': 'html/body/ul/li[99]',
            'value': 'info fields data',
            'url': 'actor_address_1/actor4.html',
            'attribute': 'attribute3',
        },
        {
            '_id': 4,
            'path': 'html/body/ul/li[99]',
            'value': 'info fields data',
            'url': 'actor_address_1/actor5.html',
            'attribute': 'attribute3',
        },
    ]


def test_get_max_similarity_with_no_matching(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [],
        'attribute2': [],
    }
    html_entity_learner.page_support = 0
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = set()
    assert expected == result


def test_get_max_similarity_with_1_matching_attribute_but_minimum_support(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
            'similarity': 0.5
        }],
        'attribute2': [],
    }
    html_entity_learner.page_support = 0
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {('attribute1', 'html/body/ul/li[1]')}
    assert result == expected


def test_get_max_similarity_with_2_matching_attributes_but_minimum_support(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2': [{
            'kb': 0,
            'extracted': 0,
        }],
    }
    html_entity_learner.page_support = 0
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {
        ('attribute1', 'html/body/ul/li[1]'),
        ('attribute2', 'html/body/ul/li[55]')
    }
    assert result == expected


def test_get_max_similarity_with_3_matching_attributes_but_minimum_support(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute3':
            [
                {
                    'kb': 0,
                    'extracted': 0,
                },
                {
                    'kb': 1,
                    'extracted': 3,
                },
            ],
    }
    html_entity_learner.page_support = 0
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {
        ('attribute1', 'html/body/ul/li[1]'),
        ('attribute2', 'html/body/ul/li[55]'),
        ('attribute3', 'html/body/ul/li[99]'),
    }
    assert result == expected


def test_with_3_matching_attributes_but_higher_support(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute3':
            [
                {
                    'kb': 0,
                    'extracted': 0,
                },
                {
                    'kb': 1,
                    'extracted': 3,
                },
            ],
    }
    html_entity_learner.page_support = 2
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {
        ('attribute3', 'html/body/ul/li[99]'),
    }
    assert result == expected


def test_with_3_matching_attributes_but_because_supported_different_pages_results_in_1(
    html_entity_learner, kb, extracted
):
    """
    to result attribute2+attribute3  attribute2 should have extracted id 3, but 3 have different rule
    """
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2':
            [
                {
                    'kb': 0,
                    'extracted': 0,
                },
                {
                    'kb': 1,
                    'extracted': 5,
                },
            ],
        'attribute3':
            [
                {
                    'kb': 0,
                    'extracted': 0,
                },
                {
                    'kb': 1,
                    'extracted': 3,
                },
                {
                    'kb': 3,
                    'extracted': 4,
                },
            ],
    }
    html_entity_learner.page_support = 2
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {
        ('attribute3', 'html/body/ul/li[99]'),
    }
    assert result == expected


def test_with_2_rules_to_extract_results_with_the_most_supported(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute2':
            [
                # rule1
                {
                    'kb': 0,
                    'extracted': 3,
                },
                {
                    'kb': 1,
                    'extracted': 4,
                },
                # different rule 2
                {
                    'kb': 1,
                    'extracted': 5,
                },
            ],
    }
    html_entity_learner.page_support = 2
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    rule1 = ('attribute2', 'html/body/ul/li[1]')
    expected = {
        rule1,
    }
    assert result == expected


def test_with_2_rules_but_no_result_since_support_is_too_high(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute2':
            [
                # rule1
                {
                    'kb': 0,
                    'extracted': 3,
                },
                {
                    'kb': 1,
                    'extracted': 4,
                },
                # different rule 2
                {
                    'kb': 1,
                    'extracted': 5,
                },
            ],
    }
    html_entity_learner.page_support = 10
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = set()
    assert result == expected


def test_with_3_matching_attributes_but_because_support_is_high_results_in_2_rules(
    html_entity_learner, kb, extracted
):
    matchings = {
        'attribute1': [{
            'kb': 0,
            'extracted': 0,
        }],
        'attribute2':
            [
                {
                    'kb': 1,
                    'extracted': 3,
                },
                {
                    'kb': 3,
                    'extracted': 4,
                },
            ],
        'attribute3':
            [
                {
                    'kb': 0,
                    'extracted': 0,
                },
                {
                    'kb': 1,
                    'extracted': 3,
                },
                {
                    'kb': 3,
                    'extracted': 4,
                },
            ],
    }
    html_entity_learner.page_support = 2
    result = html_entity_learner.calculate_max_similarity_matching(
        all_similarity_matchings=matchings, kb=kb, extracted=extracted
    )
    expected = {
        ('attribute2', 'html/body/ul/li[1]'),
        ('attribute3', 'html/body/ul/li[99]'),
    }
    assert result == expected
