import unittest
from pyspark.sql import *

from test_spark import TestSpark
from content_parser.entity_identification.html_entity_learner import HtmlEntityLearner
from pprint import pprint
from metrics.levenshtein import Levenshtein


class TestHtmlEntityLearner(TestSpark):
    def setUp(self):
        self.html_directory = 'test/content_parser/data/actor_address_1/'

    def test_get_nodes_from_sample_html(self):
        # Arrange
        page = Row(_id=0, document_path='test/content_parser/data/sample.html')
        # Act
        learner = HtmlEntityLearner(similarity=Levenshtein())
        nodes = learner._get_nodes(page)
        # Assert
        expected_item_count = 14
        self.assertEqual(expected_item_count, len(nodes))

    @staticmethod
    def _get_html(file_name):
        with open('test/content_parser/data/' + file_name, 'r') as f:
            return f.read()

    def test_get_nodes_from_real_html(self):
        # Arrange
        page = Row(
            _id=0, document_path='test/content_parser/data/imdb_page.html'
        )
        # Act
        learner = HtmlEntityLearner(similarity=Levenshtein())
        nodes = learner._get_nodes(page)
        # Assert
        expected_item_count = 2666
        self.assertEqual(expected_item_count, len(nodes))

    def test_compare_knowledge_base_entry_with_html(self):
        # Arrange
        df = self.sqlContext.createDataFrame(
            [
                Row(_id=0, fieldName="element1gg", fieldName2="3333333333"),
            ]
        )
        pages = self.sqlContext.createDataFrame(
            [Row(_id=0, document_path='test/content_parser/data/sample.html')]
        )
        threshold = 0.9
        # Act
        learner = HtmlEntityLearner(
            similarity=Levenshtein(),
            sqlContext=self.sqlContext,
            kb=df,
            threshold_weak_similarity=threshold
        )
        result = learner.compare(pages)
        # Assert
        expected_node = {
            '_id': 0,  # page_id, not node_id
            'path': '/html/body/ul/li[1]',
            'value': 'element1gg',
        }
        expected_keys = {'fieldName', 'fieldName2'}
        expected_nodes_number = {'fieldName': 1, 'fieldName2': 0}
        self.assertEqual(
            expected_nodes_number['fieldName'], len(result['fieldName'])
        )
        self.assertEqual(
            expected_nodes_number['fieldName2'], len(result['fieldName2'])
        )
        self.assertEqual(expected_node, result['fieldName'][0]['node'])
        self.assertEqual(expected_keys, set(result.keys()))

    def test_get_proved_rules_with_betta_0(self):
        # Arrange
        df = self.sqlContext.createDataFrame(
            [
                Row(_id=0, actor_address="a1, abc1, ee1, rr1"),
                Row(_id=1, actor_address="a2, abc2, ee2, rr2"),
                Row(_id=2, actor_address="a3, abc3, ee3, rr3"),
                Row(_id=3, actor_address="a4, abc4, ee4, rr4"),
                Row(_id=4, actor_address="a5, abc5, ee5, rr5"),
            ]
        )
        pages = self._get_dataframe_of_pages_with_ids([0, 1, 2, 3, 4])
        # Act
        learner = HtmlEntityLearner(
            sqlContext=self.sqlContext,
            kb=df,
            threshold_weak_similarity=0.8,
            page_support=0,
            similarity=Levenshtein()
        )
        rules = learner.compare(pages)
        proved_rules = learner._get_proved_rules(
            rules, attribute='actor_address'
        )
        # Assert
        expected_rule_number = 3
        self.assertEqual(expected_rule_number, len(proved_rules))

    def test_get_proved_rules_with_betta_5(self):
        # Arrange
        df = self.sqlContext.createDataFrame(
            [
                Row(_id=0, actor_address="a1, abc1, ee1, rr1"),
                Row(_id=1, actor_address="a2, abc2, ee2, rr2"),
                Row(_id=2, actor_address="a3, abc3, ee3, rr3"),
                Row(_id=3, actor_address="a4, abc4, ee4, rr4"),
                Row(_id=4, actor_address="a5, abc5, ee5, rr5"),
            ]
        )
        pages = self._get_dataframe_of_pages_with_ids([0, 1, 2, 3, 4])
        # Act
        learner = HtmlEntityLearner(
            similarity=Levenshtein(),
            sqlContext=self.sqlContext,
            kb=df,
            threshold_weak_similarity=0.8,
            page_support=5
        )
        rules = learner.compare(pages)
        proved_rules = learner._get_proved_rules(
            rules, attribute='actor_address'
        )
        # Assert
        expected_rule_number = 1
        self.assertEqual(expected_rule_number, len(proved_rules))

    def test_calculate_strong_similarity_first_example(self):
        # Arrange
        df = self.sqlContext.createDataFrame(
            [
                Row(_id=0, actor_address="a1, abc1, ee1, rr1"),
                Row(_id=1, actor_address="a2, abc2, ee2, rr2"),
                Row(_id=2, actor_address="a3, abc3, ee3, rr3"),
                Row(_id=3, actor_address="a4, abc4, ee4, rr4"),
                Row(_id=4, actor_address="a5, abc5, ee5, rr5"),
            ]
        )
        learner = HtmlEntityLearner(
            similarity=Levenshtein(),
            sqlContext=self.sqlContext,
            kb=df,
            threshold_weak_similarity=0.8,
            threshold_strong_similarity=0.8,
            page_support=5
        )
        pages = self._get_dataframe_of_pages_with_ids([0, 1, 2, 3, 4])
        rules = [
            {
                'path': '/html/body/ul/li[1]',
                'value': 'element1gg',
                'url': 'actor_address_1/actor0.html'
            }
        ]
        proved_rules = [{'path': '/html/body/ul/li[1]', 'value': 'element1gg'}]
        # Act
        result = learner.calculate_strong_similarities(
            pages, rules, proved_rules
        )
        # Assert
        expected = [
            {
                "rule":
                    {
                        'path': '/html/body/ul/li[1]',
                        'value': 'element1gg',
                        'url': 'actor_address_1/actor0.html'
                    },
                "similarity": 1.0
            }
        ]
        self.assertEqual(expected, result)

    def test_calculate_strong_similarity_second_example(self):
        # Arrange
        df = self.sqlContext.createDataFrame(
            [
                Row(_id=0, actor_address="a1, abc1, ee1, rr1"),
                Row(_id=1, actor_address="a2, abc2, ee2, rr2"),
                Row(_id=2, actor_address="a3, abc3, ee3, rr3"),
                Row(_id=3, actor_address="a4, abc4, ee4, rr4"),
                Row(_id=4, actor_address="a5, abc5, ee5, rr5"),
            ]
        )
        pages = self._get_dataframe_of_pages_with_ids([0, 1, 2, 3, 4])
        learner = HtmlEntityLearner(
            similarity=Levenshtein(),
            sqlContext=self.sqlContext,
            kb=df,
            threshold_weak_similarity=0.8,
            threshold_strong_similarity=0.8,
            page_support=5
        )
        rules = learner.compare(pages)
        proved_rules = learner._get_proved_rules(
            rules, attribute='actor_address'
        )
        # Act
        result = learner.calculate_strong_similarities(
            pages, rules, proved_rules
        )
        # Assert
        expected = [
            {
                "rule":
                    {
                        'path': '/html/body/ul/li[1]',
                        'value': 'element1gg',
                        'url': 'actor_address_1/actor0.html'
                    },
                "similarity": 1.0
            }
        ]
        self.assertEqual(expected, result)

    def _get_dataframe_of_pages_with_ids(self, ids):
        pages = []
        for page_id in ids:
            path = self.html_directory + '/actor' + str(page_id) + '.html'
            pages.append(Row(_id=page_id, document_path=path))
        return self.sqlContext.createDataFrame(pages)


if __name__ == '__main__':
    unittest.main()
