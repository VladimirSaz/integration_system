import unittest
from pyspark.sql import *

from test_spark import TestSpark
from content_parser.entity_identification.similarity.strong_similarity import StrongSimilarity
import networkx as nx
import networkx.algorithms.isomorphism as iso
from pprint import pprint
from metrics.levenshtein import Levenshtein
from content_parser.entity_identification.text_processor import TextProcessor
from pytest_cases import fixture_ref, fixture_plus, parametrize_plus


class TestStrongSimilarity(unittest.TestCase):
    @staticmethod
    def _get_html(file_name):
        with open('data/' + file_name, 'r') as f:
            return f.read()

    def test_get_graph_of_matching_corresponding_words_with_1_rule(self):
        # Arrange
        kb = Row(_id=0, actor_address='a1, abc1, ee1, rr1')
        extracted = {
            '_id': 0,
            'path': '/html/body/ul/li[1]',
            'value': 'a1, abc1ee1, rr1',
            'url': 'actor_address_1/actor0.html'
        }
        attribute = 'actor_address'
        strong_similarity = StrongSimilarity(
            similarity=Levenshtein(), threshold_word_similarity=0.4
        )
        # Act
        result_graph = strong_similarity.get_graph_of_matching_corresponding_words(
            kb, extracted, attribute
        )
        # Assert
        expected_graph = nx.Graph()
        expected_graph.add_weighted_edges_from(
            [
                ('kb_1', 'extracted_1', 1.0),
                ('kb_2', 'extracted_1', 0.42857142857142855),
                ('kb_2', 'extracted_2', 0.6666666666666666),
                ('kb_3', 'extracted_2', 0.4), ('kb_4', 'extracted_3', 1.0)
            ]
        )
        nx.set_node_attributes(
            expected_graph, {
                'kb_1': {
                    'data': 'a1,'
                },
                'kb_2': {
                    'data': 'abc1,'
                },
                'kb_3': {
                    'data': 'ee1,'
                },
                'kb_4': {
                    'data': 'rr1'
                },
                'extracted_1': {
                    'data': 'a1,'
                },
                'extracted_2': {
                    'data': 'abc1ee1,'
                },
                'extracted_3': {
                    'data': 'rr1'
                },
            }
        )
        self.assertGraphsEqual(
            expected_graph, result_graph, node_data_field='data'
        )
        self.assertEqual(expected_graph.nodes(), result_graph.nodes())

    def test_max_weight_matching(self):
        # Arrange
        graph = nx.Graph()
        graph.add_weighted_edges_from(
            [
                [100, 200, 1], [100, 201, 1], [100, 202, 1], [101, 200, 1],
                [102, 202, 1], [102, 205, 1], [103, 202, 1], [103, 203, 1],
                [104, 203, 1], [105, 202, 1], [105, 203, 1], [105, 206, 1],
                [106, 201, 1], [106, 204, 1], [106, 206, 1]
            ]
        )

        nx.set_node_attributes(
            graph, {
                100: {
                    'data': "1-0"
                },
                101: {
                    'data': "1-1"
                },
                102: {
                    'data': "1-2"
                },
                103: {
                    'data': "1-3"
                },
                104: {
                    'data': "1-4"
                },
                105: {
                    'data': "1-5"
                },
                106: {
                    'data': "1-6"
                },
                200: {
                    'data': "0-0"
                },
                201: {
                    'data': "0-1"
                },
                202: {
                    'data': "0-2"
                },
                203: {
                    'data': "0-3"
                },
                204: {
                    'data': "0-4"
                },
                205: {
                    'data': "0-5"
                },
                206: {
                    'data': "0-6"
                },
            }
        )
        # Act
        result = nx.algorithms.max_weight_matching(graph, maxcardinality=True)
        # Assert
        expected = [
            ('0-2', '1-3'), ('0-4', '1-6'), ('0-3', '1-4'), ('1-5', '0-6'),
            ('0-5', '1-2'), ('1-0', '0-1'), ('0-0', '1-1')
        ]
        self.assertEqual(len(expected), len(list(result)))

    def assertGraphsEqual(self, expected, result, node_data_field):
        self.assertTrue(
            nx.is_isomorphic(
                expected,
                result,
                node_match=iso.categorical_node_match(node_data_field, 1),
                edge_match=iso.numerical_edge_match('weight', 1)
            )
        )

    def test_get_graph_of_matching_corresponding_words_with_one_word_that_matches_2_in_extracted(
        self
    ):
        # Arrange
        kb = Row(_id=0, actor_address='a1, abc1, ee1, rr1 abc1,')
        extracted = {
            '_id': 0,
            'path': 'html/body/ul/li[1]',
            'value': 'abc1, qq12 ee1, abc1, rr1 rr1 gg',
            'url': 'any_url'
        }
        attribute = 'actor_address'
        strong_similarity = StrongSimilarity(
            similarity=Levenshtein(), threshold_word_similarity=0.5
        )
        # Act
        result_graph = strong_similarity.get_graph_of_matching_corresponding_words(
            kb, extracted, attribute
        )
        # Assert
        expected_graph = nx.Graph()
        expected_graph.add_weighted_edges_from(
            [
                ('kb_2', 'extracted_1', 1.0),
                ('kb_2', 'extracted_4', 1.0),
                ('kb_3', 'extracted_3', 1.0),
                ('kb_4', 'extracted_5', 1.0),
                ('kb_4', 'extracted_6', 1.0),
                ('kb_5', 'extracted_1', 1.0),
                ('kb_5', 'extracted_4', 1.0),
            ]
        )
        nx.set_node_attributes(
            expected_graph, {
                'kb_2': {
                    'data': 'abc1,'
                },
                'kb_3': {
                    'data': 'ee1,'
                },
                'kb_4': {
                    'data': 'rr1'
                },
                'kb_5': {
                    'data': 'abc1,'
                },
                'extracted_1': {
                    'data': 'abc1,'
                },
                'extracted_3': {
                    'data': 'ee1,'
                },
                'extracted_4': {
                    'data': 'abc1,'
                },
                'extracted_5': {
                    'data': 'rr1'
                },
                'extracted_6': {
                    'data': 'rr1'
                },
            }
        )
        self.assertGraphsEqual(
            expected_graph, result_graph, node_data_field='data'
        )
        self.assertEqual(expected_graph.nodes(), result_graph.nodes())


def test_calculate_strong_similarity_with_empty():
    kb = [
        Row(_id=0, actor_address='a1, abc1, ee1, rr1 abc1,'),
        Row(_id=1, actor_address='a1, abc1, ee1, rr1 abc1,'),
    ]
    extracted = [
        {
            '_id': 0,
            'path': 'html/body/ul/li[1]',
            'value': 'abc1, qq12 ee1, abc1, rr1 rr1 gg',
            'url': 'actor_address_1/actor0.html'
        }, {
            '_id': 1,
            'path': 'html/body/ul/li[1]',
            'value': 'abc1, qq12 ee1, abc1, rr1 rr1 gg',
            'url': 'actor_address_1/actor0.html'
        }
    ]
    weakly_similar = []
    attribute = 'actor_address'

    strong_similarity = StrongSimilarity(
        similarity=Levenshtein(), threshold_word_similarity=0.5
    )
    result = strong_similarity.calculate(
        kb_entries=kb,
        extracted_entries=extracted,
        weakly_similar=weakly_similar,
        attribute=attribute
    )
    assert result == (
        {
            'kb': 0,
            'extracted': 0,
            'similarity': 0.4375
        },
        {
            'kb': 0,
            'extracted': 1,
            'similarity': 0.4375
        },
        {
            'kb': 1,
            'extracted': 0,
            'similarity': 0.4375
        },
        {
            'kb': 1,
            'extracted': 1,
            'similarity': 0.4375
        },
    )


@fixture_plus(scope='module')
def similarity():
    return Levenshtein()


def test_calculate_strong_similarity_with_1_item_in_ws(similarity):
    kb = [
        Row(_id=0, actor_address='some other string not matching'),
        Row(_id=1, actor_address='a1, abc1, ee1, rr1 abc1,'),
    ]
    extracted = [
        {
            '_id': 0,
            'path': 'html/body/ul/li[1]',
            'value': 'a1, abc1, ee1, rr1 abc1,',
            'url': 'actor_address_1/actor0.html'
        }, {
            '_id': 1,
            'path': 'html/body/ul/li[1]',
            'value': 'some non similar string abcd1',
            'url': 'actor_address_1/actor0.html'
        }
    ]
    weakly_similar = [[1, 0]]  # [kb, extracted]
    attribute = 'actor_address'

    strong_similarity = StrongSimilarity(
        similarity=similarity, threshold_word_similarity=0.5
    )
    result = strong_similarity.calculate(
        kb_entries=kb,
        extracted_entries=extracted,
        weakly_similar=weakly_similar,
        attribute=attribute
    )
    assert result == (
        {
            'kb':
                0,
            'extracted':
                0,
            'similarity':
                similarity.calculate(
                    'some other string not matching', 'a1, abc1, ee1, rr1 abc1,'
                )
        },
        {
            'kb':
                0,
            'extracted':
                1,
            'similarity':
                similarity.calculate(
                    'some other string not matching',
                    'some non similar string abcd1'
                )
        },
        {
            'kb':
                1,
            'extracted':
                0,
            'similarity':
                similarity.calculate(
                    'a1, abc1, ee1, rr1 abc1,', 'a1, abc1, ee1, rr1 abc1,'
                )
        },
        {
            'kb':
                1,
            'extracted':
                1,
            'similarity':
                similarity.calculate(
                    'a1, abc1, ee1, rr1 abc1,', 'some non similar string abcd1'
                )
        },
    )


def test_strong_similarity_nearly_1():
    kb = [
        Row(
            _id=0,
            actor_address='Hampington street of New York, building 1, flat 24'
        ),
    ]
    extracted = [
        {
            '_id':
                0,
            'path':
                'html/body/ul/li[1]',
            'value':
                'Actor leaves at HampingtAn street buildEng 1, happily with wife in flat 24',
            'url':
                'actor_address_1/actor0.html'
        }
    ]
    weakly_similar = [[0, 0]]
    attribute = 'actor_address'

    strong_similarity = StrongSimilarity(
        similarity=Levenshtein(), threshold_word_similarity=0.5
    )
    result = strong_similarity.calculate(
        kb_entries=kb,
        extracted_entries=extracted,
        weakly_similar=weakly_similar,
        attribute=attribute
    )
    matching_parts_kb = ['Hampington street', 'building 1,', 'flat 24']
    matching_parts_extracted = ['HampingtAn street', 'buildEng 1,', 'flat 24']
    tp = TextProcessor()
    expected_similarity = Levenshtein().calculate(
        tp.join(matching_parts_kb), tp.join(matching_parts_extracted)
    )
    assert result == (
        {
            'kb': 0,
            'extracted': 0,
            'similarity': expected_similarity
        },
    )


def test_alpha_too_high_to_let_calculate_strong_similarity(similarity):
    kb = [
        Row(
            _id=0,
            actor_address='Hampington street of New York, building 1, flat 24'
        ),
    ]
    extracted = [
        {
            '_id':
                0,
            'path':
                'html/body/ul/li[1]',
            'value':
                'Actor leaves at HampingtAn street buildEng 1, happily with wife in flat 24',
            'url':
                'actor_address_1/actor0.html'
        }
    ]
    weakly_similar = [[0, 0]]
    attribute = 'actor_address'
    too_high_alpha = 1.1
    strong_similarity = StrongSimilarity(
        similarity=similarity,
        threshold_word_similarity=0.5,
        alpha=too_high_alpha
    )
    result = strong_similarity.calculate(
        kb_entries=kb,
        extracted_entries=extracted,
        weakly_similar=weakly_similar,
        attribute=attribute
    )
    expected_similarity = similarity.calculate(
        'Hampington street of New York, building 1, flat 24',
        'Actor leaves at HampingtAn street buildEng 1, happily with wife in flat 24'
    )
    assert result == (
        {
            'kb': 0,
            'extracted': 0,
            'similarity': expected_similarity
        },
    )
