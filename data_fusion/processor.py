import sys
import json
from data_fusion.step import Step


class Processor:
    def __init__(self, sql_context, config_path, job_id):
        self._sql_context = sql_context
        self._config_path = config_path
        self._job_id = job_id

    @staticmethod
    def _read_from_json_file(file_path):
        with open(file_path) as f:
            return json.load(f)

    pass

    def process(self):
        config = self._read_from_json_file(self._config_path)
        non_service_step_num = 0
        #known issue: if all steps doesnt produce the result, but till now leave it to the user
        if len(config.get("steps", [])) == 0:
            print("no steps defined and preformed on data fusion step")
            self._save_original_as_final()
            return

        for step_num, step in enumerate(config.get("steps", [])):
            Step(
                sql_context=self._sql_context,
                config_path=self._config_path,
                job_id=self._job_id,
                step_num=step_num,
                non_service_step_num=non_service_step_num
            ).process()

            non_service_step_num += 1
            if not step.get("isStepHasResult", True):
                non_service_step_num -= 1
        self._save_result_with_one_entry_per_index_field(config, non_service_step_num - 1)

    def _save_original_as_final(self):
        original_table = f'for_resolved_entities.table{self._job_id}'
        sql_text = f'SELECT * FROM {original_table}'
        self._save_df_from_sql_result(sql_text)

    def _save_df_from_sql_result(self, sql_text):
        df = self._sql_context.sql(sql_text)
        output_table = f"""table{self._job_id}"""
        self._sql_context.sql('USE ' + 'final')
        df.write.mode('overwrite').format('hive').saveAsTable(output_table)

    def _save_result_with_one_entry_per_index_field(self, config, last_step_num):
        last_step_table = f'for_data_fusion_step.table{self._job_id}_{str(last_step_num)}'
        original_table = f'for_resolved_entities.table{self._job_id}'
        index_field = config["indexField"]
        fields_to_select = self._get_fields_to_select(
            last_step_table, original_table, index_field
        )
        sql_text = f"""
        select *
        from (SELECT y.*, row_number() OVER (partition by y.{index_field} order by rand()) as seqnum
                FROM (
                  SELECT {fields_to_select}
                  FROM {original_table} ot
                  LEFT JOIN {last_step_table} ls
                  ON ls.{index_field}=ot.{index_field}
                ) y
             ) t
        where seqnum <= 1;"""
        self._save_df_from_sql_result(sql_text)


    def _get_fields_to_select(self, last_step_table, original_table, index_field):
        last_step_fields = self._get_fields(last_step_table)
        original_table_fields = self._get_fields(original_table)
        all_unique_fields = set(last_step_fields + original_table_fields)
        all_unique_fields.discard(index_field)
        last_step_prefix = 'ls'
        original_table_prefix = 'ot'
        all_unique_fields_with_prefixes = []
        for field in all_unique_fields:
            if field in last_step_fields:
                all_unique_fields_with_prefixes.append(f'{last_step_prefix}.{field}')
                continue
            if field in original_table_fields:
                all_unique_fields_with_prefixes.append(f'{original_table_prefix}.{field}')
                continue
        all_unique_fields_with_prefixes.append(f'{original_table_prefix}.{index_field}')
        return ','.join(all_unique_fields_with_prefixes)

    def _get_fields(self, db_table_name):
        sql_text = f"select * from {db_table_name} limit 1"
        return self._sql_context.sql(sql_text).columns
