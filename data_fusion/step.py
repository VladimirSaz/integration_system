import sys
import json
from data_fusion.algorithm.factory import Factory

class Step:
    def __init__(self, sql_context, config_path, job_id, step_num, non_service_step_num):
        self._sql_context = sql_context
        self._config_path = config_path
        self._job_id = job_id
        self._step_num = step_num
        self._non_service_step_num = non_service_step_num

    @staticmethod
    def _read_from_json_file(file_path):
        with open(file_path) as f:
            return json.load(f)
    pass
    def process(self):
        config = self._read_from_json_file(self._config_path)
        steps_config = config.get("steps", [])
        step_config = steps_config[self._step_num]
        step_config["indexField"] = config["indexField"]
        algorithm = Factory().get(
            sql_context=self._sql_context,
            options=step_config,
            job_id=self._job_id,
            step_num=self._non_service_step_num,
        )
        algorithm.execute()
