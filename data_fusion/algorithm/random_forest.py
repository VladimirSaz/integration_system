from catboost import CatBoostRegressor
import pickle
import os
import re

from data_fusion.algorithm.algorithm import Algorithm


class RandomForest(Algorithm):
    def _execute(self, df):
        features, target = self._prepare_dataset(df)
        if self._processor_options['options']['isTrain']:
            print("1")
            self._train_model(features, target)
            return None
        else:
            predicted_target = self._predict(features)
            return self._update_df(df, predicted_target)

    def _prepare_dataset(self, df):
        features_dataset = []
        target = []
        for r in df:
            row = r.asDict()
            features_row = []
            for field in self._processor_options['options']['features']:
                field_name, field_type = list(field.items())[0]
                features_row.append(self._postprocess_value(row, field_name, field_type))
            features_dataset.append(features_row)
            if not self._processor_options['options']['isTrain']:
                continue
            target_field = self._processor_options['options']['target']
            target.append(self._postprocess_float(row, target_field))
        return features_dataset, target

    def _postprocess_value(self, row, field_name, field_type):
        if field_type == 'string':
            return self._postprocess_string(row, field_name)
        if field_type in ['float', 'int']:
            return self._postprocess_float(row, field_name)

    def _postprocess_string(self, row, field_name):
        return len(row.get(field_name, ""))

    def _postprocess_float(self, row, field_name):
        value = row.get(field_name, None)
        if value is None:
            return value
        return self._clear_numeric_value(value)

    def _clear_numeric_value(self, value):
        str_value = str(value).split('\\')[0]
        str_value = re.sub('[^\d\.]', '', str_value)
        str_value = re.sub('.$', '', str_value)
        try:
            return float(str_value)
        except ValueError as e:
            return None

    def _train_model(self, features, target):

        params = self._processor_options['options']['params']
        model = CatBoostRegressor(**params)
        model.fit(features, target)

        self._save_model(model)

    def _save_model(self, model):
        with open(self._get_model_path(), 'wb') as handle:
            pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def _get_model_path(self):
        model_name = self._processor_options['options']['modelName']
        model_full_path = f'../data/{self._processor_options["supportData"]}/{model_name}.pickle'
        os.makedirs(os.path.dirname(model_full_path), exist_ok=True)
        return model_full_path


    def _predict(self, features):
        model = self._load_model()
        return model.predict(features).tolist()

    def _load_model(self):
        with open(self._get_model_path(), 'rb') as handle:
            return pickle.load(handle)

    def _update_df(self, df, target_values):
        new_df = []
        target_field = self._processor_options['options']['target']
        for i, r in enumerate(df):
            row = r.asDict()
            row[target_field] = target_values[i]
            new_df.append(row)
        return new_df