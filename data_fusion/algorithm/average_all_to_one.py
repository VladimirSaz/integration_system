import re
from data_fusion.algorithm.algorithm import Algorithm


class AverageAllToOne(Algorithm):
    def _execute(self, df):
        result_df = []
        index_field = self._options["indexField"]
        current_index = None
        num_rows_with_same_index = 0
        sum_of_fields_by_field = {}
        for r in df:
            row = r.asDict()
            if current_index is None:
                current_index = row[index_field]
            if row[index_field] != current_index:
                current_index = row[index_field]
                for field in self._options.get("inputFields", []):
                    row[field] = sum_of_fields_by_field.get(field, 0) / num_rows_with_same_index
                result_df.append(row)
                num_rows_with_same_index = 0
                sum_of_fields_by_field = {}
            num_rows_with_same_index += 1
            for field in self._options.get("inputFields", []):
                current_value = sum_of_fields_by_field.get(field, 0)
                additional_value = self._clear_numeric_value(row[field])
                sum_of_fields_by_field[field] = current_value + additional_value
        return result_df

    @staticmethod
    def _clear_numeric_value(value):
        str_value = str(value).split('\\')[0]
        str_value = re.sub('[^\d\.]', '', str_value)
        str_value = re.sub('.$', '', str_value)
        try:
            return float(str_value)
        except ValueError as e:
            return 0
