import importlib


class Factory:
    @staticmethod
    def get(**kwargs):
        processor_options = kwargs["options"]["processor"]
        class_ = getattr(importlib.import_module(processor_options["module"]), processor_options["name"])
        return class_(**kwargs)
