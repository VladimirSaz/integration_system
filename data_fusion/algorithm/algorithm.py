from pyspark.sql.types import *

class Algorithm:
    def __init__(self, **kwargs):
        self._sql_context = kwargs["sql_context"]
        self._options = kwargs["options"]
        self._processor_options = self._options["processor"]
        self._job_id = kwargs["job_id"]
        self._db_name_for_init_step_from = "for_resolved_entities"
        self._db_name_from_data_fusion_step = "for_data_fusion_step"
        self._step_num = kwargs["step_num"]
        self.is_log_results = False
        self._user_types_to_pyspark_types = {
            'string': StringType(),
            'float': FloatType(),
            'long': LongType(),
            'default': StringType()
        }

    def execute(self):
        df = self._get_df()
        new_df = self._execute(df)
        self._save_to_step_storage(new_df)

    def _execute(self, df):
        return df

    def _get_df(self):
        sql_text = self._compose_sql_to_load_df()
        return self._sql_context.sql(sql_text).collect()

    def _compose_sql_to_load_df(self):
        index_field = self._options["indexField"]
        fields = self._get_fields_to_extract()
        initial_db_and_table = f'{self._db_name_for_init_step_from}.table{self._job_id}'
        step_db_and_table_for_step = f'{self._db_name_from_data_fusion_step}.{self._get_table_name_for_step(self._step_num - 1)}'
        initial_sql = f'SELECT {fields} FROM {initial_db_and_table} '
        sql_for_step = f'''LEFT JOIN {step_db_and_table_for_step}
        ON {initial_db_and_table}.{index_field} = {step_db_and_table_for_step}.{index_field} '''
        end_of_sql = f'ORDER BY {index_field}'
        if not self._is_any_previous_steps_stored_data():
            return initial_sql + end_of_sql
        else:
            return initial_sql + sql_for_step + end_of_sql

    def _get_fields_to_extract(self):
        index_field = self._options["indexField"]
        columns_of_prev_step = self._get_fields_of_previous_step()
        previous_step_table = self._get_table_name_for_step(self._step_num - 1)
        all_fields = self._options.get("inputFields", []).copy()
        for i, field in enumerate(all_fields):
            if field in columns_of_prev_step:
                all_fields[i] = f'{previous_step_table}.{field}'
        original_index_field = f'{self._db_name_for_init_step_from}.table{self._job_id}.{index_field}'
        return ','.join(all_fields + [original_index_field])

    def _get_fields_of_previous_step(self):
        if not self._is_any_previous_steps_stored_data():
            return []
        step_db_and_table_for_step = f'{self._db_name_from_data_fusion_step}.{self._get_table_name_for_step(self._step_num - 1)}'
        sql_text = f"select * from {step_db_and_table_for_step} limit 1"
        return self._sql_context.sql(sql_text).columns

    def _is_any_previous_steps_stored_data(self):
        return  self._step_num > 0

    def _get_table_name_for_step(self, step):
        return f'table{self._job_id}_{str(step)}'

    def _save_to_step_storage(self, entries):
        if not self._options.get("isStepHasResult", True):
            return
        clean_entries = self._clear_entries_prior_storing(entries)
        schema = self._get_schema_of_df_to_store()
        df = self._sql_context.createDataFrame(clean_entries, schema=schema)
        output_table = self._get_output_table_name()
        self._sql_context.sql('USE ' + self._db_name_from_data_fusion_step)
        df.write.mode('overwrite').format('hive').saveAsTable(output_table)
        self._show_saved()

    def _clear_entries_prior_storing(self, entries):
        target_fields = self._options["targetFields"]
        clean_entries = []
        new_row = {}
        for entry in entries:
            for target_field in target_fields:
                if isinstance(target_field, str):
                    new_row[target_field] = entry[target_field]
                if isinstance(target_field, dict):
                    new_row[self._get_output_field(target_field)] = entry[target_field["input"]]
            new_row[self._options['indexField']] = entry[self._options['indexField']]
            clean_entries.append(new_row)
        return clean_entries

    @staticmethod
    def _get_output_field(target_field):
        if isinstance(target_field['output'], str):
            return target_field['output']
        if isinstance(target_field['output'], dict):
            return target_field['output']['name']

    def _get_schema_of_df_to_store(self):
        target_fields = self._options["targetFields"]
        fields_by_type = []
        for target_field in target_fields:
            if isinstance(target_field, str):
                fields_by_type.append(StructField(target_field, StringType()))
            if isinstance(target_field, dict) and  isinstance(target_field['output'], dict):
                fields_by_type.append(StructField(
                    target_field['output']['name'],
                    self._get_pyspark_type(target_field['output']['type'])
                ))
                continue
            if isinstance(target_field, dict):
                fields_by_type.append(StructField(target_field['output'], StringType()))
        fields_by_type.append(StructField(self._options['indexField'], LongType()))
        return StructType(fields_by_type)

    def _get_pyspark_type(self, type_as_string):
        default = self._user_types_to_pyspark_types.get('default')
        return self._user_types_to_pyspark_types.get(type_as_string,default)

    def _get_output_table_name(self):
        return f'table{self._job_id}_{self._step_num}'

    def _show_saved(self):
        if not self.is_log_results:
            return
        sql_text = f'SELECT * FROM {self._db_name_from_data_fusion_step}.{self._get_output_table_name()}'
        print(self._sql_context.sql(sql_text).collect())
        print(f"saved on step {self._step_num}")
