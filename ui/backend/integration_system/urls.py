from rest_framework import routers
from django.urls import path, include
from integration_system import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView)
from integration_system.views.async_tasks import get_task_status, get_tasks_status, TaskCreateView, TaskListView, \
    TaskDeleteView
from integration_system.views.user import register
from integration_system.views.note import NoteViewSet

router = routers.DefaultRouter()
router.register(r'notes', NoteViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # Submit your refresh token to this path to obtain a new access token
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    # Register a new user
    path('register/', register, name='register_view'),
    path('create-task/', TaskCreateView.as_view(), name='create-task'),
    path('get-tasks/', TaskListView.as_view(), name='get-tasks'),
    path('remove-task/', TaskDeleteView.as_view(), name='remove-task'),
    path('get-task-status/', get_task_status, name='get-task-status'),
    path('get-tasks-status/', get_tasks_status, name='get-tasks-status'),

]
