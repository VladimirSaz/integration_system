from django.contrib.auth.models import User
from django.db import models


class Note(models.Model):
    note = models.CharField(max_length=180)


class Task(models.Model):
    id = models.CharField(max_length=180, unique=True, primary_key=True)
    status = models.CharField(max_length=180)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
