from rest_framework import viewsets
from integration_system.models import Note
from integration_system.serializers import NoteSerializer


class NoteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows notes to be viewed or edited.
    """
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
