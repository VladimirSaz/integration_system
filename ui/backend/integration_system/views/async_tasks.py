import sys

from django.core.exceptions import ValidationError
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from integration_system.models import Task

sys.path.append('../../')
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from celery import shared_task
from time import sleep
from celery import current_app
from random import randint

class TaskDeleteView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)  # checks if user is authenticated to view the model objects

    @staticmethod
    def post(request, **kwargs):
        task_id = request.data.get('id', )
        user_task = Task.objects.get(id=task_id)
        user_task.delete()
        return Response({}, status=200)



class TaskListView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)  # checks if user is authenticated to view the model objects

    @staticmethod
    def get(request, **kwargs):
        user_tasks = Task.objects.filter(user=request.user)
        tasks = {}
        for task in user_tasks:
            tasks[task.id] = task.status
        return Response({"tasks": tasks}, status=200)


class TaskCreateView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def post(request):
        task = long_task.delay()
        task_record = Task(
            id=task.task_id,
            status=task.status,
            user=request.user
        )
        task_record.save()
        return Response({'id': task.task_id, 'status': task.status}, status=200)


@shared_task
def long_task():
    sleep(randint(3, 12))


@csrf_exempt
@api_view(['POST'])
def get_task_status(request):
    tasks = current_app.tasks
    tasks_group = tasks.get('integration_system.views.async_tasks.long_task', )
    task_id = request.data.get('id', )
    task = tasks_group.AsyncResult(task_id)
    return Response({'result': task.state}, status=200)


@csrf_exempt
@api_view(['POST'])
def get_tasks_status(request):
    tasks = current_app.tasks
    tasks_group = tasks.get('integration_system.views.async_tasks.long_task', )
    tasks_statuses = {}
    task_ids = request.data.get('ids', )
    for task_id in task_ids:
        task = tasks_group.AsyncResult(task_id)
        tasks_statuses[task_id] = task.state
    for task_id, status in tasks_statuses.items():
        print(status)
        if status == 'SUCCESS':
            task = Task.objects.get(id=task_id)
            task.status = status
            task.save()
    return Response(tasks_statuses, status=200)
