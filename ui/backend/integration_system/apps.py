from django.apps import AppConfig


class IntegrationSystemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'integration_system'
