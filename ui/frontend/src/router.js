import Vue from 'vue'
import Router from 'vue-router'
import about from './views/About.vue'
import login from './views/Login'
import register from './views/Register'
import integrationSystemMain from './views/IntegrationSystem/Main'
import dataSourceSelection from './views/DataSource/Selection'
import dataSourceSearchResults from './views/DataSource/Results'
import logout from './views/Logout'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'about',
      component: about,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: login,
      meta: {
        requiresLogged: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: register,
      meta: {
        requiresLogged: true
      }
    },
    {
      path: '/integration-system',
      name: 'integration-system',
      component: integrationSystemMain,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: '/data-source-selection',
      name: 'data-source-selection',
      component: dataSourceSelection,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: '/data-source-results',
      name: 'data-source-results',
      component: dataSourceSearchResults,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/logout',
      name: 'logout',
      component: logout,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
