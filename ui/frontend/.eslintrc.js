"use strict";

module.exports = {
    root: true,
    // extends: "standard",
    //plugins: ["prettier"],
    "extends": ["vue", "standard", "plugin:vue/recommended"],
    "plugins": ["import", "vue"],
    rules: {
        "max-len": "off",
        strict: "error",//error
        "vue/require-v-for-key": "off",
        "space-before-function-paren": ['error', {"anonymous": "always", "named": "never", "asyncArrow": "always"}],
        //"prettier/prettier": "error",
    },
    env: {
        node: true,
    },
    parserOptions: {
        "sourceType": "module"
    },
    overrides: [
        {
            files: ["test/**/*"],
            env: {
                mocha: true,
            },
        },
    ],
};