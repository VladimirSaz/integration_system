import unittest
import sys
sys.path.append('/Users/vladimir/integration_system/tools') # noqa
from test_tools import name_of_test


class TestUM(unittest.TestCase):

    def setUp(self):
        self.metric = levenshtein.Levenshtein()

    @name_of_test("test_calculate__With_equal_strings__Responds_1.0")
    def test_calculate_1(self):
        # Arrang
        text1 = 'same text'
        text2 = 'same text'
        # Act
        result = self.metric.calculate(text1, text2)
        # Assert
        expected = 1.0
        self.assertEqual(expected, result)

    @name_of_test("test_calculate__With_half_of_first_string_is_correct__"
               "Responds_1.0")
    def test_calculate_2(self):
        # Arrang
        text1 = 'qwerty'
        text2 = 'qweasd'
        # Act
        result = self.metric.calculate(text1, text2)
        # Assert
        expected = 0.5
        self.assertEqual(expected, result)

    @name_of_test("test_calculate__With_not_equal_lenght_lines__"
               "Responds_1.0")
    def test_calculate_3(self):
        # Arrang
        text1 = 'qwerty asd'
        text2 = 'qwert'
        # Act
        result = self.metric.calculate(text1, text2)
        # Assert
        expected = 0.5
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
