import pdb  # noqa
from pprint import pprint  #noqa
import editdistance


class Levenshtein():
    """
    Calculates Levenstein similarity of two strings normalized by max length
    """
    def calculate(self, text1, text2):
        """
        Calculates Levenstein similarity of two strings normalized by max length

        Args:
            text1: a string for comparison
            text2: a string for comparison

        Returns:
            Levenstein similarity value of two strings normalized by max length of strings
        """
        distance = self._get_levenshtein_distance(text1, text2)
        return self._normalize_distance(distance, text1, text2)

    def _get_levenshtein_distance(self, text1, text2):
        return editdistance.eval(text1, text2)

    def _normalize_distance(self, distance, text1, text2):
        if len(text1) == len(text2) and len(text1) == 0:
            return distance
        return 1.0 - distance / max(len(text1), len(text2))
