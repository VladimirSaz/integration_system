import json
import lxml.etree
import parslepy


class Rule:
    """
    Class that helps extract data from html, using rule
    description language Parslepy

    Attributes:
        path: a string path to rule file
    """

    def __init__(self, path):
        self._cached_rule = None
        self.path = path

    def get(self):
        """
        gets rule (caching the result)

        Returns:
            dictionary with rules how to extract data
        """
        if not self.path:
            raise ValueError
        if self._cached_rule:
            return self._cached_rule

        self._cached_rule = self._read_rule_from_json_file(self.path)
        print(self._cached_rule)
        return self._cached_rule

    @staticmethod
    def _read_rule_from_json_file(filepath):
        with open(filepath) as f:
            return json.load(f)

    def parse(self, html):
        """
        Extracts data from html using extraction rule

        Args:
            html: a string with html

        Returns:
            dictionary with extracted data from html
        """
        html_tree = self._get_html_tree(html)
        rule = self.get()["extraction_rule"]
        p = parslepy.Parselet(rule)
        return p.extract(html_tree)

    def _get_html_tree(self, html):
        html_parser = lxml.etree.HTMLParser()
        return lxml.etree.fromstring(html, parser=html_parser)
