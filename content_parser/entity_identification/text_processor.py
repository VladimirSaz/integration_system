import re


class TextProcessor:
    def __init__(self):
        self.split_re = r'\s+'
        self.word_join = " "

    def split(self, string):
        return re.split(self.split_re, string)

    def join(self, words):
        return self.word_join.join(words)