from scrapy.http import HtmlResponse
import re
from pprint import pprint
from lxml import etree
import itertools
import pickle
import os
from w3lib.html import remove_tags_with_content
from content_parser.entity_identification.similarity.strong_similarity import StrongSimilarity


class HtmlEntityLearner:
    def __init__(
        self,
        similarity,
        sqlContext=None,
        kb=None,
        threshold_weak_similarity=0,
        threshold_strong_similarity=0,
        page_support=0,
        absolute_path=''
    ):
        self.allowed_chars_pattern = re.compile("\W\s")
        self.sqlContext = sqlContext
        self.kb = kb
        self.similarity = similarity
        self.threshold_weak_similarity = threshold_weak_similarity
        self.threshold_strong_similarity = threshold_strong_similarity
        self.page_support = page_support
        self.absolute_path = absolute_path

    def get_strong_similarities(self, rules):
        strong_similarities_by_attribute = {}
        for attribute in rules.keys():
            proved_rules = self._get_proved_rules(rules, attribute)
            weakly_similar = self.get_weakly_similar_pairs(
                rules[attribute], proved_rules
            )
            strong_similarities = self.calculate_strong_similarities(
                rules[attribute], proved_rules, weakly_similar, attribute
            )
            filtered = self._filter_strong_similarities_by_threshold(
                strong_similarities
            )
            strong_similarities_by_attribute[attribute] = filtered
        return strong_similarities_by_attribute

    def calculate_strong_similarities(
        self, rules_of_attribute, proved_rules, weakly_similar, attribute
    ):
        strong_similarity = StrongSimilarity(
            similarity=self.similarity,
            threshold_word_similarity=0.5,
            alpha=0.1
        )
        extracted_entries = self._get_extracted_from_rules_for_attribute(
            rules_of_attribute, proved_rules
        )
        return strong_similarity.calculate(
            kb_entries=self.kb.collect(),
            extracted_entries=extracted_entries,
            weakly_similar=weakly_similar,
            attribute=attribute
        )

    @staticmethod
    def _get_extracted_from_rules_for_attribute(rules, proved_rules):
        unique_extracted_entries = []
        ids_in_extracted = []
        for rule in rules:
            for proved_rule in proved_rules:
                if rule['node']['path'] == proved_rule['node']['path'] \
                        and rule['node']['_id'] not in ids_in_extracted:
                    unique_extracted_entries.append(rule['node'])
                    ids_in_extracted.append(rule['node']['_id'])
        return unique_extracted_entries

    def _filter_strong_similarities_by_threshold(self, similarities):
        filtered_similarities = []
        for similarity in similarities:
            if similarity['similarity'] > self.threshold_strong_similarity:
                filtered_similarities.append(similarity)
        return filtered_similarities

    def _get_proved_rules(self, rules, attribute):
        proved_rules = []
        rule_counts = self._count_identical_rules(rules[attribute])
        for rule_count in rule_counts:
            if rule_count['count'] >= self.page_support:
                proved_rules.append(rule_count['rule'])
        return proved_rules

    @staticmethod
    def _count_identical_rules(rules):
        rule_counts = []
        for rule in rules:
            is_found_rule_count = False
            for rule_count in rule_counts:
                if rule_count['rule']['node']['path'] == rule['node']['path']:
                    rule_count['count'] += 1
                    is_found_rule_count = True
            if not is_found_rule_count:
                rule_counts.append({'rule': rule, 'count': 1})
        return rule_counts

    def compare(self, pages, rules_file):
        if os.path.exists(rules_file):
            return pickle.load(open(rules_file, "rb"))

        attributes = self._get_kb_attributes()
        matching = self._init_empty_page_matching(attributes)

        for page in pages.toLocalIterator():
            nodes = self._get_nodes(page)
            for entry in self.kb.toLocalIterator():
                for node in nodes:
                    for attribute in attributes:
                        similarity = self.similarity.calculate(
                            str(entry[attribute]), node['value']
                        )
                        if similarity > self.threshold_weak_similarity:
                            matching[attribute].append(
                                {
                                    'node': node,
                                    'entry': entry
                                }
                            )

        if rules_file is not None:
            pickle.dump(matching, open(rules_file, "wb"))
        return matching

    @staticmethod
    def _init_empty_page_matching(attributes):
        matching = {}
        for attribute in attributes:
            matching[attribute] = []
        return matching

    def _get_kb_attributes(self):
        attributes = self.kb.schema.names
        regex = re.compile("^_")
        attributes = filter(lambda a: not regex.search(a), attributes)
        return list(attributes)

    def _get_nodes(self, page):
        raw_html = self._get_html(self.absolute_path + page['storage_path'])
        html_tree = HtmlResponse(url="", body=raw_html, encoding="utf-8")
        nodes = []
        for xpath_query in self._xpath_queries_iterator(raw_html):
            tag_content = html_tree.xpath(xpath_query
                                         ).xpath("normalize-space(string(.))")
            node_texts = [x.strip("\r\n") for x in tag_content.extract()]
            node_texts = [x for x in node_texts if x]
            nodes += [
                {
                    '_id': page['_id'],
                    'path': xpath_query,
                    'value': ' '.join(node_texts)
                }
            ]
        return nodes

    @staticmethod
    def _get_html(file_name):
        with open(file_name, encoding='utf-8', errors='ignore') as file:
            raw_html = file.read()
        cleared_html = remove_tags_with_content(raw_html, ('script', 'style'))
        return cleared_html

    @staticmethod
    def _xpath_queries_iterator(raw_html):
        root = etree.HTML(raw_html)
        tree = etree.ElementTree(root)
        for e in root.iter():
            yield tree.getpath(e)

    # TODO: =======================================
    def calculate_max_similarity_matching(
        self, all_similarity_matchings, rules
    ):

        extracted = self._get_extracted_from_all_rules(rules)
        """
        
        Args:
            all_similarity_matchings: 
            {
                'attribute1': [{
                    'kb': 0,
                    'extracted': 0,
                }],
                'attribute2': [{
                    'kb': 0,
                    'extracted': 0,
                }],
                'attribute3':
                    [
                        {
                            'kb': 0,
                            'extracted': 0,
                        },
                        {
                            'kb': 1,
                            'extracted': 3,
                        },
                    ],
            }
            where numbers is ids in data base (id means entity extracted)
            attribute3 is supported by two rules extraction. If path in rules are equal
            then such rules treated as one.
            kb: 
            [
                Row(
                    _id=0,
                    attribute1='some other string not matching',
                    attribute2='info fields data',
                    attribute3='attribute3 data',
                ),
                Row(
                    _id=1,
                    attribute1='a1, abc1, ee1, rr1 abc1,',
                    attribute2='zxc, aaa, bbb,ccc',
                    attribute3='attribute3 different data',
                ),
            ]
            dataframe-like strusture
            extracted: 
            [
                {
                    '_id': 0,
                    'path': 'html/body/ul/li[1]',
                    'value': 'a1, abc1, ee1, rr1 abc1,',
                    'url': 'actor_address_1/actor0.html',
                    'attribute': 'attribute1',
                },
                {
                    '_id': 1,
                    'path': 'html/body/ul/li[2]',
                    'value': 'some non similar string abcd1',
                    'url': 'actor_address_1/actor0.html',
                    'attribute': 'attribute1',
                },
                {
                    '_id': 0,
                    'path': 'html/body/ul/li[55]',
                    'value': 'info fields data',
                    'url': 'actor_address_1/actor0.html',
                    'attribute': 'attribute2',
                },
            ]
            in extracted similar ids means that such data extracted from same document

        Returns:{
            ('attribute1', 'html/body/ul/li[1]'),
            ('attribute2', 'html/body/ul/li[55]'),
            ('attribute3', 'html/body/ul/li[99]'),
        }
        the most promising extraction rule
        
        
        combinations:
        (a1,p1) ; (a2,p2); (a1,p2)
        if support of the rules greater than threshold
        then on second iteration this array should contain:
        [{(a1,p1),(a2,p2)},{(a1,p2),(a2,p2)}]

        """
        similarity_matchings = self._add_path_to_similarity_matchings(
            all_similarity_matchings, extracted
        )
        max_support_combination = set()
        combinations = self._get_initial_combinations(similarity_matchings)
        cnt = 0
        while len(combinations) > 0:
            max_support = 0
            combinations_supported = []
            for combination in combinations:
                support = self._calculate_support(
                    combination, similarity_matchings
                )
                # print("combination")
                # print(combination)
                # print("support")
                # print(support)
                if support < self.page_support:
                    continue

                combinations_supported.append(combination)
                if support > max_support:
                    max_support = support
                    max_support_combination = combination
            cnt += 1
            # print(cnt)
            # print(combinations_supported)
            # print("///")
            combinations = self._get_higher_order_combinations(
                combinations_supported
            )
        #     print(combinations)
        #     print("===")
        # print("******")
        return set(max_support_combination)

    def _get_extracted_from_all_rules(self, rules):
        extracted = []
        for attribute in rules.keys():
            proved_rules = self._get_proved_rules(rules, attribute)
            extracted_entries_for_attribute = self._get_extracted_from_rules_for_attribute(
                rules[attribute], proved_rules
            )
            for entry in extracted_entries_for_attribute:
                entry['attribute'] = attribute
            extracted += extracted_entries_for_attribute

        unique_ids = []
        unique_extracted = []
        for entry in extracted:
            if entry['_id'] not in unique_ids:
                unique_ids.append(entry['_id'])
                unique_extracted.append(entry)
        return unique_extracted

    @staticmethod
    def _add_path_to_similarity_matchings(similarity_matchings, extracted):
        for entry in extracted:
            for attribute, attribute_matchings in similarity_matchings.items():
                for matching in attribute_matchings:
                    if (
                        entry['_id'] == matching['extracted'] and
                        entry['attribute'] == attribute
                    ):
                        matching['path'] = entry['path']

        cleared_matchings = {}
        for attribute, attribute_matchings in similarity_matchings.items():
            cleared_matchings[attribute] = []
            for matching in attribute_matchings:
                if 'path' in matching.keys():
                    cleared_matchings[attribute].append(matching)
        return cleared_matchings

    def _get_initial_combinations(self, similarity_matchings):
        unique = self._enumerate_unique_attribute_path_combinations(
            similarity_matchings
        )
        combinations = set()
        for attribute, path_ids in unique.items():
            for path_id in path_ids:
                combination = ((attribute, unique[attribute][path_id]), )
                combinations.add(combination)
        return combinations

    def _enumerate_unique_attribute_path_combinations(
        self, similarity_matchings
    ):
        uniques = {}
        max_unique_id = 0
        for attribute, matchings in similarity_matchings.items():
            for matching in matchings:
                path = matching['path']
                if attribute not in uniques:
                    uniques[attribute] = {}
                if not self._is_attribute_path_pair_exists(
                    uniques[attribute], path
                ):
                    uniques[attribute][str(max_unique_id)] = path
                    max_unique_id += 1
        return uniques

    @staticmethod
    def _is_attribute_path_pair_exists(attribute_paths, path):
        for unique_id, attribute_path in attribute_paths.items():
            if path == attribute_path:
                return True
        return False

    def _calculate_support(self, combination, similarity_matchings):
        pages_per_attribute = self._get_pages_per_attribute(
            combination, similarity_matchings
        )

        supporting_pages = self._get_support_pages_for_combination(
            pages_per_attribute
        )

        return len(supporting_pages)

    @staticmethod
    def _get_pages_per_attribute(combination, similarity_matchings):
        # combination[(attribute1,path1)(attribute1,path2)] - not possible by design.
        pages_per_attribute = {}
        for attribute, path in combination:
            matchings = similarity_matchings[attribute]
            pages_per_attribute[attribute] = set()
            for matching in matchings:
                if path == matching['path']:
                    pages_per_attribute[attribute].add(
                        (matching['kb'], matching['extracted'])
                    )
        return pages_per_attribute

    @staticmethod
    def _get_support_pages_for_combination(pages_per_attribute):
        supporting_pages = None
        for pages_of_attribute in pages_per_attribute.values():
            if supporting_pages is None:
                supporting_pages = pages_of_attribute
            else:
                supporting_pages = supporting_pages & pages_of_attribute
        return supporting_pages

    def _get_higher_order_combinations(self, original_combinations):
        if len(original_combinations) == 0:
            return []
        order = len(original_combinations[0]) + 1
        items = self._get_items_of(original_combinations)
        # print("items:")
        # print(items)
        ho_combinations = list(itertools.combinations(items, order))
        return self._filter_combinations(ho_combinations, original_combinations)

    @staticmethod
    def _get_items_of(original_combinations):
        items_of_combinations = []
        for combinations in original_combinations:
            for item in combinations:
                items_of_combinations.append(item)
        return set(items_of_combinations)

    def _filter_combinations(self, ho_combinations, original_combinations):
        filtered_ho_combinations = []
        for ho_combination in ho_combinations:
            for combination in original_combinations:
                if self._is_combination_in(combination, ho_combination):
                    filtered_ho_combinations.append(ho_combination)
                    break
        no_similar_attributes_combinations = []
        for combination in filtered_ho_combinations:
            if self._is_valid_combination(combination):
                no_similar_attributes_combinations.append(combination)

        return no_similar_attributes_combinations

    @staticmethod
    def _is_valid_combination(combination):
        for pair1 in combination:
            attribute1 = pair1[0]
            for pair2 in combination:
                if pair1 == pair2:
                    continue
                attribute2 = pair2[0]
                if attribute1 == attribute2:
                    return False
        return True

    @staticmethod
    def _is_combination_in(combination, candidate_combination):
        for item in combination:
            if item not in candidate_combination:
                return False
        return True

    @staticmethod
    def _is_combination_with_attribute_in_combination_already(
        combination, attribute
    ):
        for extraction_rule in combination:
            if attribute == extraction_rule[0]:
                return True
        return False

    @staticmethod
    def get_weakly_similar_pairs(rules_for_attribute, proved_rules):
        pairs = []
        for rule in rules_for_attribute:
            for proved_rule in proved_rules:
                if rule['node']['path'] == proved_rule['node']['path']:
                    pair = [rule['entry']['_id'], rule['node']['_id']]
                    if pair not in pairs:
                        pairs.append(pair)
        return pairs
