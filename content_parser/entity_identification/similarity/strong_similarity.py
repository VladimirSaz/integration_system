import re
from pprint import pprint
from ngram import NGram
import networkx as nx
from content_parser.entity_identification.similarity.matching_pattern import MatchingPattern
from content_parser.entity_identification.text_processor import TextProcessor


class StrongSimilarity:
    def __init__(
        self,
        similarity,
        sqlContext=None,
        kb=None,
        threshold_weak_similarity=0,
        threshold_strong_similarity=0,
        threshold_word_similarity=0,
        page_support=0,
        n_gram=2,
        alpha=0.1
    ):
        self.similarity = similarity
        self.sqlContext = sqlContext
        self.kb = kb
        self.threshold_weak_similarity = threshold_weak_similarity
        self.threshold_strong_similarity = threshold_strong_similarity
        self.threshold_word_similarity = threshold_word_similarity
        self.page_support = page_support
        self.n_gram = n_gram
        self.alpha = alpha
        self.text_processor = TextProcessor()

    def calculate(
        self,
        kb_entries,  # U #kb
        extracted_entries,  # V #extracted
        weakly_similar,
        attribute  # a
    ):
        patterns = self._get_patterns(
            kb_entries, extracted_entries, weakly_similar, attribute
        )
        similarities = ()
        kb_ids = self._get_ids_for(weakly_similar, 'kb')
        extracted_ids = self._get_ids_for(weakly_similar, 'extracted')
        for kb in kb_entries:
            if kb['_id'] not in kb_ids:
                continue
            for extracted in extracted_entries:
                if extracted['_id'] not in extracted_ids:
                    continue
                similarity = self._calculate_similarity(
                    kb, extracted, attribute, weakly_similar, patterns
                )
                similarities = (*similarities, similarity)
        return similarities

    def _get_patterns(self, entries, docs_extracted, weakly_similar, attribute):
        patterns = []
        for kb in entries:
            for extracted in docs_extracted:
                if not self._is_weakly_similar(kb, extracted, weakly_similar):
                    continue
                patterns.append(self._get_pattern(kb, extracted, attribute))
        return patterns

    @staticmethod
    def _get_ids_for(weakly_similar, entry_type):
        if entry_type == 'kb':
            return set([w[0] for w in weakly_similar])
        if entry_type == 'extracted':
            return set([w[1] for w in weakly_similar])

    @staticmethod
    def _is_weakly_similar(kb, extracted, weakly_similar):
        return [kb['_id'], extracted['_id']] in weakly_similar

    def _get_pattern(self, kb, extracted, attribute):
        graph = self.get_graph_of_matching_corresponding_words(
            kb, extracted, attribute
        )
        matching_words = nx.algorithms.max_weight_matching(
            graph, maxcardinality=True
        )
        return MatchingPattern().get(
            kb, extracted, attribute=attribute, graph_edges=matching_words
        )

    def _calculate_similarity(
        self, kb, extracted, attribute, weakly_similar, patterns
    ):
        is_weakly_similar = self._is_weakly_similar(
            kb, extracted, weakly_similar
        )
        is_pattern_in_fraction = self._is_pattern_in_fraction(
            kb, extracted, weakly_similar, patterns
        )
        if is_weakly_similar and is_pattern_in_fraction:
            pattern = self._get_pattern_for(kb, extracted, patterns)
            return {
                'kb': kb['_id'],
                'extracted': extracted['_id'],
                'similarity': self._calculate_similarity_by_pattern(pattern)
            }
        else:
            return self._calculate_weak_similarity(kb, extracted, attribute)

    def _is_pattern_in_fraction(self, kb, extracted, weakly_similar, patterns):
        pattern_to_search = self._get_numeric_pattern_for(
            kb, extracted, patterns
        )
        if pattern_to_search is None:
            return False
        pattern_number = self._count_pattern(pattern_to_search, patterns)
        return pattern_number >= self.alpha * len(weakly_similar)

    def _get_numeric_pattern_for(self, kb, extracted, patterns):
        for pattern in patterns:
            if self._is_pattern_of(kb, extracted, pattern):
                return self._get_pattern_sequence_numbers(pattern)
        return None

    @staticmethod
    def _is_pattern_of(kb, extracted, pattern):
        return (
            pattern['kb']['_id'] == kb['_id'] and
            pattern['extracted']['_id'] == extracted['_id']
        )

    @staticmethod
    def _get_pattern_sequence_numbers(pattern):
        return pattern['kb']['pattern'] + pattern['extracted']['pattern']

    def _count_pattern(self, pattern_to_search, patterns):
        counter = 0
        for pattern in patterns:
            pattern_as_numbers = self._get_pattern_sequence_numbers(pattern)
            if pattern_to_search == pattern_as_numbers:
                counter += 1
        return counter

    def _get_pattern_for(self, kb, extracted, patterns):
        for pattern in patterns:
            if self._is_pattern_of(kb, extracted, pattern):
                return pattern
        return None

    def _calculate_similarity_by_pattern(self, pattern):
        kb_matching_parts = []
        extracted_matching_parts = []
        for index in pattern['kb']['pattern']:
            if index == 0:
                continue
            kb_matching_parts.append(
                pattern['extracted']['sequence'][index - 1]
            )
        for index in pattern['extracted']['pattern']:
            if index == 0:
                continue
            extracted_matching_parts.append(
                pattern['kb']['sequence'][index - 1]
            )
        kb_string = self.text_processor.join(kb_matching_parts)
        extracted_string = self.text_processor.join(extracted_matching_parts)
        return self.similarity.calculate(kb_string, extracted_string)

    def _calculate_weak_similarity(self, kb, extracted, attribute):
        words_extracted = self.text_processor.split(extracted["value"])
        extracted_phrase = self.text_processor.join(words_extracted)
        words_kb = self.text_processor.split(str(kb[attribute]))
        kb_phrase = self.text_processor.join(words_kb)
        weak_similarity = {
            'kb': kb['_id'],
            'extracted': extracted['_id'],
            'similarity':
                self.similarity.calculate(kb_phrase, extracted_phrase)
        }
        return weak_similarity

    def get_graph_of_matching_corresponding_words(
        self, kb, extracted, attribute
    ):
        words_extracted = self.text_processor.split(extracted["value"])
        words_kb = self.text_processor.split(str(kb[attribute]))
        graph = nx.Graph()
        for kb_id, word_kb in enumerate(words_kb, 1):
            for extracted_id, word_extracted in enumerate(words_extracted, 1):
                similarity = NGram.compare(
                    word_kb, word_extracted, N=self.n_gram
                )
                if similarity >= self.threshold_word_similarity:
                    graph = self._add_word_pair_to_graph(
                        graph, {
                            'kb': {
                                'id': 'kb_' + str(kb_id),
                                'value': word_kb
                            },
                            'extracted':
                                {
                                    'id': 'extracted_' + str(extracted_id),
                                    'value': word_extracted
                                },
                            'similarity': similarity
                        }
                    )
        return graph

    @staticmethod
    def _add_word_pair_to_graph(graph, word_pair):
        kb = word_pair['kb']
        extracted = word_pair['extracted']
        graph.add_edge(
            kb['id'], extracted['id'], weight=word_pair['similarity']
        )
        nx.set_node_attributes(
            graph, {
                kb['id']: {
                    'data': kb['value']
                },
                extracted['id']: {
                    'data': extracted['value']
                },
            }
        )
        return graph
