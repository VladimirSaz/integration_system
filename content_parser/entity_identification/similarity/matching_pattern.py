from content_parser.entity_identification.text_processor import TextProcessor


class MatchingPattern:
    def __init__(self):
        self.text_processor = TextProcessor()

    def get(self, kb, extracted, attribute, graph_edges):
        kb_words = self.text_processor.split(str(kb[attribute]))
        extracted_words = self.text_processor.split(extracted['value'])

        kb_word_sequences, kb_position_sequences = self._get_sequences_for(
            'kb', kb_words, graph_edges
        )
        extracted_word_sequences, extracted_position_sequences = self._get_sequences_for(
            'extracted', extracted_words, graph_edges
        )
        position_sequences = {
            'kb': kb_position_sequences,
            'extracted': extracted_position_sequences
        }
        patterns = self._get_patterns(position_sequences, graph_edges)

        return {
            'kb':
                {
                    '_id': kb['_id'],
                    'sequence': kb_word_sequences,
                    'pattern': patterns['kb']
                },
            'extracted':
                {
                    '_id': extracted['_id'],
                    'sequence': extracted_word_sequences,
                    'pattern': patterns['extracted']
                }
        }

    def _get_sequences_for(self, origin, words, graph_edges):
        word_sequences = []
        word_sequence = []
        position_sequences = []
        position_sequence = []
        is_previous_word_matching = False
        for position, word in enumerate(words, 1):
            node_id = self._compose_node_id(origin, position)
            is_matching_word = self._is_matching_word(node_id, graph_edges)
            is_in_sequence = is_previous_word_matching == is_matching_word
            has_corresponding_sequence = self._has_corresponding_sequence_in_other_origin(
                node_id, graph_edges
            )
            if not is_in_sequence:
                word_sequences = self._update_word_sequences(
                    word_sequence, word_sequences
                )
                position_sequences = self._update_position_sequences(
                    position_sequence, position_sequences
                )
                position_sequence = []
                word_sequence = []
            if is_matching_word and not has_corresponding_sequence:
                word_sequences = self._update_word_sequences(
                    word_sequence, word_sequences
                )
                position_sequences = self._update_position_sequences(
                    position_sequence, position_sequences
                )
                position_sequence = []
                word_sequence = []
            word_sequence.append(word)
            position_sequence.append(position)
            is_previous_word_matching = is_matching_word
        word_sequences = self._update_word_sequences(
            word_sequence, word_sequences
        )
        position_sequences = self._update_position_sequences(
            position_sequence, position_sequences
        )

        return word_sequences, position_sequences

    def _update_word_sequences(self, sequence, sequences):
        if len(sequence) > 0:
            sequences.append(self.text_processor.join(sequence))
        return sequences

    @staticmethod
    def _update_position_sequences(sequence, sequences):
        if len(sequence) > 0:
            sequences.append(sequence)
        return sequences

    @staticmethod
    def _compose_node_id(origin, position):
        return origin + "_" + str(position)

    def _is_matching_word(self, node_id, word_id_pairs):
        pair = self._get_pair_by(node_id, word_id_pairs)
        is_edge_exists = pair is not None
        return is_edge_exists

    @staticmethod
    def _get_pair_by(node_id, word_id_pairs):
        for pair in word_id_pairs:
            for node_id_in_pair in pair:
                if node_id == node_id_in_pair:
                    return pair
        return None

    def _has_corresponding_sequence_in_other_origin(
        self, origin1_node, word_id_pairs
    ):
        pair = self._get_pair_by(origin1_node, word_id_pairs)
        if pair is None:
            return False
        origin2_node = self._get_origin2_node_id(origin1_node, pair)
        before_origin1_node = self._get_previous_node(origin1_node)
        before_origin2_node = self._get_previous_node(origin2_node)
        previous_pair = self._get_pair_by(before_origin2_node, word_id_pairs)
        return (
            previous_pair is not None and
            before_origin1_node in previous_pair and
            before_origin2_node in previous_pair
        )

    def _get_previous_node(self, node):
        origin, origin_node_pos = self._get_origin_and_position(node)
        before_origin_node = self._compose_node_id(origin, origin_node_pos - 1)
        return before_origin_node

    @staticmethod
    def _get_origin2_node_id(origin1_node, pair):
        for node_id in pair:
            if node_id != origin1_node:
                return node_id
        raise ValueError("invalid pair")

    @staticmethod
    def _get_origin_and_position(node_id):
        origin, position = node_id.split("_")
        return origin, int(position)

    def _get_patterns(self, sequences, graph_edges):
        return {
            'kb':
                self._get_pattern('kb', 'extracted', sequences, graph_edges),
            'extracted':
                self._get_pattern('extracted', 'kb', sequences, graph_edges)
        }

    def _get_pattern(self, origin1, origin2, sequences, graph_edges):
        pattern = []
        for origin1_position_sequence in sequences[origin1]:
            origin1_position = origin1_position_sequence[0]
            origin1_node = self._compose_node_id(origin1, origin1_position)
            pair = self._get_pair_by(origin1_node, graph_edges)
            if pair is None:
                pattern.append(0)
                continue
            origin2_node = self._get_origin2_node_id(origin1_node, pair)
            expected_origin2_position = self._get_origin_and_position(
                origin2_node
            )[1]

            pattern.append(
                self._get_matching_position_in_origin2_sequences(
                    expected_origin2_position, sequences[origin2]
                )
            )

        return pattern

    @staticmethod
    def _get_matching_position_in_origin2_sequences(
        expected_position, origin2_sequences
    ):
        for sequence_i, origin2_position_sequence in enumerate(
            origin2_sequences, 1
        ):
            for origin2_position in origin2_position_sequence:
                if origin2_position == expected_position:
                    return sequence_i
        raise ValueError("invalid pair")
