import string
import random

import scrapy
import json

from scrapy.exceptions import CloseSpider
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
import pdb  # noqa
from content_parser.rule import Rule


class RawHtmlCollector(scrapy.Spider):
    """
    Class that extracts raw html. It is a part of
    Scrapy framework.

    Attributes:
        rule_path: a string path to rule file
    """
    name = 'RawHtmlCollector'
    custom_settings = {
        'DOWNLOAD_DELAY': 0.25,
        'LOG_LEVEL': 'INFO',
        'DEPTH_LIMIT': 3,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_crawler_parameters()

    def set_crawler_parameters(self):
        self.validate_options()
        self.file_name_length = 10
        self.rule = Rule(self.rule_path)
        self.start_urls = self.rule.get()["start_urls"]
        self.raw_data_storage_path = self.rule.get()["raw_data_storage_path"]
        self.allowed_domains = self.rule.get()["allowed_domains"]
        self.counter = 0

    def validate_options(self):
        assert hasattr(
            self, 'rule_path'
        ), "Incorrect: add option -a rule_path=..."
        assert hasattr(self, 'domain'), "Incorrect: add option -a domain=..."

    @staticmethod
    def _read_from_json_file(file_path):
        with open(file_path) as f:
            return json.load(f)

    def parse(self, response):
        file_name = self._get_random_file_name()
        file_path = self.raw_data_storage_path + file_name
        with open(file_path, 'wb') as html_file:
            html_file.write(response.body)
        yield {"url": response.url, "storage_path": file_path}

        for url in self.get_each_url_on_page(response):
            yield scrapy.Request(url, callback=self.parse)

    def _get_random_file_name(self):
        random_chars = random.choices(
            string.ascii_lowercase + string.digits, k=self.file_name_length
        )
        return ''.join(random_chars) + '.html'

    def get_each_url_on_page(self, response):
        le = LxmlLinkExtractor(allow_domains=self.allowed_domains, unique=True)
        for link in le.extract_links(response):
            yield link.url
