import scrapy
import json
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
import pdb  # noqa
from content_parser.rule import Rule
from scheme.json_validator import Validator


class CommonScrapper(scrapy.Spider):
    """
    Class that extracts data from html. It is a part of
    Scrapy framework.

    Attributes:
        rule_path: a string path to rule file
        schema_path: a string path to schema file that describes the domain
    """
    name = 'CommonScrapper'
    custom_settings = {
        'DOWNLOAD_DELAY': 0.25,
        'LOG_LEVEL': 'INFO',
        'DEPTH_LIMIT': 3,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_crawler_parameters()

    def set_crawler_parameters(self):
        self.validate_options()
        self.rule = Rule(self.rule_path)
        self.start_urls = self._get_start_urls()
        self.allowed_domains = self.rule.get()["allowed_domains"]
        self.scheme_validator = Validator(
            schema=self._read_from_json_file(self.schema_path)
        )

    def validate_options(self):
        assert hasattr(self, 'rule_path'), "Incorrect: add option -a rule_path=..."
        assert hasattr(self, 'schema_path'), "Incorrect: add option -a schema_path=..."
        assert hasattr(self, 'domain'), "Incorrect: add option -a domain=..."

    def _get_start_urls(self):
        extra_start_urls = []
        if hasattr(self, 'extra_start_urls'):
            extra_start_urls = self.extra_start_urls
        return self.rule.get()["start_urls"] + extra_start_urls

    @staticmethod
    def _read_from_json_file(file_path):
        with open(file_path) as f:
            return json.load(f)

    def parse(self, response):
        document = self.rule.parse(response.body)
        if self.scheme_validator.is_match(document):
            yield document

        for url in self.get_each_url_on_page(response):
            yield scrapy.Request(url, callback=self.parse)

    def get_each_url_on_page(self, response):

        le = LxmlLinkExtractor(allow_domains=self.allowed_domains, unique=True)
        for link in le.extract_links(response):
            yield link.url
