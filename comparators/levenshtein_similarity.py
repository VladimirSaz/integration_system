from comparators.similarity import Similarity
from metrics.levenshtein import Levenshtein


class LevenshteinSimilarity(Similarity):
    """
    See base class comparators.similarity.Similarity
    Calculates Levenshtein similarity normalized by max length of strings

    Attributes:
        fields: a list of attributes of data, to which metrics will be applied.
        name: a name of attribute to which metric is applied
        minimum_score: threshold which every metric should pass to assume that object are similar
    """
    @staticmethod
    def _validate_structure_of_field(field):
        if "name" not in field: raise ValueError
        if "minimum_score" not in field: raise ValueError

    def get_similarities(self, object1, object2):
        """
        See base class.
        """
        self._validate_objects(object1, object2)
        scores = {}
        for field in self.fields:
            metric = Levenshtein()
            score = metric.calculate(object1[field['name']], object2[field['name']])
            scores[field['name']] = score
        return scores

    def are_similar_based_on_scores(self, scores):
        """
        See base class.
        """
        for field in self.fields:
            score = scores[field['name']]
            if score < float(field['minimum_score']): return False
        return True

