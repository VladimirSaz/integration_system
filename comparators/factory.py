from importlib import import_module


class ComparatorFactory:
    """
    Instantiates comparator according to full name of module
    and class passed in params
    """
    def get(self, params):
        """
        Instantiates comparator according to full name of module
        and class passed in params

        Args:
            params: a dictionary with name - full name of module and class, and arguments to pass to that class

        Returns:
            Instance of class that passed in name key in params, with arguments
        """
        self._validate_params(params)
        return self._get_instance(params)

    def _validate_params(self, params):
        if not isinstance(params, dict): raise ValueError
        if 'name' not in params: raise ValueError
        if 'arguments' not in params: raise ValueError

    def _get_instance(self, params):
        parts = params['name'].split(".")
        package_name = '.'.join(parts[:-1])
        class_name = parts[-1]
        module = import_module(package_name)
        _class = getattr(module, class_name)
        return _class(**params['arguments'])
