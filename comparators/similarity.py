from abc import abstractmethod


class Similarity:
    """
    Provides methods to decide whether two objects are similar.
    All similarities that used in integration system must conform
    the interface of this class, and use this class as a parent.

    Attributes:
        fields: a list of attributes of data, to which metrics will be applied.
     """

    def __init__(self, **kwargs):
        if 'fields' not in kwargs: raise ValueError
        self.fields = kwargs['fields']
        if len(self.fields) == 0: raise ValueError
        self._validate_structure_of_fields()

    def _validate_structure_of_fields(self):
        for field in self.fields:
            self._validate_structure_of_field(field)

    @staticmethod
    def _validate_structure_of_field(field):
        if "name" not in field: raise ValueError

    def are_similar(self, object1, object2):
        """
        examines if two objects are similar

        Args:
            object1: a dictionary with extracted data
            object2: a dictionary with extracted data

        Returns:
            True if all metrics greater than the thresholds specified in the fields, otherwise false
        """
        scores = self.get_similarities(object1, object2)
        return self.are_similar_based_on_scores(scores)

    @abstractmethod
    def get_similarities(self, object1, object2):
        """
        returns all similarity metrics of objects
        It is useful if you need to cache results of comparison

        Args:
            object1: a dictionary with extracted data
            object2: a dictionary with extracted data

        Returns:
            dict of similarities
        """
        pass

    @abstractmethod
    def are_similar_based_on_scores(self, scores):
        """
        examines if scores are greater than thresholds in the fields

        Args:
            scores: list of similarity scores of two objects

        Returns:
            True if all scores greater than the thresholds specified in the fields, otherwise false
        """
        pass

    def _validate_objects(self, object1, object2):
        for field in self.fields:
            if field['name'] not in object1: raise ValueError
            if field['name'] not in object2: raise ValueError
