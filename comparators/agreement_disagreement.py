from comparators.similarity import Similarity
from dateutil import relativedelta


class AgreementDisagreement(Similarity):
    """
    See base class comparators.similarity.Similarity

    Attributes:
        fields: a list of attributes of data, to which metrics will be applied.
        name: a name of attribute to which metric is applied
        minimum_score: threshold which every metric should pass to assume that object are similar
        weight: number which denotes the weight of attribute
        temporal_coefficient: dictionary that contains:

            - span - a integer number of time span (3 months for example)

            - type - a string "agreement" or "disagreement", which denotes type of metric

            - span_type - a string value of span type (for example: "year" or "month" or "day")

            - value - the probability of event to happen

    Example:
    ::

        fields = [
            {
                "name": "name",
                "weight": 0.4,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'agreement',
                    'value': 0.001
                }
            },
            {
                "name": "profession",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'disagreement',
                    'value': 0.8
                }
            },
            {
                "name": "home_airport",
                "weight": 0.2,
                "temporal_coefficient": {
                    'span': 4,
                    'span_type': 'month',
                    'type': 'agreement',
                    'value': 0.01
                }
            },
            {
                "name": "co_travellers",
                "weight": 0.2
            }
        ]
        parameters = {
            'fields': fields,
            'minimum_score': "0.85",
        }


    """

    def __init__(self, **kwargs):
        Similarity.__init__(self, **kwargs)
        self._validate_custom_weights()
        self._validate_temporal_coefficients()
        self.minimum_score = float(kwargs['minimum_score'])

    def _validate_custom_weights(self):
        for field in self.fields:
            if 'weight' in field and field['weight'] < 0:
                raise ValueError("weight less than zero")

    def _validate_temporal_coefficients(self):
        for field in self.fields:
            if 'temporal_coefficient' not in field: continue
            temporal_value = field['temporal_coefficient']['value']
            if temporal_value < 0 or temporal_value > 1:
                raise ValueError("temporal coefficient is not between 0 and 1")

    def are_similar_based_on_scores(self, scores):
        """
        See base class.

        Expects the list of scores that have 1 value
        """
        if len(scores) != 1: raise ValueError
        return scores[0] >= self.minimum_score

    def get_similarities(self, object1, object2):
        """
        See base class.

        Returns 1 score as a list
        """
        self._validate_objects(object1, object2)
        weights = self._get_weights()
        temporals = self._get_temporal_coefficients(object1, object2)
        similarities = self._get_similarities(object1, object2)

        numerator = 0
        for similarity, weight, temporal in zip(similarities, weights, temporals):
            numerator += similarity * weight * temporal

        denominator = 0
        for weight, temporal in zip(weights, temporals):
            denominator += weight * temporal

        score = numerator / denominator
        return [score]

    def _get_values_of_fields(self, object_):
        values = []
        for field in self.fields:
            values.append(self._get_value_from_object(object_, field))
        return values

    @staticmethod
    def _get_value_from_object(object_to_compare, field):
        if object_to_compare[field['name']] is None: return ""
        return object_to_compare[field['name']]

    def _get_weights(self):
        weights = []
        for field in self.fields:
            if 'weight' in field:
                weights.append(field['weight'])
            else:
                weights.append(1)
        return weights

    def _get_temporal_coefficients(self, object1, object2):
        coefficients = []
        for field in self.fields:
            if 'temporal_coefficient' in field:
                coefficients.append(
                    self._get_temporal_coefficient(field, object1, object2)
                )
            else:
                coefficients.append(1)
        return coefficients

    def _get_temporal_coefficient(self, field, object1, object2):
        periods_in_span_count = self._get_time_span_periods(field, object1, object2)
        if periods_in_span_count == 0: return 1
        probability = 1 - field['temporal_coefficient']['value']
        return 1 - (1 - probability) ** periods_in_span_count

    @staticmethod
    def _get_time_span_periods(field, object1, object2):
        span_type = field['temporal_coefficient']['span_type']
        span_size = field['temporal_coefficient']['span']
        date1 = object1['date']
        date2 = object2['date']
        r = relativedelta.relativedelta(object1['date'], object2['date'])
        if span_type == 'year':
            return abs(r.years) / span_size
        if span_type == 'month':
            return abs(r.years * 12 + r.months) / span_size
        if span_type == 'day':
            return abs((date1 - date2).days) / span_size
        raise ValueError("incorrect span_type")

    def _get_similarities(self, object1, object2):
        values_of_object1 = self._get_values_of_fields(object1)
        values_of_object2 = self._get_values_of_fields(object2)
        similarities = []
        for i in range(len(values_of_object1)):
            if values_of_object1[i] == values_of_object2[i]:
                similarities.append(1)
            else:
                similarities.append(0)
        return similarities

    def _get_jaccard_similarity(self, object1, object2):
        values_of_object1 = []
        values_of_object2 = []
        for field in self.fields:
            values_of_object1.append(self._get_value_from_object(object1, field))
            values_of_object2.append(self._get_value_from_object(object2, field))

        #jaccard similarity defined in sklearn, _must not be imported
        #or u will get crashes
        return jaccard_similarity_score(values_of_object1, values_of_object2)
