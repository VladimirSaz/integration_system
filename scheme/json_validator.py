import json
import jsonschema


class Validator:
    """
    Validates if object match the scheme.

    Attributes:
        schema: a dictionary that describes the data schema.
     """

    def __init__(self, schema):
        self.schema = schema

    def is_match(self, object):
        """
        Checks if given object matches the schema

        Args:
            object: a dictionary with extracted data

        Returns:
            True if object matches the schema, otherwise false
        """
        if not isinstance(object, dict):
            raise ValueError
        return jsonschema.Draft6Validator(self.schema).is_valid(object)
