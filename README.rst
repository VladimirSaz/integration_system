======
Big Data Integration System
======

Documentation
=============

Documentation is available online at https://big-data-integration-system.readthedocs.io/ and in the ``docs``
directory.
