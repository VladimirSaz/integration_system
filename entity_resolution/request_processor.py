import pdb # noqa
from pprint import pprint #noqa
from request_parser import RequestParser
from udf import UDF
from entity_resolver import EntityResolver


class RequestProcessor:
    hidden_id = '__id'

    def execute(self, raw_request, input_databases):
        request_parser = RequestParser()
        request = request_parser.parse(raw_request)
        data = self.load_sources(input_databases, request['sources'])
        entity_resolver = EntityResolver(self.hidden_id)
        data = entity_resolver.resolve(data, request["fuse_by"])
        return self.select_by(data, request)

    def load_sources(self, input_databases, sources):
        data = {}
        for source in sources:
            database = self.get_database_for_source(input_databases, source)
            data[source] = self.load_data(database)
        return data

    def get_database_for_source(self, input_databases, source):
        for database in input_databases:
            if (source == database['params']['name']):
                return database
        raise KeyError("Cant find source with name"+source)

    def load_data(self, database):
        adapter = database['adapter']
        params = database['params']
        return adapter(params)

    def select_by(self, data, request):
        data_by_entity = self.get_data_by_entity_id(data, request)
        resolved_data = self.apply_resolve_functions(data_by_entity, request)
        return self.clear_and_convert_to_list(resolved_data)

    def get_data_by_entity_id(self, data, request):
        data_by_entity = {}
        for source_name, entries in data.items():
            for entry in entries:
                data_by_entity = self.add_entry_to_data(data_by_entity, entry)
        return data_by_entity

    def add_entry_to_data(self, data_by_entity, entry):
        key = entry['__id']
        if key in data_by_entity:
            data_by_entity[key].append(entry)
        else:
            data_by_entity[key] = [entry]
        return data_by_entity

    def apply_resolve_functions(self, data, request):
        for entity_id, entries in data.items():
            data[entity_id] = self.apply_functions(entries, request)
        return data

    def apply_functions(self, entries, request):
        for resolve_request in request['attributes']:
            attribute_name = resolve_request['name']
            values_of_attribute = [entry[attribute_name] for entry in entries]
            function_name = resolve_request['function_to_apply']['name']
            function_params = resolve_request['function_to_apply']['params']
            resolved_value = UDF().execute(
                function_name,
                values_of_attribute,
                function_params
            )
            entries[0][attribute_name] = resolved_value
        return entries[0]

    def clear_and_convert_to_list(self, resolved_data):
        entries = []
        for key, entry in resolved_data.items():
            entry.pop(self.hidden_id, None)
            entries.append(entry)
        return entries
