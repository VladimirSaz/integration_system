import sys
sys.path.append('/Users/vladimir/integration_system/tools') # noqa
from metrics.levenshtein import Levenshtein


class EntityResolver:
    def __init__(self, hidden_id):
        self.hidden_id = hidden_id

    def resolve(self, data, fuse_by):
        entity_id = 0
        for source_name, entries in data.items():
            for entry in entries:
                if self.is_resolved(entry):
                    continue
                entry[self.hidden_id] = entity_id
                data = self.set_id_to_similar_entries(entry, fuse_by, data)
                entity_id += 1
        return data

    def is_resolved(self, entry):
        return (self.hidden_id in entry)

    def set_id_to_similar_entries(self, entry_to_match, fuse_by, data):
        for source_name, entries in data.items():
            for entry in entries:
                if self.is_resolved(entry):
                    continue
                if self.is_similar(entry, entry_to_match, fuse_by):
                    entry[self.hidden_id] = entry_to_match[self.hidden_id]
        return data

    def is_similar(self, entry, entry_to_match, fuse_by):
        attribute_for_similarity = fuse_by['name']
        function_to_apply = fuse_by['function_to_apply']
        value1 = entry[attribute_for_similarity]
        value2 = entry_to_match[attribute_for_similarity]
        score = self.get_similarity_score(value1, value2, function_to_apply)
        return self.is_this_score_better(score, function_to_apply)

    def get_similarity_score(self, value1, value2, function_to_apply):
        name = function_to_apply['name']
        if (name == 'LevenshteinDistance'):
            metric = Levenshtein()
            score = metric.calculate(value1, value2)
            return score
        raise KeyError('No such metric:'+name)

    def is_this_score_better(self, score, function_to_apply):
        name = function_to_apply['name']
        params = function_to_apply['params']
        if (name == 'LevenshteinDistance'):
            minimum_score = float(params)
            return (score >= minimum_score)
        raise KeyError('No such metric:'+name)
