from pyspark.sql import DataFrame
from graphframes import *
from functools import reduce


class Clustering:
    """
    Class that clusters data, where each cluster is some entity.
    This is a wrapper over ConnectedComponents from graphframes package
    for pyspark.

    Attributes:
        sqlContext: sql context of pyspark
        checkpointInterval: a number of checkpoints in execution where algorithm can store
        temporary results(zero is not recommended, default=2 )

    """
    def __init__(self, sqlContext, checkpointInterval=2):
        self.sqlContext = sqlContext
        self.checkpointInterval = checkpointInterval

    def cluster(self, dataframes, pairwise_matching_graph):
        """
        Assigns cluster id to each record of dataframe (of dataframes)

        Args:
            dataframes: a list of dataframes of pyspark, that contains extracted data (treated as one dataframe)
            pairwise_matching_graph: a dataframe with rows as {src=id1, dst=id2} which denotes that pairs match

        Returns:
            dataframe with records from input dataframes with new attribute component
        """
        self._validate_params(dataframes, pairwise_matching_graph)
        vertices = self._get_vertices(dataframes)
        edges = pairwise_matching_graph
        return self._connected_components_on_spark(vertices, edges)

    @staticmethod
    def _validate_params(dataframes, pairwise_matching_graph):
        if len(dataframes) == 0: raise ValueError
        if not pairwise_matching_graph: raise ValueError

    def _get_vertices(self, dataframes):
        vertices = self._union_all(*dataframes)
        return vertices.withColumnRenamed("_id", "id")

    @staticmethod
    def _union_all(*dfs):
        return reduce(DataFrame.union, dfs)

    def _connected_components_on_spark(self, vertices, edges):
        graph = GraphFrame(vertices, edges)
        result = graph.connectedComponents(checkpointInterval=self.checkpointInterval)
        return result.withColumnRenamed("id", "_id")
