from pyspark.sql import DataFrame, Row
from pyspark.sql.types import StructField, StructType, LongType
from functools import reduce
from pyspark.sql.functions import lit
import numpy as np


class Clustering:
    """
    Class that clusters data, where each cluster is some entity.
    Address the velocity problem of big data, it stores previous
    steps of pairwise matching and clustering

    Attributes:
        sqlContext: sql context of pyspark
        pairwise_matcher: instance of class that matches pairs (example entity_resolution.pairwise_matching.pair_range_pairwise_matcher_wi th_average_similarity.PairwiseMatcher)
        pair_composer: instance of class that composes list of pairs to match (example entity_resolution.blocking.pair_range.PairRange)

    """
    def __init__(self, sqlContext, pairwise_matcher, pair_composer):
        self.sqlContext = sqlContext
        self.pairwise_matcher = pairwise_matcher
        self.pair_composer = pair_composer
        self._max_component_id = None
        self._splitted_component_ids = []

    def cluster(self, data, delta_data, pairwise_graph):
        """
        Assigns cluster id to each record of dataframe (of dataframes)

        Args:
            data: a dataframe that contains list of records from previous iterations, each record should have component attribute
            delta_data: a dataframe that contains list of new records
            pairwise_matching_graph: a dataframe with rows as {src=id1, dst=id2} which denotes that pairs match

        Returns:
            dataframe with records from input dataframes with new attribute component
            pairwise_matching_graph: a dataframe with updated rows as {src=id1, dst=id2, average_similarity=float}
        """
        if self._is_missing_pairwise_graph(data, pairwise_graph):
            pairwise_graph = self._get_pairwise_graph(data)

        self._cache_dataframes([data, delta_data])
        self._max_component_id = self._get_max_component_id_of(data) + 1
        working_queue = self._get_working_queue(delta_data)
        while len(working_queue) > 0:
            data, working_queue, pairwise_graph = self._update_clusters(
                pairwise_graph,
                data,
                working_queue
            )
        return self._compress_component_ids(data), self.sqlContext.createDataFrame(pairwise_graph)

    @staticmethod
    def _is_missing_pairwise_graph(data, pairwise_graph):
        return data is not None and pairwise_graph is None

    def _get_pairwise_graph(self, data):
        pairs = self.pair_composer.get([self._get_as_dict_collection(data)], )
        return self.pairwise_matcher.match([data], pairs).collect()

    def _update_clusters(self, pairwise_graph, data, working_queue):
        is_changed_on_merge = False
        delta_cluster = working_queue.pop()
        delta_component_id = self._get_component_id(delta_cluster)
        if data is None:
            data = self._union_data(data, delta_cluster)
            return data, working_queue, pairwise_graph

        clusters = self._get_clusters(data)
        for cluster in clusters:
            cluster_component_id = self._get_component_id(cluster)
            if self._are_clusters_already_splitted(cluster_component_id, delta_component_id):
                continue

            is_changed, pairwise_graph = self._is_should_be_merged(
                cluster, delta_cluster, pairwise_graph
            )

            if is_changed:
                is_changed_on_merge = True
                working_queue, data = self._merge_cluster(
                    cluster,
                    delta_cluster,
                    data,
                    working_queue
                )
            if is_changed_on_merge:
                break

        is_changed_on_split = False
        if is_changed_on_merge:
            is_changed_on_split, working_queue = self._split_cluster_if_possible(
                cluster_to_split=working_queue.pop(),
                pairwise_graph=pairwise_graph,
                working_queue=working_queue
            )

        if is_changed_on_split:
            is_changed, working_queue = self._move_from_cluster_to_cluster_if_possible(
                pairwise_graph=pairwise_graph,
                working_queue=working_queue
            )

        if not is_changed_on_merge:
            data = self._union_data(data, delta_cluster)

        return data, working_queue, pairwise_graph

    @staticmethod
    def _get_component_id(cluster):
        return cluster.limit(1).collect()[0]['component']

    def _union_data(self, data, delta_data):
        data = self._union_all(data, delta_data)
        return data.drop_duplicates(subset=['_id'])

    @staticmethod
    def _cache_dataframes(dataframes):
        for dataframe in dataframes:
            if dataframe is not None: dataframe.persist()

    def _get_working_queue(self, dataframe):
        queue = []
        for row in dataframe.collect():
            queue.append(self.sqlContext.createDataFrame([Row(**row.asDict(), component=self._max_component_id)]))
            self._max_component_id += 1
        return queue

    @staticmethod
    def _get_max_component_id_of(dataframe):
        if dataframe is None: return -1
        return dataframe.agg({"component": "max"}).collect()[0]["max(component)"]

    def _get_clusters(self, data):
        clusters = []
        clusters_ids = self._get_unique_values_of_field(data, 'component')
        for _id in clusters_ids:
            cluster = data.filter(data["component"] == _id)
            clusters.append(cluster)
        return clusters

    def _are_clusters_already_splitted(self, component_id1, component_id2):
        pair = [min(component_id1, component_id2), max(component_id1, component_id2)]
        return pair in self._splitted_component_ids

    def _is_should_be_merged(self, cluster1, cluster2, pairwise_graph):
        # this done, because you cant fetch ids and count later,
        # otherwise it will hurt performance
        cluster1_ids = self._get_values_of_field(cluster1, "_id")
        cluster2_ids = self._get_values_of_field(cluster2, "_id")
        pairwise_graph = self._get_pairwise_graph_of_merged_clusters(cluster1, cluster2, pairwise_graph)

        sum_of_similarities = self._get_correlation_clustering_similarity_sum(
            cluster1_ids, cluster2_ids, pairwise_graph
        )
        threshold = len(cluster1_ids) * len(cluster2_ids) / 2

        return sum_of_similarities > threshold, pairwise_graph

    def _get_pairwise_graph_of_merged_clusters(self, cluster1, cluster2, pairwise_graph):
        new_pairs = self._get_pairs_of_rows_to_compare(
            cluster1, cluster2, pairwise_graph
        )
        clusters = []
        for cluster in [cluster1, cluster2]:
            if cluster is not None:
                clusters.append(cluster.drop('component'))
        delta_pairwise_graph = self.pairwise_matcher.match(clusters, new_pairs).collect()
        if self._is_no_records(pairwise_graph): return delta_pairwise_graph
        return pairwise_graph + delta_pairwise_graph

    def _get_pairs_of_rows_to_compare(self, cluster1, cluster2, pairwise_graph):
        all_pairs = self._get_all_new_pairs(cluster1, cluster2)
        if self._is_no_records(pairwise_graph): return all_pairs

        pairs = []
        for pair in all_pairs:
            if pair not in pairwise_graph: pairs.append(pair)
        return pairs

    def _get_all_new_pairs(self, old_cluster, new_cluster):
        pairs = self.pair_composer.get([self._get_as_dict_collection(new_cluster)], )
        if self._df_len(old_cluster) == 0: return pairs

        pairs += self._get_pairs_between_2_clusters(old_cluster, new_cluster)
        return pairs

    @staticmethod
    def _get_as_dict_collection(dataframe):
        return [row.asDict() for row in dataframe.collect()]

    def _get_pairs_between_2_clusters(self, cluster1, cluster2):
        pairs = []
        for row_of_cluster2 in cluster2.collect():
            for row_of_cluster1 in cluster1.collect():
                pairs.append([
                    row_of_cluster1['_block_id'],
                    row_of_cluster1['_id'],
                    row_of_cluster2['_id']
                ])
        return pairs

    @staticmethod
    def _is_no_records(data):
        return data is None or len(data) == 0

    @staticmethod
    def _df_len(dataframe):
        if dataframe is None: return 0
        return dataframe.count()

    def _get_correlation_clustering_similarity_sum(
            self, cluster1_ids, cluster2_ids, pairwise_graph):
        correlation_clustering_similarity = 0
        for edge in pairwise_graph:
            if self._is_edge_in_cluster(edge, cluster1_ids, cluster2_ids):
                correlation_clustering_similarity += 1 - edge['average_similarity']
            else:
                correlation_clustering_similarity += edge['average_similarity']
        return correlation_clustering_similarity

    def _is_edge_in_cluster(self, edge, cluster1_ids, cluster2_ids):
        if self._is_edge_in_cluster_n(edge, cluster1_ids): return True
        if self._is_edge_in_cluster_n(edge, cluster2_ids): return True
        return False

    @staticmethod
    def _is_edge_in_cluster_n(edge, cluster_ids):
        return edge['src'] in cluster_ids and edge['dst'] in cluster_ids

    def _merge_cluster(self, cluster, delta_cluster, data, working_queue):
        new_cluster = self._union_all(cluster, delta_cluster)
        component_id = self._get_new_component_id()
        new_cluster = new_cluster.withColumn('component', lit(component_id))
        working_queue.append(new_cluster)
        data = self._remove_cluster(cluster, data)
        return working_queue, data

    def _get_new_component_id(self):
        self._max_component_id += 1
        return self._max_component_id

    @staticmethod
    def _union_all(*dfs):
        dfs_to_union = list(filter(None, dfs))
        columns = dfs_to_union[0].schema.names
        dfs_to_union = [df.select(*columns) for df in dfs_to_union]
        return reduce(DataFrame.union, dfs_to_union)

    def _remove_cluster(self, cluster, data):
        ids_in_clusters = self._get_values_of_field(cluster, '_id')
        for _id in ids_in_clusters:
            data = data.filter(data["_id"] != _id)
        return data

    @staticmethod
    def _get_values_of_field(dataframe, field_name):
        return [x[0] for x in dataframe.select(field_name).collect()]

    @staticmethod
    def _get_unique_values_of_field(dataframe, field_name):
        return [x[0] for x in dataframe.select(field_name).distinct().collect()]

    def _split_cluster_if_possible(self, cluster_to_split, pairwise_graph, working_queue):
        cluster1 = []
        cluster2 = cluster_to_split.collect()
        while True:
            min_node, id_of_min_node, lowest_difference = self._get_node_with_lowest_connectivity_difference(
                cluster1, cluster2,
                pairwise_graph
            )

            if lowest_difference > ((len(cluster2) - len(cluster1) - 1) / 2):
                break

            cluster1, cluster2 = self._move_node_from_cluster_to_cluster(
                cluster_move_to=cluster1,
                cluster_move_from=cluster2,
                node=min_node,
                id_of_node=id_of_min_node
            )
        cluster1 = self._set_new_max_component_id(cluster1)
        cluster1_component_id = self._max_component_id
        self._register_component_ids_as_splitted(
            cluster1_component_id,
            cluster2[0]['component']
        )
        return self._add_clusters_to_working_queue(cluster1, cluster2, working_queue)

    def _register_component_ids_as_splitted(self, component_id1, component_id2):
        pair = [min(component_id1, component_id2), max(component_id1, component_id2)]
        self._splitted_component_ids.append(pair)

    def _get_node_with_lowest_connectivity_difference(self, cluster1, cluster2, pairwise_graph):
        min_difference = None
        min_node = None
        id_of_min_node = None
        for id_of_node, node in enumerate(cluster2):
            connectivity_cluster1 = self._connectivity(
                node['_id'], cluster1, pairwise_graph
            )
            connectivity_cluster2 = self._connectivity(
                node['_id'], cluster2, pairwise_graph
            )
            difference = connectivity_cluster2 - connectivity_cluster1
            if min_difference is None or min_difference > difference:
                min_difference = difference
                min_node = node
                id_of_min_node = id_of_node
        return min_node, id_of_min_node, min_difference

    def _connectivity(self, node_id, cluster, pairwise_graph):
        sum_of_connectivities = 0
        if len(cluster) <= 1: return 0
        for node in cluster:
            node_id_of_cluster = node['_id']
            if node_id_of_cluster == node_id: continue
            sum_of_connectivities += self._similarity_of_2_nodes(
                node_id, node_id_of_cluster, pairwise_graph
            )
        return sum_of_connectivities

    def _similarity_of_2_nodes(self, node_id1, node_id2, pairwise_graph):
        for edge in pairwise_graph:
            if edge['src'] == edge['dst']: continue
            if self._is_edge_of_nodes(edge, node_id1, node_id2):
                return edge['average_similarity']
        raise ValueError

    def _move_node_from_cluster_to_cluster(self, cluster_move_to, cluster_move_from, node, id_of_node):
        cluster_move_to.append(node)
        cluster_move_from.pop(id_of_node)
        return cluster_move_to, cluster_move_from

    def _set_new_max_component_id(self, cluster):
        component_id = self._get_new_component_id()
        for i, row in enumerate(cluster):
            new_row_data = {
                **row.asDict(),
                'component': component_id
            }
            cluster[i] = Row(**new_row_data)
        return cluster

    def _add_clusters_to_working_queue(self, cluster1, cluster2, working_queue):
        if len(cluster1) == 0:
            working_queue.append(self.sqlContext.createDataFrame(cluster2))
            return False, working_queue
        else:
            working_queue.append(self.sqlContext.createDataFrame(cluster1))
            working_queue.append(self.sqlContext.createDataFrame(cluster2))
            return True, working_queue

    @staticmethod
    def _is_edge_of_nodes(edge, node1, node2):
        if edge['src'] != node1 and edge['dst'] != node1: return False
        if edge['src'] != node2 and edge['dst'] != node2: return False
        return True

    def _compress_component_ids(self, df):
        df_compressed = self._get_dataframe_with_compressed_component_ids(df)
        df = df.drop('component')
        df = df.join(df_compressed, df._id == df_compressed._id)
        df = df.drop(df_compressed._id)
        return df

    def _get_dataframe_with_compressed_component_ids(self, df):
        ids = self._get_values_of_field(df, '_id')
        component_ids = self._get_values_of_field(df, 'component')
        compressed_component_ids = self._get_compressed_list_of_ids(component_ids)
        return self.sqlContext.createDataFrame(
            zip(ids, compressed_component_ids),
            schema=StructType([
                StructField("_id", LongType(), True),
                StructField("component", LongType(), True)
            ])
        )

    def _move_from_cluster_to_cluster_if_possible(self, pairwise_graph, working_queue):
        cluster1_df = working_queue.pop()
        cluster2_df = working_queue.pop()

        cluster1 = cluster1_df.collect()
        cluster2 = cluster2_df.collect()
        cluster1_component_id = cluster1[0]['component']
        cluster2_component_id = cluster2[0]['component']
        is_changed_once = False
        while True:
            is_move_from_cluster2 = True
            min_node, id_of_min_node, lowest_difference = self._get_node_with_lowest_connectivity_difference(
                cluster1, cluster2,
                pairwise_graph
            )

            if (lowest_difference > ((len(cluster2) - len(cluster1) - 1) / 2)):
                is_move_from_cluster2 = False
                min_node, id_of_min_node, lowest_difference = self._get_node_with_lowest_connectivity_difference(
                    cluster2, cluster1,
                    pairwise_graph
                )

            if (lowest_difference >= ((len(cluster2) - len(cluster1) - 1) / 2)):
                break

            if is_move_from_cluster2:
                cluster1, cluster2 = self._move_node_from_cluster_to_cluster(
                    cluster_move_to=cluster1,
                    cluster_move_from=cluster2,
                    node=min_node,
                    id_of_node=id_of_min_node
                )
            else:
                cluster2, cluster1 = self._move_node_from_cluster_to_cluster(
                    cluster_move_to=cluster2,
                    cluster_move_from=cluster1,
                    node=min_node,
                    id_of_node=id_of_min_node
                )

            is_changed_once = True

        if not is_changed_once:
            working_queue.append(cluster1_df)
            working_queue.append(cluster2_df)
            return False, working_queue

        cluster1 = self._set_component_id_to_cluster(cluster1, cluster1_component_id)
        cluster2 = self._set_component_id_to_cluster(cluster2, cluster2_component_id)

        working_queue.append(self.sqlContext.createDataFrame(cluster1))
        working_queue.append(self.sqlContext.createDataFrame(cluster2))
        return True, working_queue

    @staticmethod
    def _set_component_id_to_cluster(cluster, component_id):
        for i, row in enumerate(cluster):
            new_row_data = {
                **row.asDict(),
                'component': component_id
            }
            cluster[i] = Row(**new_row_data)
        return cluster

    def _get_compressed_list_of_ids(self, ids):
        mapping = self._get_mapper_to_compressed_ids(ids)

        compressed_ids = []
        for _id in ids:
            compressed_ids.append(mapping[_id])

        return compressed_ids

    @staticmethod
    def _get_mapper_to_compressed_ids(ids):
        map = {}
        for i, component_id in enumerate(set(ids)):
            map[component_id] = i
        return map
