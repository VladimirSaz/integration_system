import re


class UDF():
    def __init__(self):
        self.functions = {
            'Longest': self.get_longest_text,
            'MinimumValue': self.get_minimum_value
        }

    def execute(self, function_name, values, params):
        if function_name not in self.functions:
            raise KeyError('No such user defined function:'+function_name)
        function = self.functions[function_name]
        values = self.clear_values(values)
        return function(values, params)

    def clear_values(self, values):
        values = [value.strip() for value in values]
        values = [value.strip("\n") for value in values]
        return values

    def get_longest_text(self, texts, params):
        return max(texts, key=len)

    def get_minimum_value(self, floats_as_strings, params):
        floats = [self.to_float(float_str) for float_str in floats_as_strings]
        return min(floats)

    def to_float(self, string):
        string = string.replace(" ",'')
        regexp = re.search(r'(\d+\.?\d*)', string, re.M | re.I)
        if not regexp:
            return 0
        float_as_string = regexp.group(1)
        return float(float_as_string)
