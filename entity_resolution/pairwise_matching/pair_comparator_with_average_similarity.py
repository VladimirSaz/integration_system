import numpy as np
from pyspark.sql import *
from entity_resolution.pairwise_matching.pair_comparator import PairComparator as BasePairComparator


class PairComparator(BasePairComparator):
    """
    See base class.
    """
    def get_compared_pairs(self, partition, pairs):
        """
        Compares pairs of records with comparators

        Args:
            partition: a partition of dataframe of pyspark, that contains extracted data
            pairs: a list of triplets, with information rows should be compared to which

        Yields:
            list of pairs as a dict with average similarity
            (ignores whether comparator returned True or False)
            {src=id1, dst=id2, average_similarity=float}
        """
        data = list(partition)
        for row1_counter, row1 in enumerate(data):
            for row2_counter, row2 in enumerate(data):
                if row2_counter <= row1_counter: continue
                if not self._is_pair_in_pairs(pairs, row1['_id'], row2['_id']):
                    continue
                scores = self._get_scores_per_comparator(row1, row2)
                yield Row(
                    src=row1['_id'],
                    dst=row2['_id'],
                    average_similarity=self._get_average_similarity(scores),
                )

    @staticmethod
    def _is_pair_in_pairs(pairs, id1, id2):
        pairs_for_id1 = pairs[np.where(pairs[:, 1] == id1)]
        pairs_for_id1_id2 = pairs_for_id1[np.where(pairs_for_id1[:, 2] == id2)]

        if len(pairs_for_id1_id2) > 1: raise ValueError
        return len(pairs_for_id1_id2) == 1

    def _get_scores_per_comparator(self, row1, row2):
        scores_per_comparator = []
        for comparator in self.comparators:
            scores_per_comparator.append(comparator.get_similarities(row1, row2))
        return scores_per_comparator

    def _get_average_similarity(self, scores_per_comparator):
        score_sum = 0
        score_count = 0
        for scores in scores_per_comparator:
            if isinstance(scores, dict):
                score_sum += sum(scores.values())
            else:
                score_sum += sum(scores)
            score_count += len(scores)
        return score_sum / score_count
