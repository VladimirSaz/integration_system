from entity_resolution.pairwise_matching.partitioning_by_pairs import PartitioningByPairs
from entity_resolution.pairwise_matching.pair_comparator import PairComparator
import numpy as np
from pyspark.sql.types import *


class PairwiseMatcher:
    """
    Class that compares pairs of records in distributed computational
    environment with Spark. This class considers pair range results,
    and even out the load on clusters

    Attributes:
        comparators: a list of instances of comparators.similarity.Similarity
        partitions: a number of partitions (recommended: set equal, or to be multiple of number of cores)
        sqlContext: sql context of pyspark

    """
    def __init__(self, **kwargs):
        self.comparators = kwargs['comparators']
        self.partitions = kwargs['partitions']
        self.sqlContext = kwargs['sqlContext']

    def match(self, dataframes, pairs):
        """
        Compares pairs of records based on identifiers in each triplet of pair

        Args:
            dataframes: a list of dataframes of pyspark, that contains extracted data (treated as one dataframe)
            pairs: a list of triplets, with information rows should be compared to which (block_id; src; dst)

        Returns:
            list of pairs that match as a dict {src=id1, dst=id2}
        """
        if len(pairs) == 0: return self._get_empty_pairs_df()
        pairs = self._order_pairs(pairs)
        self._validate_pairs(pairs)
        return self._get_compared_pairs_with_spark(dataframes, pairs)

    def _get_empty_pairs_df(self):
        schema = StructType([
            StructField("src", LongType()),
            StructField("dst", LongType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)

    @staticmethod
    def _validate_pairs(pairs):
        unique_rows = np.unique(pairs, axis=0)
        if len(pairs) != len(unique_rows): raise ValueError

    def _get_compared_pairs_with_spark(self, dataframes, pairs):
        pair_comparator = self._get_pair_comparator()
        partitioner = PartitioningByPairs(
            sqlContext=self.sqlContext,
            partitions=self.partitions
        )

        def f(partition):
            return pair_comparator.get_compared_pairs(partition, pairs)

        partitioned_df = partitioner.get(dataframes, pairs=pairs)
        matched_pairs = partitioned_df.rdd.mapPartitions(f)
        matched_pairs = self._convert_to_df(matched_pairs)
        if matched_pairs.count() != len(pairs): raise ValueError
        return self._post_process_matched_pairs(matched_pairs)

    def _get_pair_comparator(self):
        return PairComparator(self.comparators)

    def _order_pairs(self, pairs):
        pairs = np.unique(pairs, axis=0)
        pairs = pairs[np.argsort(pairs[:, 1])].tolist()
        pairs = self._swap_source_and_destination_if_source_greater_destination(pairs)
        result_pairs = []
        for pair in pairs:
            if pair[1] == pair[2]: continue
            if pair[1] > pair[2]:
                result_pairs.append([pair[0], pair[2], pair[1]])
            else:
                result_pairs.append([pair[0], pair[1], pair[2]])

        return np.array(result_pairs)

    @staticmethod
    def _swap_source_and_destination_if_source_greater_destination(pairs):
        result_pairs = []
        for pair in pairs:
            if pair[1] == pair[2]: continue
            if pair[1] > pair[2]:
                result_pairs.append([pair[0], pair[2], pair[1]])
            else:
                result_pairs.append([pair[0], pair[1], pair[2]])
        return result_pairs

    def _post_process_matched_pairs(self, matched_pairs):
        matched_pairs = matched_pairs.orderBy('src')
        return matched_pairs.where(matched_pairs.are_equal).drop('are_equal')

    @staticmethod
    def _force_to_execute_spark_actions(rdd):
        rdd.count()

    def _convert_to_df(self, rdd):
        if rdd.count() == 0: return self._get_empty_matched_pairs()
        return rdd.toDF().dropDuplicates()

    def _get_empty_matched_pairs(self):
        schema = StructType([
            StructField("src", LongType()),
            StructField("dst", LongType()),
            StructField("are_equal", BooleanType()),
        ])
        return self.sqlContext.createDataFrame([], schema=schema)
