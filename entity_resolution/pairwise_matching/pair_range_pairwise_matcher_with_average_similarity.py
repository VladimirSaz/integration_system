from entity_resolution.pairwise_matching.pair_range_pairwise_matcher import PairwiseMatcher as BasePairwiseMatcher
from entity_resolution.pairwise_matching.pair_comparator_with_average_similarity import PairComparator


class PairwiseMatcher(BasePairwiseMatcher):
    """
    Class that compares pairs of records in distributed computational
    environment with Spark. This class considers pair range results,
    and even out the load on clusters

    This class uses pair_comparator_with_average_similarity module, hence
    on match returns list of all pairs with average similarity.

    Attributes:
        comparators: a list of instances of comparators.similarity.Similarity
        partitions: a number of partitions (recommended: set equal, or to be multiple of number of cores)
        sqlContext: sql context of pyspark

    """
    def _get_pair_comparator(self):
        return PairComparator(self.comparators)

    def _post_process_matched_pairs(self, matched_pairs):
        return matched_pairs.orderBy('src')
