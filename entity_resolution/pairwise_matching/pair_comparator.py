import numpy as np
from pyspark.sql import *


class PairComparator:
    """
    Class that compares pair of records in partitions in distributed computational
    environment with Spark. This class is transported to each node for execution

    Attributes:
        comparators: a list of instances of comparators.similarity.Similarity

    """
    def __init__(self, comparators):
        self.comparators = comparators

    def get_compared_pairs(self, partition, pairs):
        """
        Compares pairs of records with comparators

        Args:
            partition: a partition of dataframe of pyspark, that contains extracted data
            pairs: a list of triplets, with information rows should be compared to which

        Yields:
            list of pairs that match as a dict
            by match means, that all comparators returned True,
            {src=id1, dst=id2, are_equal=True/False}
        """
        data = list(partition)
        for row1_counter, row1 in enumerate(data):
            for row2_counter, row2 in enumerate(data):
                if row2_counter <= row1_counter: continue
                if not self._is_pair_in_pairs(pairs, row1['_id'], row2['_id']):
                    continue
                yield Row(
                    src=row1['_id'],
                    dst=row2['_id'],
                    are_equal=self._are_rows_equal(row1, row2)
                )

    @staticmethod
    def _is_pair_in_pairs(pairs, id1, id2):
        pairs_for_id1 = pairs[np.where(pairs[:, 1] == id1)]
        pairs_for_id1_id2 = pairs_for_id1[np.where(pairs_for_id1[:, 2] == id2)]

        if len(pairs_for_id1_id2) > 1: raise ValueError
        return len(pairs_for_id1_id2) == 1

    def _are_rows_equal(self, row1, row2):
        for comparator in self.comparators:
            if not comparator.are_similar(row1, row2):
                return False
        return True
