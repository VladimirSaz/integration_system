from functools import reduce
from pyspark.sql import DataFrame
import math
from pyspark.sql.types import *
import numpy as np


class PartitioningByPairs:
    """
    Class that partitions records in dataframes in such way that
    one cummulative dataframe with each partition that contains
    records that should be compared on each core of clusters nodes.


    Attributes:
        partitions: a number of partitions (recommended: set equal, or to be multiple of number of cores, default 4)
        sqlContext: sql context of pyspark

    """
    def __init__(self, sqlContext, partitions=4):
        self.partitions_count = partitions
        self.sqlContext = sqlContext

    def get(self, dataframes, pairs):
        """
        Treats dataframes as one big dataframe and splits to partitions,
        where each partition contains records for comparison
        (its not trivial, since each partition should have some overlapping records)

        Args:
            dataframes: a list of dataframes of pyspark, that contains extracted data (treated as one dataframe)
            pairs: a list of triplets, with information rows should be compared to which

        Returns:
            dataframe, that internally has partitions
        """
        if len(dataframes) == 0: self._raise_exception_empty_dataframes()
        pairs_by_partitions = self._split_pairs_by_partitions(pairs)
        combined_dataframe = self._union_all(*dataframes).orderBy('_id')
        self._validate_id_field_of_dataframe(combined_dataframe)
        partitions = self._get_partitions_as_dataframes(
            combined_dataframe, pairs_by_partitions
        )
        return self._union_all(*partitions)

    def _raise_exception_empty_dataframes(self):
        raise ValueError("list of dataframes cant be empty, no schema to infer")

    def _split_pairs_by_partitions(self, pairs):
        pairs_in_partition_count = math.ceil(len(pairs) / self.partitions_count)
        partitions = self._get_pairs_per_partition(pairs, pairs_in_partition_count)
        partitions = self._add_empty_list_if_partitions_less_than_expected(partitions)
        return partitions

    @staticmethod
    def _get_pairs_per_partition(pairs, pairs_in_partition_count):
        partitions = []
        if pairs_in_partition_count == 0: return partitions
        for i in range(0, len(pairs), pairs_in_partition_count):
            partitions.append(pairs[i:i + pairs_in_partition_count])
        return partitions

    def _add_empty_list_if_partitions_less_than_expected(self, partitions):
        while len(partitions) < self.partitions_count:
            partitions.append([])
        return partitions

    @staticmethod
    def _union_all(*dfs):
        return reduce(DataFrame.union, filter(None, dfs))

    def _validate_id_field_of_dataframe(self, dataframe):
        field = {
            'name': '_id',
            'valid_types': [IntegerType(), FloatType(), LongType()]
        }
        if not self._is_field_one_of_valid_types(dataframe, field):
            raise ValueError("_id field isn't numeric")

    def _is_field_one_of_valid_types(self, dataframe, field):
        for valid_type in field['valid_types']:
            if self._is_field_of_type(dataframe, field['name'], valid_type):
                return True
        return False

    @staticmethod
    def _is_field_of_type(dataframe, field_name, field_type):
        return dataframe.schema[field_name] == StructField(field_name, field_type)

    def _get_partitions_as_dataframes(self, dataframe, pairs_by_partitions):
        partitions = []
        for pairs_in_partition in pairs_by_partitions:
            partitions.append(
                self._get_partition(dataframe, np.array(pairs_in_partition))
            )
        return partitions

    def _get_partition(self, dataframe, pairs_in_partition):
        if len(pairs_in_partition) == 0:
            return self._get_empty_dataframe(dataframe.schema)
        minimum_id = self._get_minimum_id(pairs_in_partition)
        maximum_id = self._get_maximum_id(pairs_in_partition)
        return dataframe.where('_id>=' + str(minimum_id)) \
            .where('_id <=' + str(maximum_id)).repartition(1)

    def _get_empty_dataframe(self, schema):
        return self.sqlContext.createDataFrame([], schema=schema).repartition(1)

    @staticmethod
    def _get_minimum_id(pairs_in_partition):
        min_of_item_id_1 = min(pairs_in_partition[:, 1])
        min_of_item_id_2 = min(pairs_in_partition[:, 2])
        return min(min_of_item_id_1, min_of_item_id_2)

    @staticmethod
    def _get_maximum_id(pairs_in_partition):
        max_of_item_id_1 = max(pairs_in_partition[:, 1])
        max_of_item_id_2 = max(pairs_in_partition[:, 2])
        return max(max_of_item_id_1, max_of_item_id_2)
