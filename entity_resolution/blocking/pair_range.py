class PairRange:
    """
    Class prepares pairs of ids to compare, which helps
    to even out the load on cluster nodes
    """
    def get(self, blocks):
        """
        returns triplets
        [<id_of_block, id1_of_data, id2_of_data>,...]

        Args:
            blocks: a list of blocks of data

        Returns:
            a list of triplets, with information rows should be compared to which
        """
        result = []
        for block_id, block in enumerate(blocks):
            result += self._get_pairs_from_block(block_id, block)
        return result

    def _get_pairs_from_block(self, block_id, block):
        pairs = []
        if len(block) < 2: return pairs
        for item_id_1, item_1 in enumerate(block):
            for item_2 in block[item_id_1 + 1:]:
                pairs.append([block_id, item_1['_id'], item_2['_id']])
        return pairs

