from functools import reduce
from pyspark.sql import DataFrame


class Blocker:
    """
    Class that splits data by blocks based on blocking function

    Attributes:
        comparators: a list of blocking functions
        dataframes: a list of dataframes of pyspark, that contains extracted data (treated as one dataframe)

    Example:
    ::

        fields = [
            {
                "name": "description",
                "minimum_score": "0.50"
            },
        ]
        comparators=[LevenshteinSimilarity(fields=fields)]

    """
    def __init__(self, comparators, dataframes):
        self._row_ids_by_block = {}
        self._blocks = []
        self._comparators = comparators
        self._cumulative_df = self._get_cumulative_dataframe(dataframes)

    @staticmethod
    def _get_cumulative_dataframe(dataframes):
        if len(dataframes) == 0: return None
        if len(dataframes) == 1:
            return dataframes[0]
        return reduce(DataFrame.union, dataframes)


    def get_blocks(self):
        """
        gets blocks of data that split with comparators (blocking functions)

        Returns:
            a list of lists (list of blocks)
        """
        self._blocks = []
        self._row_ids_by_block = {}
        if not self._cumulative_df: return self._blocks
        for row_id, row in enumerate(self._cumulative_df.toLocalIterator()):
            if self._is_row_in_any_block(row_id):
                self._add_to_new_block(row_id, row)
            self._add_new_similar_rows_to_block(row_id, row)
        return self._blocks

    def _is_row_in_any_block(self, row_id):
        return row_id not in self._row_ids_by_block

    def _add_to_new_block(self, row_id, row):
        self._blocks.append([row.asDict()])
        last_block_index = len(self._blocks) - 1
        self._row_ids_by_block[row_id] = last_block_index

    def _add_new_similar_rows_to_block(self, id_of_row_for_compare, row_for_compare):
        iterator = self._get_dataframe_iterator_skipping_till(id_of_row_for_compare)
        for row_id, row in iterator:
            if row_id in self._row_ids_by_block:
                continue
            if self.__are_similar_rows(row, row_for_compare):
                self._add_to_existing_block(
                    row_id, row,
                    id_of_row_in_existing_block=id_of_row_for_compare
                )

    def _get_dataframe_iterator_skipping_till(self, row_counter_processed):
        number_of_rows_to_skip = row_counter_processed
        iterator = self._cumulative_df.toLocalIterator()
        [next(iterator) for x in range(number_of_rows_to_skip)]
        return enumerate(iterator, start=number_of_rows_to_skip)

    def __are_similar_rows(self, row1, row2):
        for comparator in self._comparators:
            if not comparator.are_similar(row1.asDict(), row2.asDict()):
                return False
        return True

    def _add_to_existing_block(self, row_id, row, id_of_row_in_existing_block):
        index_of_existing_block = self._row_ids_by_block[id_of_row_in_existing_block]
        self._row_ids_by_block[row_id] = index_of_existing_block
        self._blocks[index_of_existing_block].append(row.asDict())
