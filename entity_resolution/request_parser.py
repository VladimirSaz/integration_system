from pprint import pprint #noqa
import re


class RequestParser:
    def parse(self, raw_request):
        request_lines = self.get_request_lines(raw_request)
        lines = self.split_lines_by_keyword(request_lines)
        return self.parse_lines_of_each_section(lines)

    def get_request_lines(self, raw_request):
        request_lines = raw_request.split("\n")
        request_lines = [line.strip() for line in request_lines]
        return list(filter(None, request_lines))

    def split_lines_by_keyword(self, request_lines):
        select_lines = self.get_select_lines(request_lines)
        fuse_from_lines = self.get_fuse_from_lines(request_lines)
        fuse_by_lines = self.get_fuse_by_lines(request_lines)
        return {
            'SELECT': select_lines,
            'FROM': fuse_from_lines,
            'BY': fuse_by_lines
        }

    def get_select_lines(self, request_lines):
        attribute_lines = []
        is_select_found = 0
        is_fuse_from_found = 0
        for line in request_lines:
            if (self.is_select_line(line)):
                is_select_found = 1
            if (self.is_fuse_from_line(line)):
                is_fuse_from_found = 1
                break
            if is_select_found:
                attribute_lines.append(line)
        assert (is_fuse_from_found == 1), "No FUSE FROM keyword found"
        return attribute_lines

    def is_select_line(self, line):
        return self.is_line_match(line, r'SELECT')

    def is_line_match(self, line, match):
        matchObj = re.match(match, line, re.M | re.I)
        return True if matchObj else False

    def is_fuse_from_line(self, line):
        return self.is_line_match(line, r'FUSE FROM')

    def get_fuse_from_lines(self, request_lines):
        sources_lines = []
        is_fuse_from_found = 0
        is_fuse_by_found = 0
        for line in request_lines:
            if (self.is_fuse_by_found(line)):
                is_fuse_by_found = 1
                break
            if is_fuse_from_found:
                sources_lines.append(line)
            if (is_fuse_from_found == 0):
                is_fuse_from_found = self.is_fuse_from_line(line)
        assert (is_fuse_by_found == 1), "No FUSE BY keyword found"
        return sources_lines

    def is_fuse_by_found(self, line):
        return self.is_line_match(line, r'FUSE BY')

    def get_fuse_by_lines(self, request_lines):
        fuse_by_lines = []
        is_fuse_by_found = 0
        for line in request_lines:
            if is_fuse_by_found:
                fuse_by_lines.append(line)
            if (self.is_fuse_by_found(line)):
                is_fuse_by_found = 1
        return fuse_by_lines

    def parse_lines_of_each_section(self, lines):
        attributes = self.get_attributes_to_resolve(lines['SELECT'])
        sources = self.get_sources(lines['FROM'])
        fuse_by = self.get_fuse_by(lines['BY'])
        return {
            'attributes': attributes,
            'sources': sources,
            'fuse_by': fuse_by
        }

    def get_attributes_to_resolve(self, selects_lines):
        attributes_to_resolve = []
        for selects_line in selects_lines:
            attr = self.get_attribute_to_resolve(selects_line)
            attributes_to_resolve.append(attr)
        attributes_to_resolve = list(filter(None, attributes_to_resolve))
        return attributes_to_resolve

    def get_attribute_to_resolve(self, selects_line):
        return self.parse_command_by_keyword(selects_line, 'RESOLVE')

    def parse_command_by_keyword(self, line, keyword_name):
        regexp = re.search(keyword_name + r'\((.*)\)', line, re.M | re.I)
        if not regexp:
            return
        text_in_parenthesis = regexp.group(1)
        if not self.is_line_match(text_in_parenthesis, r'.+,.+'):
            return
        (attribute, command_line) = text_in_parenthesis.split(",")
        command = self.get_command_params(command_line.strip())
        return {
            'name': attribute,
            'function_to_apply': command
        }

    def get_command_params(self, command_line):
        regexp = re.search(r'(.*)\((.*)\)', command_line, re.M | re.I)
        if not regexp:
            return {'name': command_line, 'params': ''}

        command = {}
        command['name'] = regexp.group(1)
        command['params'] = regexp.group(2)
        return command

    def get_sources(self, sources_lines):
        return [sources_line.strip(',') for sources_line in sources_lines]

    def get_fuse_by(self, fuse_by_lines):
        assert (len(fuse_by_lines) == 1), "Incorrect FUSE BY"
        fuse_by_line = fuse_by_lines[0]
        keyword_name = 'SIMILARITY'
        return self.parse_command_by_keyword(fuse_by_line, keyword_name)
